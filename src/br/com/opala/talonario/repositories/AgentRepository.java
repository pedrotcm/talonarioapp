package br.com.opala.talonario.repositories;

import br.com.opala.talonario.entities.Agent;

import com.activeandroid.query.Select;

public class AgentRepository extends BaseRepository {

	public static Agent getCurrentAgent() {
		return new Select().from( Agent.class ).orderBy( "id DESC" ).executeSingle();
	}

	public static Agent getAgentByUsername( String username ) {
		return new Select().from( Agent.class ).where( "username = ?", username ).executeSingle();
	}

}
