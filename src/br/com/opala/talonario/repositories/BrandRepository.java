package br.com.opala.talonario.repositories;

import java.util.List;

import br.com.opala.talonario.entities.Brand;

import com.activeandroid.query.Select;

public class BrandRepository extends BaseRepository {

	public static Brand getBrandByDescription( String description ) {
		return new Select().from( Brand.class ).where( "descricao = ?", description ).executeSingle();
	}

	public static List<Brand> getAllOrderByName() {
		return new Select().from( Brand.class ).orderBy( "descricao ASC" ).execute();
	}
}
