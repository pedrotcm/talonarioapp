package br.com.opala.talonario.repositories;

import java.util.List;

import br.com.opala.talonario.entities.Specie;

import com.activeandroid.query.Select;

public class SpecieRepository extends BaseRepository {

	public static Specie getSpecieByDescription( String description ) {
		return new Select().from( Specie.class ).where( "descricao = ?", description ).executeSingle();
	}

	public static List<Specie> getAllOrderByOrder() {
		return new Select().from( Specie.class ).orderBy( "ordem ASC" ).execute();
	}
}
