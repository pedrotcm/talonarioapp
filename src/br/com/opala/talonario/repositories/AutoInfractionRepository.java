package br.com.opala.talonario.repositories;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.opala.talonario.entities.AutoInfraction;

import com.activeandroid.query.Select;

public class AutoInfractionRepository extends BaseRepository {

	public static List<AutoInfraction> getAllValid( String usernameAgent ) {
		return new Select().from( AutoInfraction.class ).where( "username_agente = ? ", usernameAgent ).and( "fk_id_cancelado is null" ).orderBy( "id DESC" ).execute();
	}

	public static List<AutoInfraction> findAutoInfractionByAgentAndPlaqueAndDate( String usernameAgent, String plaque, Date date ) {
		Calendar cal = Calendar.getInstance();
		String query = "%" + plaque.trim() + "%";
		String queryDateInit = "%%";
		String queryDateEnd = "";
		if ( date != null ) {
			queryDateInit = date.getTime() + "";
			cal.setTime( date );
			cal.add( Calendar.DATE, 1 );
			queryDateEnd = cal.getTimeInMillis() + "";
			return new Select().from( AutoInfraction.class ).where( "username_agente = ? ", usernameAgent ).and( "placa LIKE ?", query ).and( "data_criacao >= ?", queryDateInit ).and( "data_criacao <= ?", queryDateEnd ).and( "concluido = ? ", true ).and( "fk_id_cancelado is null" ).orderBy( "id DESC" ).execute();
		}
		return new Select().from( AutoInfraction.class ).where( "username_agente = ? ", usernameAgent ).and( "placa LIKE ?", query ).and( "data_criacao LIKE ?", queryDateInit ).and( "concluido = ? ", true ).and( "fk_id_cancelado is null" ).orderBy( "id DESC" ).execute();
	}

	public static List<AutoInfraction> getAllNotSent( String usernameAgent ) {
		return new Select().from( AutoInfraction.class ).where( "username_agente = ? ", usernameAgent ).and( "enviado = ?", false ).and( "concluido = ? ", true ).orderBy( "id ASC" ).execute();
	}

	public static List<AutoInfraction> getAllSent( String usernameAgent ) {
		return new Select().from( AutoInfraction.class ).where( "username_agente = ? ", usernameAgent ).and( "enviado = ?", true ).and( "concluido = ? ", true ).orderBy( "id ASC" ).execute();
	}
}
