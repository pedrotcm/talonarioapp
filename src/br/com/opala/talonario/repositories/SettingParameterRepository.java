package br.com.opala.talonario.repositories;

import android.database.Cursor;
import br.com.opala.talonario.entities.SettingParameter;

import com.activeandroid.Cache;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;

public class SettingParameterRepository extends BaseRepository {

	public static Integer getQntPhotos() {
		From query = new Select( "quantidade_fotos" ).from( SettingParameter.class ).orderBy( "id DESC" );
		Cursor cursor = Cache.openDatabase().rawQuery( query.toSql(), query.getArguments() );
		if ( cursor == null || cursor.getCount() == 0 )
			return null;
		cursor.moveToFirst();
		return cursor.getInt( 0 );
	}

	public static Integer getValidityAutos() {
		From query = new Select( "validade_autos" ).from( SettingParameter.class ).orderBy( "id DESC" );
		Cursor cursor = Cache.openDatabase().rawQuery( query.toSql(), query.getArguments() );
		if ( cursor == null || cursor.getCount() == 0 )
			return 0;
		cursor.moveToFirst();
		return cursor.getInt( 0 );
	}

	public static Boolean getIsDateEdit() {
		From query = new Select( "editar_data" ).from( SettingParameter.class ).orderBy( "id DESC" );
		Cursor cursor = Cache.openDatabase().rawQuery( query.toSql(), query.getArguments() );
		if ( cursor == null || cursor.getCount() == 0 )
			return null;
		cursor.moveToFirst();
		if ( cursor.getInt( 0 ) == 1 )
			return true;
		return false;
	}

	public static Integer getMinAutosWarn() {
		From query = new Select( "minimo_autos_avisar" ).from( SettingParameter.class ).orderBy( "id DESC" );
		Cursor cursor = Cache.openDatabase().rawQuery( query.toSql(), query.getArguments() );
		if ( cursor == null || cursor.getCount() == 0 )
			return null;
		cursor.moveToFirst();
		return cursor.getInt( 0 );
	}

	public static Integer getTimeToSendAutos() {
		From query = new Select( "tempo_min_enviar_autos" ).from( SettingParameter.class ).orderBy( "id DESC" );
		Cursor cursor = Cache.openDatabase().rawQuery( query.toSql(), query.getArguments() );
		if ( cursor == null || cursor.getCount() == 0 )
			return null;
		cursor.moveToFirst();
		return cursor.getInt( 0 );
	}

	public static Integer getTimeToRemoveAutos() {
		From query = new Select( "tempo_min_remover_autos" ).from( SettingParameter.class ).orderBy( "id DESC" );
		Cursor cursor = Cache.openDatabase().rawQuery( query.toSql(), query.getArguments() );
		if ( cursor == null || cursor.getCount() == 0 )
			return null;
		cursor.moveToFirst();
		return cursor.getInt( 0 );
	}
}
