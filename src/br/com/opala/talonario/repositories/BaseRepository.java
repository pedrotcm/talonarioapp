package br.com.opala.talonario.repositories;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

public class BaseRepository {

	public static List<?> getAll( Class<? extends Model> klass ) {
		return new Select().from( klass ).execute();

	}

	public static List<?> getAllOrderDesc( Class<? extends Model> klass ) {
		return new Select().from( klass ).orderBy( "id DESC" ).execute();

	}

	public static Object getLast( Class<? extends Model> klass ) {
		return new Select().from( klass ).orderBy( "id DESC" ).executeSingle();
	}

	public static void deleteAll( Class<? extends Model> klass ) {
		new Delete().from( klass ).where( "id > 0" ).execute();
	}

}
