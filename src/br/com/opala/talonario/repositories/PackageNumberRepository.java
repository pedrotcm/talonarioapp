package br.com.opala.talonario.repositories;

import br.com.opala.talonario.entities.PackageNumbers;

import com.activeandroid.query.Select;

public class PackageNumberRepository extends BaseRepository {

	public static PackageNumbers getPackageByAgentAndOrgan( String agentUsername, Integer codeOrgan ) {
		return new Select().from( PackageNumbers.class ).where( "usuario_agente = ?", agentUsername ).and( "codigo_orgao = ?", codeOrgan ).executeSingle();
	}

}
