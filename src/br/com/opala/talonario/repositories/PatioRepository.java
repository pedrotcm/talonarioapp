package br.com.opala.talonario.repositories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.database.Cursor;
import br.com.opala.talonario.entities.Patio;

import com.activeandroid.Cache;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.activeandroid.util.SQLiteUtils;

public class PatioRepository extends BaseRepository {

	public static List<String> getFilterOptions() {
		From query = new Select( "tipo_logradouro" ).distinct().from( Patio.class );
		Cursor cursor = Cache.openDatabase().rawQuery( query.toSql(), query.getArguments() );
		SQLiteUtils.processCursor( Patio.class, cursor );
		List<String> filters = new ArrayList<>();
		if ( cursor == null )
			return null;
		cursor.moveToFirst();
		while ( !cursor.isAfterLast() ) {
			if ( cursor.getString( 0 ) != null && !filters.contains( cursor.getString( 0 ).trim() ) )
				filters.add( cursor.getString( 0 ).trim() );
			cursor.moveToNext();
		}
		Collections.sort( filters );
		return filters;
	}

	public static List<Patio> getAllOrderly() {
		return new Select().from( Patio.class ).orderBy( "tipo_logradouro ASC, descricao_logradouro ASC" ).execute();
	}

	public static List<Patio> getAllOrderlyByFilter( String filter ) {
		filter = "tipo_logradouro = " + "'" + filter + "'";
		return new Select().from( Patio.class ).orderBy( "tipo_logradouro ASC, descricao_logradouro ASC" ).where( filter ).execute();
	}
}
