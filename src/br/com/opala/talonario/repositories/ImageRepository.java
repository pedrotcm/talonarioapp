package br.com.opala.talonario.repositories;

import br.com.opala.talonario.entities.Image;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

public class ImageRepository extends BaseRepository {

	public static Image getImageByPath( String path ) {
		return new Select().from( Image.class ).where( "path = ?", path ).executeSingle();
	}

	public static void removeImagesNotUsed() {
		new Delete().from( Image.class ).where( "fk_id_auto_infracao is null" ).execute();
	}
}
