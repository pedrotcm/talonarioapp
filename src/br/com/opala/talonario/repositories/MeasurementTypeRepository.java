package br.com.opala.talonario.repositories;

import br.com.opala.talonario.entities.MeasurementType;

import com.activeandroid.query.Select;

public class MeasurementTypeRepository extends BaseRepository {

	public static MeasurementType getMeasurementTypeByCode( Integer code ) {
		return new Select().from( MeasurementType.class ).where( "codigo = ?", code ).executeSingle();
	}
}
