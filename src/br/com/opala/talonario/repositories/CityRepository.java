package br.com.opala.talonario.repositories;

import br.com.opala.talonario.entities.City;

import com.activeandroid.query.Select;

public class CityRepository extends BaseRepository {

	public static City getCityByDescriptionAndCode( String description, Integer codeCity ) {
		return new Select().from( City.class ).where( "municipio = ?", description ).and( "codigo_municipio = ?", codeCity ).executeSingle();
	}
}
