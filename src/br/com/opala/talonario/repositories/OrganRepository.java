package br.com.opala.talonario.repositories;

import br.com.opala.talonario.entities.Organ;

import com.activeandroid.query.Select;

public class OrganRepository extends BaseRepository {

	public static Organ getOrganByDescription( String description ) {
		return new Select().from( Organ.class ).where( "orgao = ?", description ).executeSingle();
	}

}
