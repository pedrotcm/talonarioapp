package br.com.opala.talonario.repositories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import android.database.Cursor;
import br.com.opala.talonario.entities.Infraction;

import com.activeandroid.Cache;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.activeandroid.util.SQLiteUtils;

public class InfractionRepository extends BaseRepository {

	public static List<Infraction> getInfractionsByCompetence( String competence ) {
		List<String> newCompetences = getCompetences( competence );

		if ( newCompetences.size() == 1 )
			return new Select().from( Infraction.class ).where( "competencia LIKE ?", newCompetences.get( 0 ) ).orderBy( "codigo_infracao, desdobramento ASC" ).execute();
		else if ( newCompetences.size() == 2 )
			return new Select().from( Infraction.class ).where( "competencia LIKE ?", newCompetences.get( 0 ) ).or( "competencia LIKE ?", newCompetences.get( 1 ) ).orderBy( "codigo_infracao, desdobramento ASC" ).execute();
		else if ( newCompetences.size() == 3 )
			return new Select().from( Infraction.class ).where( "competencia LIKE ?", newCompetences.get( 0 ) ).or( "competencia LIKE ?", newCompetences.get( 1 ) ).or( "competencia LIKE ?", newCompetences.get( 2 ) ).orderBy( "codigo_infracao, desdobramento ASC" ).execute();

		return new Select().from( Infraction.class ).execute();
	}

	private static List<String> getCompetences( String competence ) {
		String competences[] = competence.split( Pattern.quote( "/" ) );
		List<String> newCompetences = new ArrayList<String>();

		for ( String c : competences ) {
			StringBuilder stringBuilder = new StringBuilder( c );
			stringBuilder.insert( 0, '%' );
			stringBuilder.insert( stringBuilder.toString().length(), '%' );
			newCompetences.add( stringBuilder.toString() );
		}
		return newCompetences;
	}

	public static Infraction getInfractionByCodeAndDeployment( Integer code, Integer deployment ) {
		return new Select().from( Infraction.class ).where( "codigo_infracao = ?", code ).and( "desdobramento = ?", deployment ).executeSingle();
	}

	public static List<Infraction> getInfractionsByFilterAndCompetence( String filter, String competence ) {
		List<String> newCompetences = getCompetences( competence );

		if ( filter == null )
			filter = "filtro is null";
		else
			filter = "filtro = " + "'" + filter + "'";

		if ( newCompetences.size() == 1 )
			return new Select().from( Infraction.class ).where( "competencia LIKE ?", newCompetences.get( 0 ) ).and( filter ).orderBy( "codigo_infracao, desdobramento ASC" ).execute();
		else if ( newCompetences.size() == 2 )
			return new Select().from( Infraction.class ).where( "competencia LIKE ?", newCompetences.get( 0 ) ).or( "competencia LIKE ?", newCompetences.get( 1 ) ).and( filter ).orderBy( "codigo_infracao, desdobramento ASC" ).execute();
		else if ( newCompetences.size() == 3 )
			return new Select().from( Infraction.class ).where( "competencia LIKE ?", newCompetences.get( 0 ) ).or( "competencia LIKE ?", newCompetences.get( 1 ) ).or( "competencia LIKE ?", newCompetences.get( 2 ) ).and( filter ).orderBy( "codigo_infracao, desdobramento ASC" ).execute();

		return new Select().from( Infraction.class ).execute();

	}

	public static List<String> getFilterOptions() {
		From query = new Select( "filtro" ).distinct().from( Infraction.class );
		Cursor cursor = Cache.openDatabase().rawQuery( query.toSql(), query.getArguments() );
		SQLiteUtils.processCursor( Infraction.class, cursor );
		List<String> filters = new ArrayList<>();
		if ( cursor == null )
			return null;
		cursor.moveToFirst();
		while ( !cursor.isAfterLast() ) {
			filters.add( cursor.getString( 0 ) != null ? cursor.getString( 0 ) : "Outros" );
			cursor.moveToNext();
		}
		Collections.sort( filters );
		return filters;
	}

}
