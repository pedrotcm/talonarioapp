package br.com.opala.talonario.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Column.ForeignKeyAction;
import com.activeandroid.annotation.Table;

@Table( name = "foto" )
public class Image extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column( name = "imagem" )
	private byte[] image;

	@Column( name = "data_criacao" )
	private Date createdAt;

	@Column( name = "path" )
	private String path;

	@Column( name = "extensao" )
	private String extension;

	@Column( name = "logradouro" )
	private String street;

	@Column( name = "numero" )
	private String number;

	@Column( name = "fk_id_auto_infracao", onUpdate = ForeignKeyAction.CASCADE, onDelete = ForeignKeyAction.CASCADE )
	private AutoInfraction autoInfraction;

	private transient Map<String, String> address;

	public byte[] getImage() {
		return image;
	}

	public void setImage( byte[] image ) {
		this.image = image;
	}

	public String getPath() {
		return path;
	}

	public void setPath( String path ) {
		this.path = path;
	}

	public AutoInfraction getAutoInfraction() {
		return autoInfraction;
	}

	public void setAutoInfraction( AutoInfraction autoInfraction ) {
		this.autoInfraction = autoInfraction;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension( String extension ) {
		this.extension = extension;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt( Date createdAt ) {
		this.createdAt = createdAt;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet( String street ) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber( String number ) {
		this.number = number;
	}

	public Map<String, String> getAddress() {
		return address;
	}

	public void setAddress( Map<String, String> address ) {
		this.address = address;
	}

}
