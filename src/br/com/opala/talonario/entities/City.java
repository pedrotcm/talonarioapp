package br.com.opala.talonario.entities;

import java.io.Serializable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Column.ForeignKeyAction;
import com.activeandroid.annotation.Table;

@Table( name = "municipio" )
public class City extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column( name = "municipio" )
	private String city;

	@Column( name = "codigo_municipio" )
	private Integer codeCity;

	@Column( name = "uf" )
	private String state;

	@Column( notNull = true, name = "fk_id_orgao", onUpdate = ForeignKeyAction.CASCADE, onDelete = ForeignKeyAction.CASCADE )
	private Organ organ;

	public City() {
		super();
	}

	public City( String city, Integer codeCity, String state, Organ organ ) {
		super();
		this.city = city;
		this.codeCity = codeCity;
		this.state = state;
		this.organ = organ;
	}

	public String getCity() {
		return city;
	}

	public void setCity( String city ) {
		this.city = city;
	}

	public Integer getCodeCity() {
		return codeCity;
	}

	public void setCodeCity( Integer codeCity ) {
		this.codeCity = codeCity;
	}

	public String getState() {
		return state;
	}

	public void setState( String state ) {
		this.state = state;
	}

	public Organ getOrgan() {
		return organ;
	}

	public void setOrgan( Organ organ ) {
		this.organ = organ;
	}

	@Override
	public String toString() {
		return city + "/" + state;
	}

}
