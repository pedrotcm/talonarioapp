package br.com.opala.talonario.entities;

import java.io.Serializable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Column.ForeignKeyAction;
import com.activeandroid.annotation.Table;

@Table( name = "telefone_ocorrencia" )
public class TelephoneOccurrence extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8574636453590910325L;

	@Column( name = "telefone" )
	private String telephone;

	@Column( notNull = true, name = "fk_id_contato", onUpdate = ForeignKeyAction.CASCADE, onDelete = ForeignKeyAction.CASCADE )
	private ContactOccurrence organOccurrence;

	@Column( name = "padrao" )
	private Boolean standard;

	public TelephoneOccurrence() {
		super();
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone( String telephone ) {
		this.telephone = telephone;
	}

	public ContactOccurrence getOrganOccurrence() {
		return organOccurrence;
	}

	public void setOrganOccurrence( ContactOccurrence organOccurrence ) {
		this.organOccurrence = organOccurrence;
	}

	public Boolean getStandard() {
		return standard;
	}

	public void setStandard( Boolean standard ) {
		this.standard = standard;
	}

}
