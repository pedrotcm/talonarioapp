package br.com.opala.talonario.entities;

import java.io.Serializable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table( name = "marca" )
public class Brand extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column( name = "descricao" )
	private String description;

	public Brand() {
		super();
	}

	public Brand( String description ) {
		super();
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	@Override
	public String toString() {
		return description;
	}

}
