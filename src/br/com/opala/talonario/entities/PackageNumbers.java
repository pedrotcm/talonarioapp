package br.com.opala.talonario.entities;

import java.io.Serializable;
import java.util.Date;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table( name = "pacote_numeros" )
public class PackageNumbers extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column( name = "data_solicitacao" )
	private Date requestDate;

	@Column( name = "numero_inicial" )
	private Integer initNumber;

	@Column( name = "numero_final" )
	private Integer endNumber;

	@Column( name = "ultimo_numero" )
	private Integer currentNumber;

	@Column( name = "usuario_agente" )
	private String agentUsername;

	@Column( name = "codigo_orgao" )
	private Integer codeOrgan;

	public PackageNumbers() {
		super();
	}

	public PackageNumbers( Date requestDate, Integer initNumber, Integer endNumber, Integer currentNumber ) {
		super();
		this.requestDate = requestDate;
		this.initNumber = initNumber;
		this.endNumber = endNumber;
		this.currentNumber = currentNumber;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate( Date requestDate ) {
		this.requestDate = requestDate;
	}

	public Integer getInitNumber() {
		return initNumber;
	}

	public void setInitNumber( Integer initNumber ) {
		this.initNumber = initNumber;
	}

	public Integer getEndNumber() {
		return endNumber;
	}

	public void setEndNumber( Integer endNumber ) {
		this.endNumber = endNumber;
	}

	public Integer getCurrentNumber() {
		return currentNumber;
	}

	public void setCurrentNumber( Integer currentNumber ) {
		this.currentNumber = currentNumber;
	}

	public String getAgentUsername() {
		return agentUsername;
	}

	public void setAgentUsername( String agentUsername ) {
		this.agentUsername = agentUsername;
	}

	public Integer getCodeOrgan() {
		return codeOrgan;
	}

	public void setCodeOrgan( Integer codeOrgan ) {
		this.codeOrgan = codeOrgan;
	}

	@Override
	public String toString() {
		return "PackageNumbers [requestDate=" + requestDate + ", initNumber=" + initNumber + ", endNumber=" + endNumber + ", currentNumber=" + currentNumber + "]";
	}

}
