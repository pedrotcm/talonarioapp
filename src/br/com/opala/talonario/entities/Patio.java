package br.com.opala.talonario.entities;

import java.io.Serializable;

import org.apache.commons.lang3.text.WordUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table( name = "logradouro" )
public class Patio extends Model implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column( name = "tipo_logradouro" )
	private String patioType;

	@Column( name = "descricao_logradouro" )
	private String patioDescription;

	@Column( name = "bairro" )
	private String neighborhood;

	public Patio() {}

	public String getPatioType() {
		return patioType;
	}

	public void setPatioType( String patioType ) {
		this.patioType = patioType;
	}

	public String getPatioDescription() {
		return patioDescription;
	}

	public void setPatioDescription( String patioDescription ) {
		this.patioDescription = patioDescription;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood( String neighborhood ) {
		this.neighborhood = neighborhood;
	}

	@Override
	public String toString() {
		return WordUtils.capitalizeFully( patioType ) + " " + WordUtils.capitalizeFully( patioDescription );
	}
}
