package br.com.opala.talonario.entities;

import java.io.Serializable;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Column.ForeignKeyAction;
import com.activeandroid.annotation.Table;

@Table( name = "orgao" )
public class Organ extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column( name = "orgao" )
	private String organ;

	@Column( name = "codigo_orgao" )
	private Integer codeOrgan;

	@Column( notNull = true, name = "fk_id_agente", onDelete = ForeignKeyAction.CASCADE, onUpdate = ForeignKeyAction.CASCADE )
	private Agent agent;

	@Column( name = "competencia" )
	private String competence;

	public Organ() {
		super();
	}

	public Organ( String organ, Integer codeOrgan, Agent agent ) {
		super();
		this.organ = organ;
		this.codeOrgan = codeOrgan;
		this.agent = agent;
	}

	public String getOrgan() {
		return organ;
	}

	public void setOrgan( String organ ) {
		this.organ = organ;
	}

	public Integer getCodeOrgan() {
		return codeOrgan;
	}

	public void setCodeOrgan( Integer codeOrgan ) {
		this.codeOrgan = codeOrgan;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent( Agent agent ) {
		this.agent = agent;
	}

	public List<City> getCities() {
		return getMany( City.class, "fk_id_orgao" );
	}

	@Override
	public String toString() {
		return organ;
	}

	public String getCompetence() {
		return competence;
	}

	public void setCompetence( String competence ) {
		this.competence = competence;
	}

}
