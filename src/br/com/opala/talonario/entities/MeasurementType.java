package br.com.opala.talonario.entities;

import java.io.Serializable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table( name = "tipo_medicao" )
public class MeasurementType extends Model implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column( name = "codigo" )
	private int code;

	@Column( name = "descricao" )
	private String description;

	@Column( name = "valor_permitido" )
	private String allowedValue;

	public int getCode() {
		return code;
	}

	public void setCode( int code ) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	public String getAllowedValue() {
		return allowedValue;
	}

	public void setAllowedValue( String allowedValue ) {
		this.allowedValue = allowedValue;
	}

}
