package br.com.opala.talonario.entities;

import java.io.Serializable;

public class DriverOffenderVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	private String cpfRg;
	private String driverCnh;
	private String stateCnh;

	public DriverOffenderVO( String name, String cpfRg ) {
		super();
		this.name = name;
		this.cpfRg = cpfRg;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getCpfRg() {
		return cpfRg;
	}

	public void setCpfRg( String cpfRg ) {
		this.cpfRg = cpfRg;
	}

	public String getDriverCnh() {
		return driverCnh;
	}

	public void setDriverCnh( String driverCnh ) {
		this.driverCnh = driverCnh;
	}

	public String getStateCnh() {
		return stateCnh;
	}

	public void setStateCnh( String stateCnh ) {
		this.stateCnh = stateCnh;
	}

}
