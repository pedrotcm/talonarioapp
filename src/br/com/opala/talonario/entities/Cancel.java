package br.com.opala.talonario.entities;

import java.io.Serializable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table( name = "cancelado" )
public class Cancel extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column( name = "justificativa" )
	private String justification;

	@Column( name = "motivo" )
	private String reason;

	public Cancel() {
		super();
	}

	public String getJustification() {
		return justification;
	}

	public void setJustification( String justification ) {
		this.justification = justification;
	}

	public String getReason() {
		return reason;
	}

	public void setReason( String reason ) {
		this.reason = reason;
	}

}
