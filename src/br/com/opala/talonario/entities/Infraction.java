package br.com.opala.talonario.entities;

import java.io.Serializable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table( name = "infracao" )
public class Infraction extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column( name = "codigo_infracao" )
	private Integer codeInfraction;

	@Column( name = "desdobramento" )
	private Integer deployment;

	@Column( name = "descricao" )
	private String description;

	@Column( name = "amparo_legal" )
	private String legalSupport;

	@Column( name = "lei" )
	private String law;

	@Column( name = "penalidade" )
	private String punishment;

	@Column( name = "medida_administrativa" )
	private String administrativeMeasure;

	@Column( name = "tipo_medicao" )
	private Integer measurementType;

	@Column( name = "observacoes_um" )
	private String observationsOne;

	@Column( name = "observacoes_dois" )
	private String observationsTwo;

	@Column( name = "observacoes_tres" )
	private String observationsThree;

	@Column( name = "competencia" )
	private String competence;

	@Column( name = "filtro" )
	private String filter;

	public Infraction() {
		super();
	}

	public Infraction( Integer codeInfraction, String description, String legalSupport, String punishment, String administrativeMeasure, Integer measurementType ) {
		super();
		this.codeInfraction = codeInfraction;
		this.description = description;
		this.legalSupport = legalSupport;
		this.punishment = punishment;
		this.administrativeMeasure = administrativeMeasure;
		this.measurementType = measurementType;
	}

	public Integer getCodeInfraction() {
		return codeInfraction;
	}

	public void setCodeInfraction( Integer codeInfraction ) {
		this.codeInfraction = codeInfraction;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	public String getLegalSupport() {
		return legalSupport;
	}

	public void setLegalSupport( String legalSupport ) {
		this.legalSupport = legalSupport;
	}

	public String getLaw() {
		return law;
	}

	public void setLaw( String law ) {
		this.law = law;
	}

	public String getObservationsOne() {
		return observationsOne;
	}

	public void setObservationsOne( String observationsOne ) {
		this.observationsOne = observationsOne;
	}

	public String getObservationsTwo() {
		return observationsTwo;
	}

	public void setObservationsTwo( String observationsTwo ) {
		this.observationsTwo = observationsTwo;
	}

	public String getObservationsThree() {
		return observationsThree;
	}

	public void setObservationsThree( String observationsThree ) {
		this.observationsThree = observationsThree;
	}

	public String getPunishment() {
		return punishment;
	}

	public void setPunishment( String punishment ) {
		this.punishment = punishment;
	}

	public String getAdministrativeMeasure() {
		return administrativeMeasure;
	}

	public void setAdministrativeMeasure( String administrativeMeasure ) {
		this.administrativeMeasure = administrativeMeasure;
	}

	public Integer getMeasurementType() {
		return measurementType;
	}

	public void setMeasurementType( Integer measurementType ) {
		this.measurementType = measurementType;
	}

	public Integer getDeployment() {
		return deployment;
	}

	public void setDeployment( Integer deployment ) {
		this.deployment = deployment;
	}

	public String getCompetence() {
		return competence;
	}

	public void setCompetence( String competence ) {
		this.competence = competence;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter( String filter ) {
		this.filter = filter;
	}

	@Override
	public String toString() {
		return "Infraction [codeInfraction=" + codeInfraction + ", description=" + description + ", legalSupport=" + legalSupport + ", punishment=" + punishment + ", administrativeMeasure=" + administrativeMeasure + ", measurementType=" + measurementType + "]";
	}

}
