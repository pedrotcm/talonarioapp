package br.com.opala.talonario.entities;

import java.io.Serializable;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table( name = "contato_ocorrencia" )
public class ContactOccurrence extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5459677625126387440L;

	@Column( name = "nome" )
	private String name;

	public ContactOccurrence() {
		super();
	}

	public ContactOccurrence( String name ) {
		this.name = name;
	}

	public List<TelephoneOccurrence> getTelephones() {
		return getMany( TelephoneOccurrence.class, "fk_id_contato" );
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

}
