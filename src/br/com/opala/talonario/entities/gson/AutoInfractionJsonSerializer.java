package br.com.opala.talonario.entities.gson;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.text.SimpleDateFormat;

import android.annotation.SuppressLint;
import br.com.opala.talonario.entities.AutoInfraction;
import br.com.opala.talonario.entities.Image;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class AutoInfractionJsonSerializer implements JsonSerializer<AutoInfraction> {
	@SuppressLint( "NewApi" )
	@Override
	public JsonElement serialize( AutoInfraction entity, Type typeOfSrc, JsonSerializationContext context ) {
		SimpleDateFormat dateFormat = new SimpleDateFormat( "dd/MM/yyyy HH:mm" );

		final JsonObject jsonObject = new JsonObject();

		jsonObject.addProperty( "organ", entity.getOrgan() );
		jsonObject.addProperty( "codeOrgan", entity.getCodeOrgan() );

		jsonObject.addProperty( "city", entity.getCity() );
		jsonObject.addProperty( "codeCity", entity.getCodeCity() );
		jsonObject.addProperty( "state", entity.getState() );

		jsonObject.addProperty( "agentUsername", entity.getAgentUsername() );
		jsonObject.addProperty( "agentName", entity.getAgentName() );

		jsonObject.addProperty( "numberAutoInfraction", entity.getNumberAutoInfraction() );
		jsonObject.addProperty( "createdAt", dateFormat.format( entity.getCreatedAt() ) );
		jsonObject.addProperty( "plaque", entity.getPlaque() );
		jsonObject.addProperty( "renavam", entity.getRenavam() );
		jsonObject.addProperty( "model", entity.getModel() );
		jsonObject.addProperty( "color", entity.getColor() );
		jsonObject.addProperty( "brand", entity.getBrand() );
		jsonObject.addProperty( "otherBrand", entity.getOtherBrand() );
		jsonObject.addProperty( "specie", entity.getSpecie() );

		jsonObject.addProperty( "codeInfraction", entity.getCodeInfraction() );
		jsonObject.addProperty( "deployment", entity.getDeployment() );
		jsonObject.addProperty( "description", entity.getDescription() );
		jsonObject.addProperty( "legalSupport", entity.getLegalSupport() );
		jsonObject.addProperty( "punishment", entity.getPunishment() );
		jsonObject.addProperty( "administrativeMeasure", entity.getAdministrativeMeasure() );
		jsonObject.addProperty( "measurementType", entity.getMeasurementType() );
		jsonObject.addProperty( "observations", entity.getObservations() );
		jsonObject.addProperty( "competence", entity.getCompetence() );
		jsonObject.addProperty( "otherInformation", entity.getOtherInformation() );

		jsonObject.addProperty( "driverName", entity.getDriverName() );
		jsonObject.addProperty( "driverCpfRg", entity.getDriverCpfRg() );
		jsonObject.addProperty( "driverCnh", entity.getDriverCnh() );
		jsonObject.addProperty( "stateCnh", entity.getStateCnh() );

		jsonObject.addProperty( "offenderName", entity.getOffenderName() );
		jsonObject.addProperty( "offenderCpfRg", entity.getOffenderCpfRg() );

		jsonObject.addProperty( "street", entity.getStreet() );
		jsonObject.addProperty( "number", entity.getNumber() );
		jsonObject.addProperty( "neighborhood", entity.getNeighborhood() );
		jsonObject.addProperty( "complement", entity.getComplement() );

		jsonObject.addProperty( "measurementPerformed", entity.getMeasurementPerformed() );
		jsonObject.addProperty( "regulatedLimit", entity.getRegulatedLimit() );
		jsonObject.addProperty( "measurementVerified", entity.getMeasurementVerified() );
		jsonObject.addProperty( "exceeded", entity.getExceeded() );
		jsonObject.addProperty( "equipmentUsed", entity.getEquipmentUsed() );

		jsonObject.addProperty( "latitude", entity.getLatitude() );
		jsonObject.addProperty( "longitude", entity.getLongitude() );

		if ( !entity.getPhotos().isEmpty() ) {
			JsonArray jsonPhotos = new JsonArray();
			int qnt = 1;
			for ( Image photo : entity.getPhotos() ) {
				JsonObject jsonPhoto = new JsonObject();
				String photoFile = null;
				try {
					photoFile = new String( photo.getImage(), "ISO-8859-1" );
				} catch ( UnsupportedEncodingException e ) {
					e.printStackTrace();
				}
				jsonPhoto.addProperty( "fileContent", photoFile );
				String photoName = String.format( "%s_%s", entity.getNumberAutoInfraction() + "_" + qnt++, new SimpleDateFormat( "yyyyMMddHHmm" ).format( entity.getCreatedAt() ) );
				jsonPhoto.addProperty( "name", photoName );
				jsonPhoto.addProperty( "createdDate", dateFormat.format( entity.getCreatedAt() ) );
				jsonPhoto.addProperty( "extension", photo.getExtension() );
				jsonPhotos.add( jsonPhoto );
			}
			jsonObject.add( "images", jsonPhotos );
		}

		if ( entity.getCancel() != null ) {
			JsonObject jsonCancel = new JsonObject();
			jsonCancel.addProperty( "justification", entity.getCancel().getJustification() );
			jsonCancel.addProperty( "reason", entity.getCancel().getReason() );
			jsonObject.add( "cancel", jsonCancel );
		}

		return jsonObject;
	}

	public static String bytesToStringUTFNIO( byte[] bytes ) {
		CharBuffer cBuffer = ByteBuffer.wrap( bytes ).asCharBuffer();
		return cBuffer.toString();
	}
}