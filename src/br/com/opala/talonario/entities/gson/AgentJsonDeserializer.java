package br.com.opala.talonario.entities.gson;

import java.lang.reflect.Type;

import br.com.opala.talonario.entities.Agent;
import br.com.opala.talonario.entities.City;
import br.com.opala.talonario.entities.Organ;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class AgentJsonDeserializer implements JsonDeserializer<Agent> {

	@Override
	public Agent deserialize( JsonElement json, Type typeOfSrc, JsonDeserializationContext context ) throws JsonParseException {
		Agent agent = new Agent();

		String name = json.getAsJsonObject().get( "name" ).getAsString();
		String username = json.getAsJsonObject().get( "username" ).getAsString();
		String token = json.getAsJsonObject().get( "token" ).getAsString();
		String password = json.getAsJsonObject().get( "password" ).getAsString();

		agent.setName( name );
		agent.setUsername( username );
		agent.setToken( token );
		agent.setPassword( password );
		agent.save();

		JsonElement organs = json.getAsJsonObject().get( "organs" );
		for ( int i = 0; i < organs.getAsJsonArray().size(); i++ ) {
			Organ organEntity = new Organ();

			JsonElement organ = organs.getAsJsonArray().get( i );
			Integer code = organ.getAsJsonObject().get( "code" ).getAsInt();
			String nameOrgan = organ.getAsJsonObject().get( "name" ).getAsString();
			String competence = organ.getAsJsonObject().get( "competence" ).getAsString();

			organEntity.setCodeOrgan( code );
			organEntity.setOrgan( nameOrgan );
			organEntity.setCompetence( competence );
			organEntity.setAgent( agent );
			organEntity.save();

			JsonElement cities = organ.getAsJsonObject().get( "cities" );
			for ( int j = 0; j < cities.getAsJsonArray().size(); j++ ) {
				City cityEntity = new City();

				JsonElement city = cities.getAsJsonArray().get( j );
				Integer cityCode = city.getAsJsonObject().get( "cityCode" ).getAsInt();
				String nameCity = city.getAsJsonObject().get( "name" ).getAsString();
				JsonElement state = city.getAsJsonObject().get( "state" );
				String uf = state.getAsJsonObject().get( "uf" ).getAsString();

				cityEntity.setCity( nameCity );
				cityEntity.setCodeCity( cityCode );
				cityEntity.setState( uf );
				cityEntity.setOrgan( organEntity );
				cityEntity.save();
			}
		}

		return agent;
	}

}