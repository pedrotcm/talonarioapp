package br.com.opala.talonario.entities.gson;

import java.lang.reflect.Type;

import android.util.Log;
import br.com.opala.talonario.entities.Brand;
import br.com.opala.talonario.entities.ContactOccurrence;
import br.com.opala.talonario.entities.Infraction;
import br.com.opala.talonario.entities.MeasurementType;
import br.com.opala.talonario.entities.Patio;
import br.com.opala.talonario.entities.Reason;
import br.com.opala.talonario.entities.SettingParameter;
import br.com.opala.talonario.entities.Specie;
import br.com.opala.talonario.entities.TelephoneOccurrence;
import br.com.opala.talonario.repositories.BaseRepository;

import com.activeandroid.ActiveAndroid;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class DataJsonDeserializer implements JsonDeserializer<Boolean> {
	@Override
	public Boolean deserialize( JsonElement json, Type typeOfSrc, JsonDeserializationContext context ) throws JsonParseException {
		final long startTime = System.currentTimeMillis();

		ActiveAndroid.beginTransaction();

		BaseRepository.deleteAll( Brand.class );
		BaseRepository.deleteAll( Specie.class );
		BaseRepository.deleteAll( Reason.class );
		BaseRepository.deleteAll( SettingParameter.class );
		BaseRepository.deleteAll( Infraction.class );
		BaseRepository.deleteAll( ContactOccurrence.class );
		BaseRepository.deleteAll( Patio.class );

		try {

			for ( int i = 0; i < json.getAsJsonArray().size(); i++ ) {
				JsonElement elementArrayEntity = json.getAsJsonArray().get( i );
				for ( int j = 0; j < elementArrayEntity.getAsJsonArray().size(); j++ ) {
					JsonElement elementEntity = elementArrayEntity.getAsJsonArray().get( j );

					if ( elementEntity.getAsJsonObject().has( "id_logradouro" ) ) {
						Patio patio = new Patio();
						if ( !elementEntity.getAsJsonObject().get( "patioType" ).isJsonNull() )
							patio.setPatioType( elementEntity.getAsJsonObject().get( "patioType" ).getAsString() );
						if ( !elementEntity.getAsJsonObject().get( "patioDescription" ).isJsonNull() )
							patio.setPatioDescription( elementEntity.getAsJsonObject().get( "patioDescription" ).getAsString() );
						if ( !elementEntity.getAsJsonObject().get( "neighborhood" ).isJsonNull() )
							patio.setNeighborhood( elementEntity.getAsJsonObject().get( "neighborhood" ).getAsString() );
						patio.save();
					} else if ( elementEntity.getAsJsonObject().has( "id_marca" ) ) {
						Brand brand = new Brand();
						brand.setDescription( elementEntity.getAsJsonObject().get( "description" ).getAsString() );
						brand.save();
					} else if ( elementEntity.getAsJsonObject().has( "id_especie" ) ) {
						Specie specie = new Specie();
						specie.setDescription( elementEntity.getAsJsonObject().get( "description" ).getAsString() );
						specie.setOrder( elementEntity.getAsJsonObject().get( "order" ).getAsInt() );
						specie.save();
					} else if ( elementEntity.getAsJsonObject().has( "id_motivo" ) ) {
						Reason reason = new Reason();
						reason.setReason( elementEntity.getAsJsonObject().get( "description" ).getAsString() );
						reason.save();
					} else if ( elementEntity.getAsJsonObject().has( "id_param_config" ) ) {
						SettingParameter setting = new SettingParameter();
						setting.setIsDateEdit( elementEntity.getAsJsonObject().get( "isDateEdit" ).getAsBoolean() );
						setting.setQntPhotos( elementEntity.getAsJsonObject().get( "qntPhotos" ).getAsInt() );
						setting.setValidityAutos( elementEntity.getAsJsonObject().get( "validityAutos" ).getAsInt() );
						setting.setQntAutoInPackage( elementEntity.getAsJsonObject().get( "qntAutoInPackage" ).getAsInt() );
						setting.setMinAutosWarn( elementEntity.getAsJsonObject().get( "minAutosWarn" ).getAsInt() );
						setting.setTimeToRemoveAutos( elementEntity.getAsJsonObject().get( "timeToSendAutos" ).getAsInt() );
						setting.setTimeToSendAutos( elementEntity.getAsJsonObject().get( "timeToRemoveAutos" ).getAsInt() );
						setting.save();
					} else if ( elementEntity.getAsJsonObject().has( "id_tipo_medicao" ) ) {
						MeasurementType measurementType = new MeasurementType();
						measurementType.setCode( elementEntity.getAsJsonObject().get( "code" ).getAsInt() );
						measurementType.setDescription( elementEntity.getAsJsonObject().get( "description" ).getAsString() );
						measurementType.setAllowedValue( elementEntity.getAsJsonObject().get( "allowedValue" ).getAsString() );
						measurementType.save();
					} else if ( elementEntity.getAsJsonObject().has( "id_contato_ocorrencia" ) ) {
						ContactOccurrence organOccurrence = new ContactOccurrence();
						organOccurrence.setName( elementEntity.getAsJsonObject().get( "name" ).getAsString() );
						organOccurrence.save();

						JsonElement telephones = elementEntity.getAsJsonObject().get( "telephones" );
						for ( int k = 0; k < telephones.getAsJsonArray().size(); k++ ) {
							TelephoneOccurrence telephone = new TelephoneOccurrence();
							JsonElement phone = telephones.getAsJsonArray().get( k );

							telephone.setOrganOccurrence( organOccurrence );
							telephone.setTelephone( phone.getAsJsonObject().get( "telephone" ).getAsString() );
							telephone.setStandard( phone.getAsJsonObject().get( "standard" ).getAsBoolean() );
							telephone.save();
						}
					} else if ( elementEntity.getAsJsonObject().has( "id_infracao" ) ) {
						Infraction infraction = new Infraction();
						JsonElement codeInfraction = elementEntity.getAsJsonObject().get( "codeInfraction" );
						JsonElement deployment = elementEntity.getAsJsonObject().get( "deployment" );
						JsonElement description = elementEntity.getAsJsonObject().get( "description" );
						JsonElement legalSupport = elementEntity.getAsJsonObject().get( "legalSupport" );
						JsonElement law = elementEntity.getAsJsonObject().get( "law" );
						JsonElement punishment = elementEntity.getAsJsonObject().get( "punishment" );
						JsonElement administrativeMeasure = elementEntity.getAsJsonObject().get( "administrativeMeasure" );
						JsonElement measurementType = elementEntity.getAsJsonObject().get( "measurementType" );
						JsonElement observationsOne = elementEntity.getAsJsonObject().get( "observationsOne" );
						JsonElement observationsTwo = elementEntity.getAsJsonObject().get( "observationsTwo" );
						JsonElement observationsThree = elementEntity.getAsJsonObject().get( "observationsThree" );
						JsonElement competence = elementEntity.getAsJsonObject().get( "competence" );
						JsonElement filter = elementEntity.getAsJsonObject().get( "filter" );

						if ( !codeInfraction.isJsonNull() )
							infraction.setCodeInfraction( codeInfraction.getAsInt() );
						if ( !deployment.isJsonNull() )
							infraction.setDeployment( deployment.getAsInt() );
						if ( !description.isJsonNull() )
							infraction.setDescription( description.getAsString() );
						if ( !legalSupport.isJsonNull() )
							infraction.setLegalSupport( legalSupport.getAsString() );
						if ( !punishment.isJsonNull() )
							infraction.setPunishment( punishment.getAsString() );
						if ( !administrativeMeasure.isJsonNull() )
							infraction.setAdministrativeMeasure( administrativeMeasure.getAsString() );
						if ( !measurementType.isJsonNull() )
							infraction.setMeasurementType( measurementType.getAsInt() );
						if ( !observationsOne.isJsonNull() )
							infraction.setObservationsOne( observationsOne.getAsString() );
						if ( !observationsTwo.isJsonNull() )
							infraction.setObservationsTwo( observationsTwo.getAsString() );
						if ( !observationsThree.isJsonNull() )
							infraction.setObservationsThree( observationsThree.getAsString() );
						if ( !competence.isJsonNull() )
							infraction.setCompetence( competence.getAsString() );
						if ( !law.isJsonNull() )
							infraction.setLaw( law.getAsString() );
						if ( !filter.isJsonNull() )
							infraction.setFilter( filter.getAsString() );
						infraction.save();
					}
				}
			}

			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}

		final long endTime = System.currentTimeMillis();
		Log.d( "TEST", "Total execution time: " + ( endTime - startTime ) / 1000 );

		return true;
	}
}
