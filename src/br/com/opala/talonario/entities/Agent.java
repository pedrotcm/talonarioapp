package br.com.opala.talonario.entities;

import java.io.Serializable;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table( name = "agente" )
public class Agent extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column( name = "nome" )
	private String name;

	@Column( name = "username" )
	private String username;

	@Column( name = "password" )
	private String password;

	@Column( name = "token" )
	private String token;

	public Agent() {
		super();
	}

	public Agent( String name, String username, String token ) {
		super();
		this.name = name;
		this.username = username;
		this.token = token;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername( String username ) {
		this.username = username;
	}

	public String getToken() {
		return token;
	}

	public void setToken( String token ) {
		this.token = token;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword( String password ) {
		this.password = password;
	}

	public List<AutoInfraction> getAutoInfraction() {
		return getMany( AutoInfraction.class, "fk_id_agente" );
	}

	public List<Organ> getOrgans() {
		return getMany( Organ.class, "fk_id_agente" );
	}

	@Override
	public String toString() {
		return "Agent [name=" + name + ", username=" + username + ", token=" + token + ", organ= " + getOrgans() + "]";
	}

}
