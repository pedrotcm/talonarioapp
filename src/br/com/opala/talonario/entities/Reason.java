package br.com.opala.talonario.entities;

import java.io.Serializable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table( name = "motivo" )
public class Reason extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column( name = "descricao" )
	private String description;

	public Reason() {
		super();
	}

	public Reason( String description ) {
		super();
		this.description = description;
	}

	public String getReason() {
		return description;
	}

	public void setReason( String reason ) {
		this.description = reason;
	}

	@Override
	public String toString() {
		return description;
	}

}
