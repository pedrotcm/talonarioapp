package br.com.opala.talonario.entities;

import java.io.Serializable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table( name = "parametros_configuracao" )
public class SettingParameter extends Model implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column( name = "editar_data" )
	private Boolean isDateEdit;

	@Column( name = "quantidade_fotos" )
	private Integer qntPhotos;

	@Column( name = "validade_autos" )
	private Integer validityAutos;

	@Column( name = "quantidade_autos_pacote" )
	private Integer qntAutoInPackage;

	@Column( name = "minimo_autos_avisar" )
	private Integer minAutosWarn;

	@Column( name = "tempo_min_enviar_autos" )
	private Integer timeToSendAutos;

	@Column( name = "tempo_min_remover_autos" )
	private Integer timeToRemoveAutos;

	public SettingParameter() {
		super();
	}

	public Boolean getIsDateEdit() {
		return isDateEdit;
	}

	public void setIsDateEdit( Boolean isDateEdit ) {
		this.isDateEdit = isDateEdit;
	}

	public Integer getQntPhotos() {
		return qntPhotos;
	}

	public void setQntPhotos( Integer qntPhotos ) {
		this.qntPhotos = qntPhotos;
	}

	public Integer getValidityAutos() {
		return validityAutos;
	}

	public void setValidityAutos( Integer validityAutos ) {
		this.validityAutos = validityAutos;
	}

	public Integer getQntAutoInPackage() {
		return qntAutoInPackage;
	}

	public void setQntAutoInPackage( Integer qntAutoInPackage ) {
		this.qntAutoInPackage = qntAutoInPackage;
	}

	public Integer getMinAutosWarn() {
		return minAutosWarn;
	}

	public void setMinAutosWarn( Integer minAutosWarn ) {
		this.minAutosWarn = minAutosWarn;
	}

	public Integer getTimeToSendAutos() {
		return timeToSendAutos;
	}

	public void setTimeToSendAutos( Integer timeToSendAutos ) {
		this.timeToSendAutos = timeToSendAutos;
	}

	public Integer getTimeToRemoveAutos() {
		return timeToRemoveAutos;
	}

	public void setTimeToRemoveAutos( Integer timeToRemoveAutos ) {
		this.timeToRemoveAutos = timeToRemoveAutos;
	}

}
