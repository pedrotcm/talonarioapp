package br.com.opala.talonario.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Column.ForeignKeyAction;
import com.activeandroid.annotation.Table;

@Table( name = "auto_infracao" )
public class AutoInfraction extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column( name = "orgao" )
	private String organ;
	@Column( name = "codigo_orgao" )
	private Integer codeOrgan;

	@Column( name = "municipio" )
	private String city;
	@Column( name = "codigo_municipio" )
	private Integer codeCity;
	@Column( name = "uf" )
	private String state;

	@Column( name = "nome_agente" )
	private String agentName;
	@Column( name = "username_agente" )
	private String agentUsername;

	@Column( name = "numero_auto_infracao" )
	private String numberAutoInfraction;
	@Column( name = "data_criacao" )
	private Date createdAt;
	@Column( name = "placa" )
	private String plaque;
	@Column( name = "renavam" )
	private String renavam;
	@Column( name = "modelo" )
	private String model;
	@Column( name = "cor" )
	private String color;

	@Column( name = "marca" )
	private String brand;
	@Column( name = "outra_marca" )
	private String otherBrand;
	@Column( name = "especie" )
	private String specie;

	@Column( name = "codigo_infracao" )
	private Integer codeInfraction;
	@Column( name = "desdobramento" )
	private Integer deployment;
	@Column( name = "descricao" )
	private String description;
	@Column( name = "amparo_legal" )
	private String legalSupport;
	@Column( name = "penalidade" )
	private String punishment;
	@Column( name = "medida_administrativa" )
	private String administrativeMeasure;
	@Column( name = "tipo_medicao" )
	private Integer measurementType;
	@Column( name = "observacoes" )
	private String observations;
	@Column( name = "competencia" )
	private String competence;
	@Column( name = "outras_informacoes" )
	private String otherInformation;
	@Column( name = "nome_condutor" )
	private String driverName;
	@Column( name = "cpf_rg_condutor" )
	private String driverCpfRg;
	@Column( name = "cnh_condutor" )
	private String driverCnh;
	@Column( name = "uf_cnh" )
	private String stateCnh;
	@Column( name = "nome_infrator" )
	private String offenderName;
	@Column( name = "cpf_rg_infrator" )
	private String offenderCpfRg;
	@Column( name = "logradouro" )
	private String street;
	@Column( name = "numero" )
	private Integer number;
	@Column( name = "bairro" )
	private String neighborhood;
	@Column( name = "complemento" )
	private String complement;

	@Column( name = "medicao_realizada" )
	private String measurementPerformed;
	@Column( name = "limite_regulamentado" )
	private String regulatedLimit;
	@Column( name = "medicao_considerada" )
	private String measurementVerified;
	@Column( name = "excesso_verificado" )
	private String exceeded;
	@Column( name = "equipamento_utilizado" )
	private String equipmentUsed;

	@Column( name = "fk_id_cancelado", onUpdate = ForeignKeyAction.CASCADE, onDelete = ForeignKeyAction.CASCADE )
	private Cancel cancel;
	@Column( name = "latitude" )
	private Double latitude;
	@Column( name = "longitude" )
	private Double longitude;
	@Column( name = "concluido" )
	private Boolean completed = false;
	@Column( name = "enviado" )
	private Boolean sent = false;

	public AutoInfraction() {
		super();
	}

	public String getOrgan() {
		return organ;
	}

	public void setOrgan( String organ ) {
		this.organ = organ;
	}

	public Integer getCodeOrgan() {
		return codeOrgan;
	}

	public void setCodeOrgan( Integer codeOrgan ) {
		this.codeOrgan = codeOrgan;
	}

	public String getCity() {
		return city;
	}

	public void setCity( String city ) {
		this.city = city;
	}

	public Integer getCodeCity() {
		return codeCity;
	}

	public void setCodeCity( Integer codeCity ) {
		this.codeCity = codeCity;
	}

	public String getState() {
		return state;
	}

	public void setState( String state ) {
		this.state = state;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName( String agentName ) {
		this.agentName = agentName;
	}

	public String getAgentUsername() {
		return agentUsername;
	}

	public void setAgentUsername( String agentUsername ) {
		this.agentUsername = agentUsername;
	}

	public String getNumberAutoInfraction() {
		return numberAutoInfraction;
	}

	public void setNumberAutoInfraction( String numberAutoInfraction ) {
		this.numberAutoInfraction = numberAutoInfraction;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt( Date createdAt ) {
		this.createdAt = createdAt;
	}

	public String getPlaque() {
		return plaque;
	}

	public void setPlaque( String plaque ) {
		this.plaque = plaque;
	}

	public String getRenavam() {
		return renavam;
	}

	public void setRenavam( String renavam ) {
		this.renavam = renavam;
	}

	public String getModel() {
		return model;
	}

	public void setModel( String model ) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor( String color ) {
		this.color = color;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand( String brand ) {
		this.brand = brand;
	}

	public String getOtherBrand() {
		return otherBrand;
	}

	public void setOtherBrand( String otherBrand ) {
		this.otherBrand = otherBrand;
	}

	public String getSpecie() {
		return specie;
	}

	public void setSpecie( String specie ) {
		this.specie = specie;
	}

	public Integer getCodeInfraction() {
		return codeInfraction;
	}

	public void setCodeInfraction( Integer codeInfraction ) {
		this.codeInfraction = codeInfraction;
	}

	public Integer getDeployment() {
		return deployment;
	}

	public void setDeployment( Integer deployment ) {
		this.deployment = deployment;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	public String getLegalSupport() {
		return legalSupport;
	}

	public void setLegalSupport( String legalSupport ) {
		this.legalSupport = legalSupport;
	}

	public String getPunishment() {
		return punishment;
	}

	public void setPunishment( String punishment ) {
		this.punishment = punishment;
	}

	public String getAdministrativeMeasure() {
		return administrativeMeasure;
	}

	public void setAdministrativeMeasure( String administrativeMeasure ) {
		this.administrativeMeasure = administrativeMeasure;
	}

	public Integer getMeasurementType() {
		return measurementType;
	}

	public void setMeasurementType( Integer measurementType ) {
		this.measurementType = measurementType;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations( String observations ) {
		this.observations = observations;
	}

	public String getCompetence() {
		return competence;
	}

	public void setCompetence( String competence ) {
		this.competence = competence;
	}

	public String getOtherInformation() {
		return otherInformation;
	}

	public void setOtherInformation( String otherInformation ) {
		this.otherInformation = otherInformation;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName( String driverName ) {
		this.driverName = driverName;
	}

	public String getDriverCpfRg() {
		return driverCpfRg;
	}

	public void setDriverCpfRg( String driverCpfRg ) {
		this.driverCpfRg = driverCpfRg;
	}

	public String getDriverCnh() {
		return driverCnh;
	}

	public void setDriverCnh( String driverCnh ) {
		this.driverCnh = driverCnh;
	}

	public String getStateCnh() {
		return stateCnh;
	}

	public void setStateCnh( String stateCnh ) {
		this.stateCnh = stateCnh;
	}

	public String getOffenderName() {
		return offenderName;
	}

	public void setOffenderName( String offenderName ) {
		this.offenderName = offenderName;
	}

	public String getOffenderCpfRg() {
		return offenderCpfRg;
	}

	public void setOffenderCpfRg( String offenderCpfRg ) {
		this.offenderCpfRg = offenderCpfRg;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet( String street ) {
		this.street = street;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber( Integer number ) {
		this.number = number;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood( String neighborhood ) {
		this.neighborhood = neighborhood;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement( String complement ) {
		this.complement = complement;
	}

	public String getMeasurementPerformed() {
		return measurementPerformed;
	}

	public void setMeasurementPerformed( String measurementPerformed ) {
		this.measurementPerformed = measurementPerformed;
	}

	public String getRegulatedLimit() {
		return regulatedLimit;
	}

	public void setRegulatedLimit( String regulatedLimit ) {
		this.regulatedLimit = regulatedLimit;
	}

	public String getMeasurementVerified() {
		return measurementVerified;
	}

	public void setMeasurementVerified( String measurementVerified ) {
		this.measurementVerified = measurementVerified;
	}

	public String getExceeded() {
		return exceeded;
	}

	public void setExceeded( String exceeded ) {
		this.exceeded = exceeded;
	}

	public String getEquipmentUsed() {
		return equipmentUsed;
	}

	public void setEquipmentUsed( String equipmentUsed ) {
		this.equipmentUsed = equipmentUsed;
	}

	public Cancel getCancel() {
		return cancel;
	}

	public void setCancel( Cancel cancel ) {
		this.cancel = cancel;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude( Double latitude ) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude( Double longitude ) {
		this.longitude = longitude;
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted( Boolean completed ) {
		this.completed = completed;
	}

	public Boolean getSent() {
		return sent;
	}

	public void setSent( Boolean sent ) {
		this.sent = sent;
	}

	public List<Image> getPhotos() {
		return getMany( Image.class, "fk_id_auto_infracao" );
	}

	public String getCompleteLocal() {
		StringBuilder builder = new StringBuilder();
		builder.append( getStreet() + ", " );
		builder.append( getNumber() + ", " );
		if ( getNeighborhood() != null && !getNeighborhood().equals( "" ) )
			builder.append( "Bairro " + getNeighborhood() + " - " );
		if ( getComplement() != null && !getComplement().equals( "" ) )
			builder.append( getComplement() + " - " );
		builder.append( getCity() + "/" ).append( getState() );
		return builder.toString();
	}

}
