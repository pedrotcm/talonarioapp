package br.com.opala.talonario.enums;

public class Urls {

	public static final String URL_BASE = "http://200.151.25.67:8282/talonario/";
	// public static final String URL_BASE =
	// "http://192.168.0.11:8080/talonario/";
	// public static final String URL_BASE =
	// "http://192.168.1.102:8080/talonario/";

	public static final String USER_LOGIN = URL_BASE + "ws/user/login";
	public static final String USER_LOGOUT = URL_BASE + "ws/user/logout";
	public static final String DATA_LOAD = URL_BASE + "ws/data/load";
	public static final String SEND_AUTOS = URL_BASE + "ws/auto/send";
	public static final String PACKAGE_LOAD = URL_BASE + "ws/package/load";
}
