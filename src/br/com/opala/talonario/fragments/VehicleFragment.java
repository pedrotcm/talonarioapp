package br.com.opala.talonario.fragments;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import br.com.opala.talonario.R;
import br.com.opala.talonario.activities.InitAutoFragmentActivity;
import br.com.opala.talonario.entities.AutoInfraction;
import br.com.opala.talonario.entities.Brand;
import br.com.opala.talonario.entities.Specie;
import br.com.opala.talonario.repositories.BrandRepository;
import br.com.opala.talonario.repositories.SpecieRepository;
import br.com.opala.talonario.services.LocationTrackerService;
import br.com.opala.talonario.utils.MaskedWatcher;
import br.com.opala.talonario.utils.ViewUtil;

public class VehicleFragment extends Fragment {

	private ViewPager mPager;

	private EditText et_plaque;
	private EditText et_plaqueNumber;
	private EditText et_renavam;
	private Spinner sp_specie;
	private Spinner sp_brand;
	private EditText et_model;
	private EditText et_color;
	private EditText et_otherBrand;

	private AutoInfraction autoInfraction;

	private TextView tv_brand;
	private TextView tv_specie;

	private ArrayAdapter<Brand> adapterBrand;

	private ArrayAdapter<Specie> adapterSpecie;

	public static String OPTION_OTHER;

	public void init( View rootView ) {
		et_plaque = (EditText) rootView.findViewById( R.id.et_plaque );
		InitAutoFragmentActivity.modifiedEditText( et_plaque );
		ViewUtil.checkErrorEditText( et_plaque );
		et_plaqueNumber = (EditText) rootView.findViewById( R.id.et_plaqueNumber );
		InitAutoFragmentActivity.modifiedEditText( et_plaqueNumber );
		ViewUtil.checkErrorEditText( et_plaqueNumber );
		et_plaque.setFilters( new InputFilter[] { new InputFilter() {
			@Override
			public CharSequence filter( CharSequence src, int start, int end, Spanned dst, int dstart, int dend ) {
				// if ( src.equals( "" ) ) { // for backspace
				// return src;
				// }
				if ( src.toString().matches( "[A-Z]+" ) ) {
					return src;
				}
				return "";
			}
		} } );

		et_renavam = (EditText) rootView.findViewById( R.id.et_renavam );
		InitAutoFragmentActivity.modifiedEditText( et_renavam );
		et_model = (EditText) rootView.findViewById( R.id.et_model );
		InitAutoFragmentActivity.modifiedEditText( et_model );
		et_color = (EditText) rootView.findViewById( R.id.et_color );
		InitAutoFragmentActivity.modifiedEditText( et_color );
		sp_brand = (Spinner) rootView.findViewById( R.id.sp_brand );
		sp_specie = (Spinner) rootView.findViewById( R.id.sp_specie );
		et_otherBrand = (EditText) rootView.findViewById( R.id.et_otherBrand );

		autoInfraction = InitAutoFragmentActivity.autoInfraction;
		mPager = InitAutoFragmentActivity.mPager;

		OPTION_OTHER = getResources().getString( R.string.option_other );

		// et_plaque.setInputType( InputType.TYPE_CLASS_TEXT |
		// InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS |
		// InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS );

		new MaskedWatcher( et_plaque, "###" );

		et_plaque.addTextChangedListener( new TextWatcher() {

			@Override
			public void onTextChanged( CharSequence s, int start, int before, int count ) {
				if ( s.length() == 3 && et_plaqueNumber.getText().toString().length() < 4 )
					et_plaqueNumber.requestFocus();
				if ( s.length() == 3 && et_plaqueNumber.getText().toString().length() == 4 )
					ViewUtil.hideKeyboard( getActivity(), et_plaque );
			}

			@Override
			public void beforeTextChanged( CharSequence s, int start, int count, int after ) {}

			@Override
			public void afterTextChanged( Editable s ) {}
		} );

		et_plaqueNumber.addTextChangedListener( new TextWatcher() {

			@Override
			public void onTextChanged( CharSequence s, int start, int before, int count ) {
				if ( s.length() == 4 )
					ViewUtil.hideKeyboard( getActivity(), et_plaqueNumber );
			}

			@Override
			public void beforeTextChanged( CharSequence s, int start, int count, int after ) {}

			@Override
			public void afterTextChanged( Editable s ) {}
		} );
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		super.onCreateView( inflater, container, savedInstanceState );
		setHasOptionsMenu( true );

		View rootView = inflater.inflate( R.layout.fragment_vehicle, container, false );
		init( rootView );

		List<Specie> species = SpecieRepository.getAllOrderByOrder();
		species.add( 0, new Specie( "Selecione uma espécie" ) );
		List<Brand> brands = BrandRepository.getAllOrderByName();
		brands.add( 0, new Brand( "Selecione uma marca" ) );

		adapterBrand = new ArrayAdapter<Brand>( getActivity(), android.R.layout.simple_spinner_dropdown_item, brands );

		sp_brand.setAdapter( adapterBrand );
		sp_brand.setOnItemSelectedListener( new OnItemSelectedListener() {

			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {
				Brand selected = (Brand) parent.getSelectedItem();
				if ( position != 0 )
					autoInfraction.setBrand( selected.getDescription() );
				if ( selected.getDescription().equalsIgnoreCase( OPTION_OTHER ) ) {
					et_otherBrand.setText( null );
					if ( autoInfraction.getOtherBrand() != null )
						et_otherBrand.setText( autoInfraction.getOtherBrand() );
					et_otherBrand.setVisibility( View.VISIBLE );
				} else {
					autoInfraction.setOtherBrand( null );
					et_otherBrand.setError( null );
					et_otherBrand.setVisibility( View.GONE );
				}

				if ( !selected.getDescription().equalsIgnoreCase( "Selecione uma marca" ) )
					InitAutoFragmentActivity.wasModified = true;

				tv_brand = (TextView) parent.getSelectedView();
			}

			@Override
			public void onNothingSelected( AdapterView<?> parent ) {

			}
		} );

		adapterSpecie = new ArrayAdapter<Specie>( getActivity(), android.R.layout.simple_spinner_dropdown_item, species );
		sp_specie.setAdapter( adapterSpecie );
		sp_specie.setOnItemSelectedListener( new OnItemSelectedListener() {

			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {
				Specie selected = (Specie) parent.getSelectedItem();
				if ( position != 0 )
					autoInfraction.setSpecie( selected.getDescription() );

				if ( !selected.getDescription().equalsIgnoreCase( "Selecione uma espécie" ) )
					InitAutoFragmentActivity.wasModified = true;

				tv_specie = (TextView) view;

			}

			@Override
			public void onNothingSelected( AdapterView<?> parent ) {

			}
		} );

		// et_otherBrand.addTextChangedListener( new TextWatcher() {
		//
		// @Override
		// public void onTextChanged( CharSequence s, int start, int before, int
		// count ) {
		// if ( s != null && s.length() > 0 && et_otherBrand.getError() != null
		// )
		// et_otherBrand.setError( null );
		// }
		//
		// @Override
		// public void beforeTextChanged( CharSequence s, int start, int count,
		// int after ) {}
		//
		// @Override
		// public void afterTextChanged( Editable s ) {}
		// } );
		ViewUtil.checkErrorEditText( et_otherBrand );

		return rootView;

	}

	@Override
	public void onCreateOptionsMenu( Menu menu, MenuInflater inflater ) {
		inflater = getActivity().getMenuInflater();
		inflater.inflate( R.menu.menu_next, menu );
		super.onCreateOptionsMenu( menu, inflater );
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item ) {
		InitAutoFragmentActivity.hideKeyboard();
		switch ( item.getItemId() ) {
			case R.id.action_next:
				if ( saveValues() ) {
					mPager.setCurrentItem( mPager.getCurrentItem() + 1 );
					getActivity().setTitle( getResources().getString( R.string.title_infraction ) );

					if ( InitAutoFragmentActivity.location.getLocation() != null && InitAutoFragmentActivity.location.canGetLocation() ) {
						autoInfraction.setLatitude( InitAutoFragmentActivity.location.getLatitude() );
						autoInfraction.setLongitude( InitAutoFragmentActivity.location.getLongitude() );
					}

					if ( ( InitAutoFragmentActivity.idAutoRecovered == null || InitAutoFragmentActivity.idAutoRecovered == 0 ) && autoInfraction.getStreet() == null && LocalFragment.et_street.getText().toString().equals( "" ) && autoInfraction.getLatitude() != null && autoInfraction.getLongitude() != null && !LocationTrackerService.obtainedAddress )
						LocationTrackerService.getAddress( autoInfraction.getLatitude().toString(), autoInfraction.getLongitude().toString(), LocalFragment.et_street, LocalFragment.et_number, LocalFragment.et_neighborhood );

				}
				// fillValues();
				// mPager.setCurrentItem( mPager.getCurrentItem() + 1 );
				// getActivity().setTitle( getResources().getString(
				// R.string.title_infraction ) );
				return true;
			case R.id.action_cancel:
				InitAutoFragmentActivity.dialogCancel();
				return true;
			case android.R.id.home:
				mPager.setCurrentItem( mPager.getCurrentItem() - 1 );
				getActivity().setTitle( getResources().getString( R.string.title_photo ) );
				getActivity().getActionBar().setDisplayHomeAsUpEnabled( false );
				getActivity().getActionBar().setLogo( R.drawable.ic_bar_app );
				return true;
			default:
				return super.onOptionsItemSelected( item );
		}
	}

	private boolean saveValues() {
		String plaque = et_plaque.getText().toString() + "-" + et_plaqueNumber.getText().toString();
		if ( plaque.equals( "" ) ) {
			// et_plaque.requestFocus();
			et_plaque.setError( "" );
			Toast toast = Toast.makeText( getActivity(), getResources().getString( R.string.error_required ), Toast.LENGTH_LONG );
			toast.setGravity( Gravity.CENTER, 0, 0 );
			toast.show();
			return false;
		} else {
			if ( !et_plaque.getText().toString().matches( "^[A-Z]{3}$" ) ) {
				et_plaque.requestFocus();
				et_plaque.setError( getResources().getString( R.string.error_plaque_invalid ) );
				return false;
			} else if ( !et_plaqueNumber.getText().toString().matches( "^\\d{4}$" ) ) {
				et_plaqueNumber.requestFocus();
				et_plaqueNumber.setError( getResources().getString( R.string.error_plaque_invalid ) );
				return false;
			}

		}

		// if ( et_renavam.getText().toString().equals( "" ) ) {
		// et_renavam.requestFocus();
		// et_renavam.setError( getResources().getString(
		// R.string.error_required ) );
		// return false;
		// } else if ( !et_renavam.getText().toString().matches( "^\\d{11}$" ) )
		// {
		// et_renavam.requestFocus();
		// et_renavam.setError( getResources().getString(
		// R.string.error_digits_invalid ) );
		// return false;
		// }

		if ( autoInfraction.getBrand() == null ) {
			tv_brand.setError( "" );
			Toast toast = Toast.makeText( getActivity(), getResources().getString( R.string.error_required ), Toast.LENGTH_LONG );
			toast.setGravity( Gravity.CENTER, 0, 0 );
			toast.show();
			return false;
		} else if ( tv_brand.getText().toString().equalsIgnoreCase( OPTION_OTHER ) ) {
			if ( et_otherBrand.getText().toString().equals( "" ) ) {
				et_otherBrand.requestFocus();
				et_otherBrand.setError( getResources().getString( R.string.error_required ) );
				return false;
			}
		}

		if ( autoInfraction.getSpecie() == null ) {
			tv_specie.setError( "" );
			Toast toast = Toast.makeText( getActivity(), getResources().getString( R.string.error_required ), Toast.LENGTH_LONG );
			toast.setGravity( Gravity.CENTER, 0, 0 );
			toast.show();
			return false;
		}

		completeSaveValues();
		autoInfraction.save();
		return true;
	}

	public void completeSaveValues() {
		autoInfraction.setPlaque( et_plaque.getText().toString() + "-" + et_plaqueNumber.getText().toString() );

		if ( !et_renavam.getText().toString().equals( "" ) )
			autoInfraction.setRenavam( et_renavam.getText().toString() );
		if ( !et_model.getText().toString().equals( "" ) )
			autoInfraction.setModel( et_model.getText().toString() );
		if ( !et_color.getText().toString().equals( "" ) )
			autoInfraction.setColor( et_color.getText().toString() );

		if ( autoInfraction.getBrand().equalsIgnoreCase( OPTION_OTHER ) )
			autoInfraction.setOtherBrand( et_otherBrand.getText().toString() );

	}

	@Override
	public void onViewCreated( View view, Bundle savedInstanceState ) {
		super.onViewCreated( view, savedInstanceState );
		if ( autoInfraction.getPlaque() != null ) {
			if ( autoInfraction.getPlaque().length() >= 3 )
				et_plaque.setText( autoInfraction.getPlaque().substring( 0, 3 ) );
			if ( autoInfraction.getPlaque().length() == 8 )
				et_plaqueNumber.setText( autoInfraction.getPlaque().substring( 4, 8 ) );
		}

		et_renavam.setText( autoInfraction.getRenavam() );
		if ( autoInfraction.getBrand() != null ) {
			Brand brand = BrandRepository.getBrandByDescription( autoInfraction.getBrand() );
			int brandPosition = adapterBrand.getPosition( brand );
			sp_brand.setSelection( brandPosition );
			if ( autoInfraction.getBrand().equalsIgnoreCase( OPTION_OTHER ) )
				et_otherBrand.setText( autoInfraction.getOtherBrand() );
		}
		if ( autoInfraction.getSpecie() != null ) {
			Specie specie = SpecieRepository.getSpecieByDescription( autoInfraction.getSpecie() );
			int speciePosition = adapterSpecie.getPosition( specie );
			sp_specie.setSelection( speciePosition );
		}
		et_model.setText( autoInfraction.getModel() );
		et_color.setText( autoInfraction.getColor() );
	}
}
