package br.com.opala.talonario.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import br.com.opala.talonario.R;

public class CancelFragment extends Fragment {

	@Override
	public void onViewCreated( View view, Bundle savedInstanceState ) {
		super.onViewCreated( view, savedInstanceState );
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService( Context.INPUT_METHOD_SERVICE );
		imm.hideSoftInputFromWindow( view.getWindowToken(), 0 );
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		setHasOptionsMenu( true );

		View rootView = inflater.inflate( R.layout.dialog_cancel, container, false );
		getActivity().setTitle( getResources().getString( R.string.title_cancel ) );
		getActivity().getActionBar().setDisplayHomeAsUpEnabled( true );

		Spinner sp_reason = (Spinner) rootView.findViewById( R.id.sp_reason );

		List<String> loadMarque = new ArrayList<>();
		loadMarque.add( "Selecione um Motivo" );
		for ( int i = 1; i <= 10; i++ )
			loadMarque.add( "Motivo " + i );
		ArrayAdapter<String> adapterReason = new ArrayAdapter<String>( getActivity(), android.R.layout.simple_spinner_dropdown_item, loadMarque );
		sp_reason.setAdapter( adapterReason );

		return rootView;
	}

	@Override
	public void onCreateOptionsMenu( Menu menu, MenuInflater inflater ) {
		inflater = getActivity().getMenuInflater();
		inflater.inflate( R.menu.menu_next, menu );
		super.onCreateOptionsMenu( menu, inflater );
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item ) {
		switch ( item.getItemId() ) {
			case R.id.action_accept:
				getActivity().finish();
				return true;
			case android.R.id.home:
				getFragmentManager().popBackStack();
				return true;
			default:
				return super.onOptionsItemSelected( item );
		}
	}
}
