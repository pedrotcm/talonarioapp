package br.com.opala.talonario.fragments;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import br.com.opala.talonario.R;
import br.com.opala.talonario.activities.InitAutoFragmentActivity;
import br.com.opala.talonario.adapters.PhotoAdapter;
import br.com.opala.talonario.entities.AutoInfraction;
import br.com.opala.talonario.entities.Image;
import br.com.opala.talonario.repositories.SettingParameterRepository;
import br.com.opala.talonario.services.LocationTrackerService;
import br.com.opala.talonario.utils.ViewUtil;

public class PhotoFragment extends Fragment {

	private ViewPager mPager;
	private LinearLayout bt_addPhoto;
	private ListView lv_photos;

	private AutoInfraction autoInfraction;

	private Uri mImageCaptureUri;
	private PhotoAdapter adapterPhoto;
	private static final int PICK_FROM_CAMERA = 1;
	private static final int PICK_FROM_FILE = 2;
	protected static int MAX_PHOTOS;
	private Date dateImagePicked;
	private int countImageWithDate;
	public static boolean haveImageWithDate;
	public static Image imageInformations;

	public void init( View rootView ) {
		mPager = InitAutoFragmentActivity.mPager;
		autoInfraction = InitAutoFragmentActivity.autoInfraction;

		bt_addPhoto = (LinearLayout) rootView.findViewById( R.id.bt_addPhoto );
		lv_photos = (ListView) rootView.findViewById( R.id.lv_photos );

		MAX_PHOTOS = SettingParameterRepository.getQntPhotos();
		countImageWithDate = 0;
		haveImageWithDate = false;
		imageInformations = null;
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		super.onCreateView( inflater, container, savedInstanceState );
		setHasOptionsMenu( true );
		getActivity().setTitle( getResources().getString( R.string.title_photo ) );

		View rootView = inflater.inflate( R.layout.fragment_photo, container, false );
		init( rootView );

		bt_addPhoto.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				if ( autoInfraction.getId() == null || autoInfraction.getPhotos().size() < MAX_PHOTOS )
					dialogPhoto();
				else {
					Toast toast = Toast.makeText( getActivity(), "Máximo: " + MAX_PHOTOS + " fotos", Toast.LENGTH_LONG );
					toast.setGravity( Gravity.CENTER, 0, 0 );
					toast.show();
				}
			}
		} );

		lv_photos.setOnItemLongClickListener( new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick( AdapterView<?> parent, View view, int position, long id ) {
				Image photo = adapterPhoto.getItem( position );
				adapterPhoto.remove( photo );
				// adapterPhoto.notifyDataSetChanged();

				if ( imageInformations != null ) {
					if ( photo.getPath().equalsIgnoreCase( imageInformations.getPath() ) ) {
						imageInformations = null;
						LocalFragment.clearAddressImage();
					}
				}

				String path = photo.getPath();
				File file = new File( path );
				photo.delete();
				// deleteAndRefresh( file.getAbsolutePath() );

				if ( path.contains( "Talonário" ) ) {
					countImageWithDate = countImageWithDate - 1;
					System.out.println( countImageWithDate );
					if ( countImageWithDate <= 0 ) {
						countImageWithDate = 0;
						LocalFragment.bt_useInformations.setVisibility( View.GONE );
					} else {
						for ( Image image : autoInfraction.getPhotos() ) {
							if ( image.getPath().contains( "Talonário" ) ) {
								if ( checkImagesWithInformations( image, false ) )
									break;
							}
						}
					}
				}

				adapterPhoto = new PhotoAdapter( getActivity(), R.layout.row_photo, autoInfraction.getPhotos() );
				lv_photos.setAdapter( adapterPhoto );
				ViewUtil.setListViewHeightBasedOnChildren( lv_photos );

				return false;
			}
		} );

		return rootView;
	}

	@Override
	public void onCreateOptionsMenu( Menu menu, MenuInflater inflater ) {
		inflater = getActivity().getMenuInflater();
		inflater.inflate( R.menu.menu_next, menu );
		super.onCreateOptionsMenu( menu, inflater );
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item ) {
		InitAutoFragmentActivity.hideKeyboard();
		switch ( item.getItemId() ) {
			case R.id.action_next:
				mPager.setCurrentItem( mPager.getCurrentItem() + 1 );
				getActivity().setTitle( getResources().getString( R.string.title_vehicle ) );
				getActivity().getActionBar().setDisplayHomeAsUpEnabled( true );
				getActivity().getActionBar().setLogo( R.drawable.ic_action_previous );

				if ( InitAutoFragmentActivity.location.getLocation() != null && InitAutoFragmentActivity.location.canGetLocation() ) {
					autoInfraction.setLatitude( InitAutoFragmentActivity.location.getLatitude() );
					autoInfraction.setLongitude( InitAutoFragmentActivity.location.getLongitude() );
				}

				if ( ( InitAutoFragmentActivity.idAutoRecovered == null || InitAutoFragmentActivity.idAutoRecovered == 0 ) && autoInfraction.getStreet() == null && LocalFragment.et_street.getText().toString().equals( "" ) && autoInfraction.getLatitude() != null && autoInfraction.getLongitude() != null && !LocationTrackerService.obtainedAddress )
					LocationTrackerService.getAddress( autoInfraction.getLatitude().toString(), autoInfraction.getLongitude().toString(), LocalFragment.et_street, LocalFragment.et_number, LocalFragment.et_neighborhood );

				return true;
			case R.id.action_cancel:
				InitAutoFragmentActivity.dialogCancel();
				return true;
			default:
				return super.onOptionsItemSelected( item );
		}
	}

	private void dialogPhoto() {

		final String[] items = new String[] { "Câmera", "Galeria" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>( getActivity(), android.R.layout.select_dialog_item, items );
		AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );

		builder.setTitle( "Escolha uma opcão" );
		builder.setAdapter( adapter, new DialogInterface.OnClickListener() {

			@Override
			public void onClick( DialogInterface dialog, int item ) {
				if ( item == 0 ) {

					// MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA
					Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
					File dir = new File( Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES ), "Talonário" );
					if ( !dir.exists() ) {
						dir.mkdirs();
					}

					LocationTrackerService location = new LocationTrackerService( getActivity() );
					Double latitude = null;
					Double longitude = null;
					if ( location.canGetLocation() ) {
						latitude = location.getLatitude();
						longitude = location.getLongitude();
					}
					location.stopUsingLocation();

					dateImagePicked = new Date();
					String creationDate = new SimpleDateFormat( "yyyyMMdd_HHmmss" ).format( dateImagePicked );
					String path;
					if ( latitude != null && longitude != null ) {
						path = dir.getPath() + "/" + creationDate + "#" + Math.abs( latitude ) + "_" + Math.abs( longitude ) + ".jpg";
					} else
						path = dir.getPath() + "/" + creationDate + ".jpg";
					mImageCaptureUri = Uri.fromFile( new File( path ) );

					try {
						intent.putExtra( MediaStore.EXTRA_OUTPUT, mImageCaptureUri );
						intent.putExtra( "return-data", true );

						startActivityForResult( intent, PICK_FROM_CAMERA );
					} catch ( Exception e ) {
						e.printStackTrace();
					}

					dialog.cancel();
				} else {

					Intent intent = new Intent();

					intent.setType( "image/*" );
					intent.setAction( Intent.ACTION_GET_CONTENT );

					startActivityForResult( Intent.createChooser( intent, "Localize a foto" ), PICK_FROM_FILE );

				}
			}
		} );

		final AlertDialog dialog = builder.create();
		// dialog.setCanceledOnTouchOutside( true );

		dialog.show();

	}

	@Override
	public void onActivityResult( int requestCode, int resultCode, Intent data ) {
		if ( resultCode != getActivity().RESULT_OK )
			return;

		InitAutoFragmentActivity.wasModified = true;
		Bitmap bitmap = null;
		String path = "";
		Uri fileUri = null;

		if ( requestCode == PICK_FROM_FILE ) {
			mImageCaptureUri = data.getData();
			path = getRealPathFromURI( mImageCaptureUri ); // from Gallery

			if ( path == null )
				path = mImageCaptureUri.getPath(); // from File Manager

			if ( path != null )
				bitmap = BitmapFactory.decodeFile( path );

			fileUri = Uri.fromFile( new File( path ) );
			if ( !path.contains( "Talonário" ) )
				bitmap = adjustPhoto( bitmap, fileUri, path, true );

		} else {
			path = mImageCaptureUri.getPath();
			bitmap = BitmapFactory.decodeFile( path );

			fileUri = Uri.fromFile( new File( path ) );
			bitmap = adjustPhoto( bitmap, fileUri, path, false );
		}

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		// Bitmap byteBitmap = bitmap.copy( Bitmap.Config.ARGB_8888, false );
		bitmap.compress( CompressFormat.PNG, 100, byteArrayOutputStream );
		byte[] array = byteArrayOutputStream.toByteArray();

		Image photo = new Image();
		photo.setImage( array );
		photo.setPath( path );
		photo.setExtension( path.substring( path.lastIndexOf( "." ) ) );
		photo.setAutoInfraction( autoInfraction );
		photo.setCreatedAt( dateImagePicked );
		photo.save();

		checkImagesWithInformations( photo, true );

		// AddMedia
		updateGallery( path );

		adapterPhoto = new PhotoAdapter( getActivity(), R.layout.row_photo, autoInfraction.getPhotos() );
		lv_photos.setAdapter( adapterPhoto );
		ViewUtil.setListViewHeightBasedOnChildren( lv_photos );

	}

	private boolean checkImagesWithInformations( Image photo, boolean count ) {
		File file = new File( photo.getPath() );
		if ( photo.getPath().contains( "Talonário" ) ) {
			String dateTime = photo.getPath().replace( file.getParent() + File.separator, "" ).replace( photo.getExtension(), "" );
			Date dateCreated = null;
			try {
				if ( dateTime.contains( "#" ) ) {
					String date = dateTime.split( "#" )[0];
					dateCreated = new SimpleDateFormat( "yyyyMMdd_HHmmss" ).parse( date );
					String latLng = dateTime.split( "#" )[1];
					Double latitude = Double.valueOf( latLng.split( "_" )[0] ) * -1;
					Double longitude = Double.valueOf( latLng.split( "_" )[1] ) * -1;
					LocationTrackerService.getAddress( latitude.toString(), longitude.toString(), photo );
					// if ( address != null && !address.isEmpty() )
					// photo.setAddress( address );
				} else {
					dateCreated = new SimpleDateFormat( "yyyyMMdd_HHmmss" ).parse( dateTime );
				}
			} catch ( ParseException e ) {
				e.printStackTrace();
			}

			if ( dateCreated != null ) {
				photo.setCreatedAt( dateCreated );
				if ( imageInformations == null )
					imageInformations = photo;
				haveImageWithDate = true;
				if ( count )
					countImageWithDate = countImageWithDate + 1;
				if ( LocalFragment.bt_useInformations != null )
					LocalFragment.bt_useInformations.setVisibility( View.VISIBLE );
				return true;
			}
			return false;
		}
		return false;
	}

	private void updateGallery( String path ) {
		getActivity().sendBroadcast( new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile( new File( path ) ) ) );
	}

	private String getRealPathFromURI( Uri contentUri ) {
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = getActivity().getContentResolver().query( contentUri, proj, null, null, null );

		if ( cursor == null )
			return null;

		int column_index = cursor.getColumnIndexOrThrow( MediaStore.Images.Media.DATA );

		cursor.moveToFirst();

		return cursor.getString( column_index );
	}

	private Bitmap adjustPhoto( Bitmap bitmap, Uri fileUri, String path, boolean bitmapGallery ) {

		getActivity().getContentResolver().notifyChange( fileUri, null );
		ContentResolver cr = getActivity().getContentResolver();

		// Bitmap bitmap = bitmapCaptured;
		int w = 0;
		int h = 0;

		float angle = 0;
		try {

			// if ( bitmapGallery )
			// bitmap = android.provider.MediaStore.Images.Media.getBitmap( cr,
			// fileUri );
			if ( !bitmapGallery )
				bitmap = readBitmap( fileUri );
			// captura as dimens�es da imagem

			// pega o caminho onda a imagem est� salva
			ExifInterface exif = new ExifInterface( path );
			// pega a orienta��o real da imagem

			int orientation = exif.getAttributeInt( ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL );
			// gira a imagem de acordo com a orienta��o

			switch ( orientation ) {

				case 3: // ORIENTATION_ROTATE_180
					angle = 180;
					break;
				case 6: // ORIENTATION_ROTATE_90
					angle = 90;
					break;
				case 8: // ORIENTATION_ROTATE_270
					angle = 270;
					break;
				default: // ORIENTATION_ROTATE_0
					angle = 0;
					break;
			}

		} catch ( FileNotFoundException e1 ) {
			e1.printStackTrace();
		} catch ( IOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		FileOutputStream out;
		Bitmap rotatedBitmap = null;
		Bitmap mutableBitmap = null;
		try {
			out = new FileOutputStream( path );
			// define um indice = 1 pois se der erro vai manter a imagem como
			// est�.
			// Integer idx = 1;
			// reupera as dimens�es da imagem
			w = bitmap.getWidth();
			h = bitmap.getHeight();
			// verifica qual a maior dimens�o e divide pela lateral final para
			// definir qual o indice de redu��o

			if ( w > h ) {
				if ( w > 800 ) {
					w = 800;
				}

				if ( h > 600 ) {
					h = 600;
				}

			} else {
				if ( h > 800 ) {
					h = 800;
				}

				if ( w > 600 ) {
					w = 600;
				}
			}

			// scale it to fit the screen, x and y swapped because my image is
			// wider than it is tall
			Bitmap scaledBitmap = Bitmap.createScaledBitmap( bitmap, w, h, true );

			// create a matrix object
			Matrix matrix = new Matrix();
			matrix.postRotate( angle ); // rotate by angle

			// create a new bitmap from the original using the matrix to
			// transform the result
			rotatedBitmap = Bitmap.createBitmap( scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true );
			if ( !bitmapGallery ) {
				SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy HH:mm" );
				String dateTime = sdf.format( dateImagePicked );
				mutableBitmap = rotatedBitmap.copy( Bitmap.Config.ARGB_8888, true );
				Canvas cs = new Canvas( mutableBitmap );
				Paint tPaint = new Paint();
				tPaint.setTextSize( 20 );
				tPaint.setColor( Color.YELLOW );
				tPaint.setStyle( Style.FILL );
				float height = tPaint.measureText( "yY" );
				cs.drawText( dateTime, 20f, height + 15f, tPaint );
				cs.drawBitmap( mutableBitmap, 0, 0, tPaint );

				// salva a imagem reduzida no disco
				mutableBitmap.compress( Bitmap.CompressFormat.PNG, 100, out );
				return mutableBitmap;
			}
			rotatedBitmap.compress( Bitmap.CompressFormat.PNG, 100, out );

		} catch ( FileNotFoundException e ) {
			e.printStackTrace();
		}
		// uma nova instancia do bitmap rotacionado
		return rotatedBitmap;
	}

	@Override
	public void onViewCreated( View view, Bundle savedInstanceState ) {
		super.onViewCreated( view, savedInstanceState );
		for ( Image image : autoInfraction.getPhotos() ) {
			checkImagesWithInformations( image, true );
			break;
		}
		adapterPhoto = new PhotoAdapter( getActivity(), R.layout.row_photo, autoInfraction.getPhotos() );
		lv_photos.setAdapter( adapterPhoto );
		ViewUtil.setListViewHeightBasedOnChildren( lv_photos );
	}

	private void deleteAndRefresh( String absolutPath ) {
		// Set up the projection (we only need the ID)
		String[] projection = { MediaStore.Images.Media._ID };

		// Match on the file path
		String selection = MediaStore.Images.Media.DATA + " = ?";
		String[] selectionArgs = new String[] { absolutPath };

		// Query for the ID of the media matching the file path
		Uri queryUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
		ContentResolver contentResolver = getActivity().getContentResolver();
		Cursor c = contentResolver.query( queryUri, projection, selection, selectionArgs, null );
		if ( c.moveToFirst() ) {
			// We found the ID. Deleting the item via the content
			// provider will also remove the file
			long idImage = c.getLong( c.getColumnIndexOrThrow( MediaStore.Images.Media._ID ) );
			Uri deleteUri = ContentUris.withAppendedId( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, idImage );
			contentResolver.delete( deleteUri, null, null );
		} else {
			// File not found in media store DB
		}
		c.close();
	}

	private Bitmap readBitmap( Uri selectedImage ) {
		Bitmap bm = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 4;
		AssetFileDescriptor fileDescriptor = null;
		try {
			fileDescriptor = getActivity().getContentResolver().openAssetFileDescriptor( selectedImage, "r" );
		} catch ( FileNotFoundException e ) {
			e.printStackTrace();
		} finally {
			try {
				bm = BitmapFactory.decodeFileDescriptor( fileDescriptor.getFileDescriptor(), null, options );
				fileDescriptor.close();
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
		return bm;
	}

	public static void clearBitmap( Bitmap bm ) {

		bm.recycle();
		System.gc();

	}

}
