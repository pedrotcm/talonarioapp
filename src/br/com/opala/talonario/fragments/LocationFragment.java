package br.com.opala.talonario.fragments;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import br.com.opala.talonario.R;
import br.com.opala.talonario.activities.InitAutoFragmentActivity;
import br.com.opala.talonario.entities.AutoInfraction;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

public class LocationFragment extends Fragment {

	private static View view;
	/**
	 * Note that this may be null if the Google Play services APK is not
	 * available.
	 */

	private GoogleMap mMap;
	private Double latitude, longitude;
	private AutoInfraction autoInfraction;

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		if ( container == null ) {
			return null;
		}
		view = inflater.inflate( R.layout.map_fragment, container, false );
		// Passing harcoded values for latitude & longitude. Please change as
		// per your need. This is just used to drop a Marker on the Map

		setUpMapIfNeeded(); // For setting up the MapFragment

		autoInfraction = InitAutoFragmentActivity.autoInfraction;

		return view;
	}

	/***** Sets up the map if it is possible to do so *****/
	public void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if ( mMap == null ) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ( (SupportMapFragment) InitAutoFragmentActivity.fm.findFragmentById( R.id.location_map ) ).getMap();
			// Check if we were successful in obtaining the map.
			if ( mMap != null )
				setUpMap();
		}
	}

	/**
	 * This is where we can add markers or lines, add listeners or move the
	 * camera.
	 * <p>
	 * This should only be called once and when we are sure that {@link #mMap}
	 * is not null.
	 */
	private void setUpMap() {
		// For showing a move to my loction button
		mMap.setMyLocationEnabled( true );

		mMap.setOnMyLocationChangeListener( new GoogleMap.OnMyLocationChangeListener() {
			boolean findLatLng = true;

			@Override
			public void onMyLocationChange( Location arg0 ) {
				if ( findLatLng ) {
					latitude = arg0.getLatitude();
					longitude = arg0.getLongitude();
					autoInfraction.setLatitude( latitude );
					autoInfraction.setLongitude( longitude );

					findLatLng = false;
				}
			}
		} );
	}

	@Override
	public void onViewCreated( View view, Bundle savedInstanceState ) {
		// TODO Auto-generated method stub
		if ( mMap != null )
			setUpMap();

		if ( mMap == null ) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ( (SupportMapFragment) InitAutoFragmentActivity.fm.findFragmentById( R.id.location_map ) ).getMap();
			// Check if we were successful in obtaining the map.
			if ( mMap != null )
				setUpMap();
		}
	}

	/****
	 * The mapfragment's id must be removed from the FragmentManager or else if
	 * the same it is passed on the next time then app will crash
	 ****/
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if ( mMap != null ) {
			InitAutoFragmentActivity.mPager.setOffscreenPageLimit( 4 );
			InitAutoFragmentActivity.mPager.getAdapter().notifyDataSetChanged();
			mMap = null;
		}
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

}