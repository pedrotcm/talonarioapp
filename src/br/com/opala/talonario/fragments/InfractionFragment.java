package br.com.opala.talonario.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Toast;
import br.com.opala.talonario.R;
import br.com.opala.talonario.activities.InitAutoFragmentActivity;
import br.com.opala.talonario.adapters.AutoCompleteInfractionAdapter;
import br.com.opala.talonario.adapters.DriverOffenderAdapter;
import br.com.opala.talonario.adapters.InfractionAdapter;
import br.com.opala.talonario.adapters.MeasurementAdapter;
import br.com.opala.talonario.adapters.ObservationsAdapter;
import br.com.opala.talonario.entities.AutoInfraction;
import br.com.opala.talonario.entities.DriverOffenderVO;
import br.com.opala.talonario.entities.Infraction;
import br.com.opala.talonario.entities.MeasurementType;
import br.com.opala.talonario.repositories.InfractionRepository;
import br.com.opala.talonario.repositories.MeasurementTypeRepository;
import br.com.opala.talonario.services.LocationTrackerService;
import br.com.opala.talonario.utils.ViewUtil;

public class InfractionFragment extends Fragment {

	private ViewPager mPager;
	private EditText et_infraction;
	private EditText et_setPunishment;
	private EditText et_setAdministrativeMeasure;
	private EditText et_observations;
	private EditText et_otherInformation;

	private RadioGroup rg_driver_offender;
	private EditText et_nameDriverOffender;
	private EditText et_cpfRgDriverOffender;
	private EditText et_cnhPermissionDriver;
	private EditText et_ufCnh;

	private EditText et_nameOffender;
	private EditText et_cpfRgOffender;

	private ListView lv_driver;
	private ListView lv_offender;

	private Infraction infraction;
	private int infractionPosition;

	private AutoInfraction autoInfraction;
	private List<Infraction> infractions;
	private DriverOffenderVO driverOffender;
	private DriverOffenderAdapter adapterDriver;
	private DriverOffenderAdapter adapterOffender;
	private List<DriverOffenderVO> listOffender;
	private List<DriverOffenderVO> listDriver;
	private LinearLayout ll_driver;
	private LinearLayout ll_offender;
	private LinearLayout bt_addDriverOffender;
	private ImageView iv_addDriverOffender;

	private ListView lv_observations;
	private ArrayList<String> observations;
	private EditText et_observations_extras;
	private ArrayList<String> checkedsTemp;
	private String observationsTemp;

	private LinearLayout bt_selectInfraction;
	private OnClickListener clickListener;

	private ImageView iv_addMeasurement;
	private MeasurementAdapter adapterMeasurement;
	private LinearLayout bt_addMeasurement;
	private ListView lv_measurement;

	public void init( View rootView ) {
		infraction = new Infraction();
		bt_addDriverOffender = (LinearLayout) rootView.findViewById( R.id.bt_addDriverOffender );
		iv_addDriverOffender = (ImageView) rootView.findViewById( R.id.iv_addDriver );
		// bt_addOffender = (LinearLayout) rootView.findViewById(
		// R.id.bt_addOffender );
		// tv_btnOffender = (TextView) rootView.findViewById(
		// R.id.tv_btnOffender );
		// iv_addOffender = (ImageView) rootView.findViewById(
		// R.id.iv_addOffender );

		bt_selectInfraction = (LinearLayout) rootView.findViewById( R.id.bt_selectInfraction );
		bt_addMeasurement = (LinearLayout) rootView.findViewById( R.id.bt_addMeasurement );

		et_infraction = (EditText) rootView.findViewById( R.id.et_infraction );
		et_setPunishment = (EditText) rootView.findViewById( R.id.et_setPunishment );
		et_setAdministrativeMeasure = (EditText) rootView.findViewById( R.id.et_setAdministrativeMeasure );
		et_observations = (EditText) rootView.findViewById( R.id.et_observations );
		et_observations.setFocusable( false );
		InitAutoFragmentActivity.modifiedEditText( et_observations );
		et_otherInformation = (EditText) rootView.findViewById( R.id.et_otherInformation );
		InitAutoFragmentActivity.modifiedEditText( et_otherInformation );

		lv_driver = (ListView) rootView.findViewById( R.id.lv_driver );
		lv_offender = (ListView) rootView.findViewById( R.id.lv_offender );

		iv_addMeasurement = (ImageView) rootView.findViewById( R.id.iv_addMeasurement );
		lv_measurement = (ListView) rootView.findViewById( R.id.lv_measurement );

		mPager = InitAutoFragmentActivity.mPager;
		autoInfraction = InitAutoFragmentActivity.autoInfraction;

		listOffender = new ArrayList<>();
		listDriver = new ArrayList<>();

		infractions = InfractionRepository.getInfractionsByCompetence( InitAutoFragmentActivity.organ.getCompetence() );
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		super.onCreateView( inflater, container, savedInstanceState );
		setHasOptionsMenu( true );

		View rootView = inflater.inflate( R.layout.fragment_infraction, container, false );
		init( rootView );

		bt_selectInfraction.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				InfractionDialog dialog = new InfractionDialog();
				dialog.show( getFragmentManager(), "dialogInfraction" );
			}
		} );

		bt_addMeasurement.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				dialogMeasurement();
			}
		} );

		bt_addDriverOffender.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				dialogAddDriverOffender();
			}
		} );

		lv_driver.setOnItemLongClickListener( new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick( AdapterView<?> parent, View view, int position, long id ) {
				adapterDriver.remove( adapterDriver.getItem( position ) );
				adapterDriver.notifyDataSetChanged();
				// tv_btnDriver.setText( getResources().getString(
				// R.string.bt_addDriver ) );
				if ( lv_offender.getAdapter().getCount() == 0 )
					iv_addDriverOffender.setBackgroundResource( R.drawable.ic_btn_add );
				autoInfraction.setDriverName( null );
				autoInfraction.setDriverCpfRg( null );
				autoInfraction.setDriverCnh( null );
				autoInfraction.setStateCnh( null );
				return false;
			}
		} );

		lv_offender.setOnItemLongClickListener( new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick( AdapterView<?> parent, View view, int position, long id ) {
				adapterOffender.remove( adapterOffender.getItem( position ) );
				adapterOffender.notifyDataSetChanged();
				// tv_btnOffender.setText( getResources().getString(
				// R.string.bt_addOffender ) );
				// iv_addOffender.setBackgroundResource( R.drawable.ic_btn_add
				// );
				if ( lv_driver.getAdapter().getCount() == 0 )
					iv_addDriverOffender.setBackgroundResource( R.drawable.ic_btn_add );

				autoInfraction.setOffenderName( null );
				autoInfraction.setOffenderCpfRg( null );
				return false;
			}
		} );

		lv_measurement.setOnItemLongClickListener( new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick( AdapterView<?> parent, View view, int position, long id ) {
				adapterMeasurement.remove( adapterMeasurement.getItem( position ) );
				adapterMeasurement.notifyDataSetChanged();
				if ( lv_measurement.getAdapter().getCount() == 0 )
					iv_addMeasurement.setBackgroundResource( R.drawable.ic_btn_add );
				autoInfraction.setMeasurementPerformed( null );
				autoInfraction.setRegulatedLimit( null );
				autoInfraction.setMeasurementVerified( null );
				autoInfraction.setExceeded( null );
				autoInfraction.setEquipmentUsed( null );
				return false;
			}

		} );

		clickListener = new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				// ViewUtil.hideKeyboard( getActivity(), v );
				if ( infraction != null ) {
					observations = new ArrayList<>();
					if ( infraction.getObservationsOne() != null )
						observations.add( infraction.getObservationsOne() );
					if ( infraction.getObservationsTwo() != null )
						observations.add( infraction.getObservationsTwo() );
					if ( infraction.getObservationsThree() != null )
						observations.add( infraction.getObservationsThree() );
					dialogObservations();
				}
			}
		};

		InitAutoFragmentActivity.customMultilineEnter( et_observations );
		InitAutoFragmentActivity.customMultilineEnter( et_otherInformation );
		ViewUtil.checkErrorEditText( et_observations );

		return rootView;

	}

	@Override
	public void onCreateOptionsMenu( Menu menu, MenuInflater inflater ) {
		inflater = getActivity().getMenuInflater();
		inflater.inflate( R.menu.menu_next, menu );
		super.onCreateOptionsMenu( menu, inflater );
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item ) {
		InitAutoFragmentActivity.hideKeyboard();
		switch ( item.getItemId() ) {
			case R.id.action_next:
				if ( saveValues() ) {
					mPager.setCurrentItem( mPager.getCurrentItem() + 1 );
					getActivity().setTitle( getResources().getString( R.string.title_local ) );

					if ( InitAutoFragmentActivity.location.getLocation() != null && InitAutoFragmentActivity.location.canGetLocation() ) {
						autoInfraction.setLatitude( InitAutoFragmentActivity.location.getLatitude() );
						autoInfraction.setLongitude( InitAutoFragmentActivity.location.getLongitude() );
					}

					if ( ( InitAutoFragmentActivity.idAutoRecovered == null || InitAutoFragmentActivity.idAutoRecovered == 0 ) && autoInfraction.getStreet() == null && LocalFragment.et_street.getText().toString().equals( "" ) && autoInfraction.getLatitude() != null && autoInfraction.getLongitude() != null && !LocationTrackerService.obtainedAddress )
						LocationTrackerService.getAddress( autoInfraction.getLatitude().toString(), autoInfraction.getLongitude().toString(), LocalFragment.et_street, LocalFragment.et_number, LocalFragment.et_neighborhood );

				}
				return true;
			case R.id.action_cancel:
				InitAutoFragmentActivity.dialogCancel();
				return true;
			case android.R.id.home:
				mPager.setCurrentItem( mPager.getCurrentItem() - 1 );
				getActivity().setTitle( getResources().getString( R.string.title_vehicle ) );
				return true;
			default:
				return super.onOptionsItemSelected( item );
		}
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	public class InfractionDialog extends DialogFragment {

		private InfractionAdapter adapterInfraction;
		private boolean infractionModified = false;
		private AutoCompleteInfractionAdapter adapterAutoComplete;

		public InfractionDialog() {}

		@Override
		public Dialog onCreateDialog( Bundle savedInstanceState ) {
			LayoutInflater inflater = getActivity().getLayoutInflater();
			View rootView = inflater.inflate( R.layout.dialog_create_infraction, null );

			final ListView lv_infractions = (ListView) rootView.findViewById( android.R.id.list );
			adapterInfraction = new InfractionAdapter( getActivity(), R.layout.adapter_list_text, infractions );

			final AutoCompleteTextView auto_searchInfraction = (AutoCompleteTextView) rootView.findViewById( R.id.auto_SearchInfraction );
			auto_searchInfraction.setThreshold( 1 );

			auto_searchInfraction.addTextChangedListener( new TextWatcher() {

				@Override
				public void onTextChanged( CharSequence s, int start, int before, int count ) {
					if ( s != null && s.length() == 0 && adapterInfraction.getCount() == 1 ) {
						adapterInfraction = new InfractionAdapter( getActivity(), R.layout.adapter_list_text, infractions );
						lv_infractions.setAdapter( adapterInfraction );
						InitAutoFragmentActivity.hideKeyboard( lv_infractions );
					}

				}

				@Override
				public void beforeTextChanged( CharSequence s, int start, int count, int after ) {

				}

				@Override
				public void afterTextChanged( Editable s ) {

				}
			} );

			adapterAutoComplete = new AutoCompleteInfractionAdapter( getActivity(), infractions );
			auto_searchInfraction.setAdapter( adapterAutoComplete );

			auto_searchInfraction.setOnItemClickListener( new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {
					infraction = (Infraction) auto_searchInfraction.getAdapter().getItem( position );
					List<Infraction> selectedInfraction = new ArrayList<>();
					selectedInfraction.add( infraction );
					adapterInfraction = new InfractionAdapter( getActivity(), R.layout.adapter_list_text, selectedInfraction );
					adapterInfraction.getCount();
					lv_infractions.setAdapter( adapterInfraction );
					auto_searchInfraction.setText( infraction.getCodeInfraction().toString() + infraction.getDeployment() );
					auto_searchInfraction.setSelection( infraction.getCodeInfraction().toString().length() );
					lv_infractions.requestFocus();
					InitAutoFragmentActivity.hideKeyboard( lv_infractions );
				}
			} );

			lv_infractions.setAdapter( adapterInfraction );

			if ( !et_infraction.getText().toString().isEmpty() ) {
				lv_infractions.setSelection( infractionPosition );
			}

			lv_infractions.setOnItemClickListener( new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {
					InitAutoFragmentActivity.wasModified = true;

					infraction = (Infraction) lv_infractions.getAdapter().getItem( position );
					autoInfraction.setCodeInfraction( infraction.getCodeInfraction() );
					autoInfraction.setDeployment( infraction.getDeployment() );
					autoInfraction.setDescription( infraction.getDescription() );
					autoInfraction.setLegalSupport( infraction.getLegalSupport() );
					autoInfraction.setPunishment( infraction.getPunishment() );
					autoInfraction.setAdministrativeMeasure( infraction.getAdministrativeMeasure() );
					autoInfraction.setMeasurementType( infraction.getMeasurementType() );
					if ( infraction.getMeasurementType() != null )
						bt_addMeasurement.setVisibility( View.VISIBLE );
					else
						bt_addMeasurement.setVisibility( View.GONE );
					// autoInfraction.setObservations(
					// infraction.getObservationsOne() );
					autoInfraction.setCompetence( infraction.getCompetence() );

					infractionPosition = position;
					if ( infraction.getCodeInfraction() != null )
						et_infraction.setText( infraction.getCodeInfraction().toString() + "" + infraction.getDeployment().toString() );
					else
						et_infraction.setText( null );

					if ( infraction.getPunishment() != null )
						et_setPunishment.setText( infraction.getPunishment().toString() );
					else
						et_setPunishment.setText( null );

					if ( infraction.getAdministrativeMeasure() != null )
						et_setAdministrativeMeasure.setText( infraction.getAdministrativeMeasure().toString() );
					else
						et_setAdministrativeMeasure.setText( null );
					// if ( infraction.getObservationsOne() != null )
					// et_observations.setText(
					// infraction.getObservationsOne().toString() );

					bt_selectInfraction.setBackgroundResource( R.drawable.btn_background );
					et_observations.setOnClickListener( clickListener );
					et_observations.setText( null );
					checkedsTemp = new ArrayList<>();
					observationsTemp = null;
					// et_observations.setOnTouchListener( clickListener );
					autoInfraction.save();
					dismiss();
				}
			} );

			List<String> filters = InfractionRepository.getFilterOptions();
			filters.add( 0, "Todos" );
			ArrayAdapter<String> adapterSpinner = new ArrayAdapter<String>( getActivity(), android.R.layout.simple_spinner_dropdown_item, filters );
			Spinner sp_filter = (Spinner) rootView.findViewById( R.id.sp_filter );
			sp_filter.setAdapter( adapterSpinner );

			sp_filter.setOnItemSelectedListener( new OnItemSelectedListener() {

				@Override
				public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {
					String filter = (String) parent.getSelectedItem();
					if ( !filter.equalsIgnoreCase( "Todos" ) ) {
						if ( filter.equalsIgnoreCase( "Outros" ) ) {
							infractions = InfractionRepository.getInfractionsByFilterAndCompetence( null, InitAutoFragmentActivity.organ.getCompetence() );
						} else {
							infractions = InfractionRepository.getInfractionsByFilterAndCompetence( filter, InitAutoFragmentActivity.organ.getCompetence() );
						}
						adapterInfraction = new InfractionAdapter( getActivity(), R.layout.adapter_list_text, infractions );
						lv_infractions.setAdapter( adapterInfraction );
						infractionModified = true;
					} else if ( infractionModified ) {
						infractions = InfractionRepository.getInfractionsByCompetence( InitAutoFragmentActivity.organ.getCompetence() );
						adapterInfraction = new InfractionAdapter( getActivity(), R.layout.adapter_list_text, infractions );
						lv_infractions.setAdapter( adapterInfraction );
						infractionModified = false;
					}
					adapterAutoComplete = new AutoCompleteInfractionAdapter( getActivity(), infractions );
					auto_searchInfraction.setAdapter( adapterAutoComplete );
				}

				@Override
				public void onNothingSelected( AdapterView<?> parent ) {}
			} );

			AlertDialog.Builder infractionDialog = new AlertDialog.Builder( getActivity() );
			infractionDialog.setTitle( "Selecione uma infração" );
			infractionDialog.setCancelable( true );
			infractionDialog.setView( rootView );

			return infractionDialog.create();
		}
	}

	private AlertDialog dialogDriverOffender = null;

	public void dialogAddDriverOffender() {

		LayoutInflater li = getActivity().getLayoutInflater();
		View view = li.inflate( R.layout.dialog_create_driver_offender, null );
		AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
		builder.setTitle( "Dados do Condutor/Infrator" );
		builder.setCancelable( true );
		builder.setView( view );

		ll_driver = (LinearLayout) view.findViewById( R.id.ll_driver );
		ll_offender = (LinearLayout) view.findViewById( R.id.ll_offender );

		rg_driver_offender = (RadioGroup) view.findViewById( R.id.rg_driver_offender );
		et_nameDriverOffender = (EditText) view.findViewById( R.id.et_nameDriver );
		et_nameDriverOffender.setText( autoInfraction.getDriverName() );
		et_cpfRgDriverOffender = (EditText) view.findViewById( R.id.et_cpfRgDriver );
		et_cpfRgDriverOffender.setText( autoInfraction.getDriverCpfRg() );

		et_cnhPermissionDriver = (EditText) view.findViewById( R.id.et_cnhPermissionDriver );
		et_cnhPermissionDriver.setText( autoInfraction.getDriverCnh() );
		et_ufCnh = (EditText) view.findViewById( R.id.et_ufCnh );
		et_ufCnh.setText( autoInfraction.getStateCnh() );

		et_nameOffender = (EditText) view.findViewById( R.id.et_nameOffender );
		et_nameOffender.setText( autoInfraction.getOffenderName() );
		et_cpfRgOffender = (EditText) view.findViewById( R.id.et_cpfRgOffender );
		et_cpfRgOffender.setText( autoInfraction.getOffenderCpfRg() );

		rg_driver_offender.setOnCheckedChangeListener( new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged( RadioGroup group, int checkedId ) {
				if ( checkedId == R.id.rb_driver ) {
					ll_driver.setVisibility( View.VISIBLE );
					ll_offender.setVisibility( View.GONE );

					// et_nameDriverOffender.setText(
					// autoInfraction.getDriverName() );
					// et_cpfRgDriverOffender.setText(
					// autoInfraction.getDriverCpfRg() );
					// et_cnhPermissionDriver.setText(
					// autoInfraction.getDriverCnh() );
					// et_ufCnh.setText( autoInfraction.getStateCnh() );
				} else if ( checkedId == R.id.rb_offender ) {
					ll_driver.setVisibility( View.GONE );
					ll_offender.setVisibility( View.VISIBLE );

					// et_nameOffender.setText( autoInfraction.getOffenderName()
					// );
					// et_cpfRgOffender.setText(
					// autoInfraction.getOffenderCpfRg() );
				}
			}
		} );

		builder.setPositiveButton( "Salvar", new DialogInterface.OnClickListener() {

			@Override
			public void onClick( DialogInterface dialog, int which ) {
				InitAutoFragmentActivity.wasModified = true;

				// Driver
				if ( !et_nameDriverOffender.getText().toString().equals( "" ) )
					autoInfraction.setDriverName( et_nameDriverOffender.getText().toString() );
				else
					autoInfraction.setDriverName( null );

				if ( !et_cpfRgDriverOffender.getText().toString().equals( "" ) )
					autoInfraction.setDriverCpfRg( et_cpfRgDriverOffender.getText().toString() );
				else
					autoInfraction.setDriverCpfRg( null );

				if ( !et_cnhPermissionDriver.getText().toString().equals( "" ) )
					autoInfraction.setDriverCnh( et_cnhPermissionDriver.getText().toString() );
				else
					autoInfraction.setDriverCnh( null );

				if ( !et_ufCnh.getText().toString().equals( "" ) )
					autoInfraction.setStateCnh( et_ufCnh.getText().toString() );
				else
					autoInfraction.setStateCnh( null );

				if ( autoInfraction.getDriverName() != null || autoInfraction.getDriverCpfRg() != null ) {
					driverOffender = new DriverOffenderVO( autoInfraction.getDriverName(), autoInfraction.getDriverCpfRg() );
					listDriver.clear();
					listDriver.add( driverOffender );
					adapterDriver = new DriverOffenderAdapter( getActivity(), R.layout.adapter_driver, listDriver );
					lv_driver.setAdapter( adapterDriver );
					iv_addDriverOffender.setBackgroundResource( R.drawable.ic_btn_edit );
				} else {
					listDriver.clear();
					adapterDriver = new DriverOffenderAdapter( getActivity(), R.layout.adapter_driver, listDriver );
					lv_driver.setAdapter( adapterDriver );

					if ( lv_offender.getAdapter() != null && lv_offender.getAdapter().getCount() == 0 )
						iv_addDriverOffender.setBackgroundResource( R.drawable.ic_btn_add );

				}

				// Offender
				if ( !et_nameOffender.getText().toString().equals( "" ) )
					autoInfraction.setOffenderName( et_nameOffender.getText().toString() );
				else
					autoInfraction.setOffenderName( null );

				if ( !et_cpfRgOffender.getText().toString().equals( "" ) )
					autoInfraction.setOffenderCpfRg( et_cpfRgOffender.getText().toString() );
				else
					autoInfraction.setOffenderCpfRg( null );

				if ( autoInfraction.getOffenderName() != null || autoInfraction.getOffenderCpfRg() != null ) {
					driverOffender = new DriverOffenderVO( autoInfraction.getOffenderName(), autoInfraction.getOffenderCpfRg() );
					listOffender.clear();
					listOffender.add( driverOffender );
					adapterOffender = new DriverOffenderAdapter( getActivity(), R.layout.adapter_offender, listOffender );
					lv_offender.setAdapter( adapterOffender );
					iv_addDriverOffender.setBackgroundResource( R.drawable.ic_btn_edit );
				} else {
					listOffender.clear();
					adapterOffender = new DriverOffenderAdapter( getActivity(), R.layout.adapter_offender, listOffender );
					lv_offender.setAdapter( adapterOffender );

					if ( lv_driver.getAdapter() != null && lv_driver.getAdapter().getCount() == 0 )
						iv_addDriverOffender.setBackgroundResource( R.drawable.ic_btn_add );
				}

				// tv_btnDriver.setText( getResources().getString(
				// R.string.bt_editDriver ) );
				// iv_addDriverOffender.setBackgroundResource(
				// R.drawable.ic_btn_edit );
				autoInfraction.save();
				InitAutoFragmentActivity.hideKeyboard();
				dialogDriverOffender.dismiss();
			}
		} );

		builder.setNegativeButton( "Cancelar", new DialogInterface.OnClickListener() {

			@Override
			public void onClick( DialogInterface dialog, int which ) {
				dialogDriverOffender.dismiss();
			}
		} );

		dialogDriverOffender = builder.create();
		dialogDriverOffender.show();
	}

	private AlertDialog dialogObservations = null;

	public void dialogObservations() {

		LayoutInflater li = getActivity().getLayoutInflater();
		View view = li.inflate( R.layout.dialog_observations, null );
		AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
		builder.setTitle( "Informar observações" );
		builder.setView( view );

		et_observations_extras = (EditText) view.findViewById( R.id.et_observations_extras );
		if ( observationsTemp != null )
			et_observations_extras.setText( observationsTemp );
		lv_observations = (ListView) view.findViewById( R.id.lv_observations );
		final ObservationsAdapter adapterObservations = new ObservationsAdapter( getActivity(), R.layout.adapter_observations, observations, checkedsTemp );
		lv_observations.setAdapter( adapterObservations );

		builder.setPositiveButton( "Salvar", new DialogInterface.OnClickListener() {

			@Override
			public void onClick( DialogInterface dialog, int which ) {
				InitAutoFragmentActivity.wasModified = true;

				List<String> obsCheckeds = adapterObservations.getItemsChecked();
				StringBuilder builder = new StringBuilder();
				if ( checkedsTemp == null )
					checkedsTemp = new ArrayList<String>();
				else
					checkedsTemp.clear();
				for ( String checked : obsCheckeds ) {
					checkedsTemp.add( checked );
					String c = checked.trim();
					if ( c.charAt( c.length() - 1 ) == '.' || c.charAt( c.length() - 1 ) == ';' )
						builder.append( c ).append( " " );
					else
						builder.append( c ).append( ". " );
				}

				if ( !et_observations_extras.getText().toString().equals( "" ) ) {
					String obs = et_observations_extras.getText().toString().trim();
					observationsTemp = obs;
					if ( obs.charAt( obs.length() - 1 ) == '.' || obs.charAt( obs.length() - 1 ) == ';' )
						builder.append( obs );
					else
						builder.append( obs ).append( "." );
				} else
					observationsTemp = null;
				et_observations.setText( builder.toString().trim() );
				autoInfraction.setObservations( builder.toString().trim() );

				dialogObservations.dismiss();
			}
		} );

		builder.setNegativeButton( "Cancelar", new DialogInterface.OnClickListener() {

			@Override
			public void onClick( DialogInterface dialog, int which ) {
				dialogObservations.dismiss();
			}
		} );

		dialogObservations = builder.create();
		dialogObservations.show();
	}

	private AlertDialog dialogMeasurement = null;
	private EditText et_measurementType;
	private EditText et_measurementPerformed;
	private EditText et_regulatedLimit;
	private EditText et_measurementVerified;
	private EditText et_exceeded;
	private EditText et_equipmentUsed;

	public void dialogMeasurement() {

		LayoutInflater li = getActivity().getLayoutInflater();
		View view = li.inflate( R.layout.dialog_measurement, null );
		AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
		builder.setTitle( "Informar medição" );
		builder.setView( view );

		et_measurementType = (EditText) view.findViewById( R.id.et_measurementType );
		et_measurementPerformed = (EditText) view.findViewById( R.id.et_measurementPerformed );
		et_regulatedLimit = (EditText) view.findViewById( R.id.et_regulatedLimit );
		et_measurementVerified = (EditText) view.findViewById( R.id.et_measurementVerified );
		et_exceeded = (EditText) view.findViewById( R.id.et_exceeded );
		et_equipmentUsed = (EditText) view.findViewById( R.id.et_equipmentUsed );

		final MeasurementType measurementType = MeasurementTypeRepository.getMeasurementTypeByCode( autoInfraction.getMeasurementType() );
		if ( measurementType != null ) {
			et_measurementType.setText( measurementType.getDescription() );
			et_regulatedLimit.setText( measurementType.getAllowedValue() );
		}

		if ( autoInfraction.getMeasurementPerformed() != null )
			et_measurementPerformed.setText( autoInfraction.getMeasurementPerformed().toString() );
		if ( autoInfraction.getRegulatedLimit() != null )
			et_regulatedLimit.setText( autoInfraction.getRegulatedLimit() );
		if ( autoInfraction.getMeasurementVerified() != null )
			et_measurementVerified.setText( autoInfraction.getMeasurementVerified().toString() );
		if ( autoInfraction.getExceeded() != null )
			et_exceeded.setText( autoInfraction.getExceeded().toString() );
		if ( autoInfraction.getEquipmentUsed() != null )
			et_equipmentUsed.setText( autoInfraction.getEquipmentUsed() );

		builder.setPositiveButton( "Salvar", new DialogInterface.OnClickListener() {

			@Override
			public void onClick( DialogInterface dialog, int which ) {
				InitAutoFragmentActivity.wasModified = true;
				if ( !et_measurementPerformed.getText().toString().equals( "" ) )
					autoInfraction.setMeasurementPerformed( et_measurementPerformed.getText().toString() );
				else
					autoInfraction.setMeasurementPerformed( null );

				if ( !et_regulatedLimit.getText().toString().equals( "" ) )
					autoInfraction.setRegulatedLimit( et_regulatedLimit.getText().toString() );
				else
					autoInfraction.setRegulatedLimit( null );

				if ( !et_measurementVerified.getText().toString().equals( "" ) )
					autoInfraction.setMeasurementVerified( et_measurementVerified.getText().toString() );
				else
					autoInfraction.setMeasurementVerified( null );

				if ( !et_exceeded.getText().toString().equals( "" ) )
					autoInfraction.setExceeded( et_exceeded.getText().toString() );
				else
					autoInfraction.setExceeded( null );

				if ( !et_equipmentUsed.getText().toString().equals( "" ) )
					autoInfraction.setEquipmentUsed( et_equipmentUsed.getText().toString() );
				else
					autoInfraction.setEquipmentUsed( null );

				List<MeasurementType> measurementList = new ArrayList<MeasurementType>();
				measurementList.add( measurementType );
				adapterMeasurement = new MeasurementAdapter( getActivity(), R.layout.adapter_measurement, measurementList, et_measurementPerformed.getText().toString() );
				lv_measurement.setAdapter( adapterMeasurement );
				iv_addMeasurement.setBackgroundResource( R.drawable.ic_btn_edit );
				dialogMeasurement.dismiss();
			}
		} );

		builder.setNegativeButton( "Cancelar", new DialogInterface.OnClickListener() {

			@Override
			public void onClick( DialogInterface dialog, int which ) {
				dialogMeasurement.dismiss();
			}
		} );

		dialogMeasurement = builder.create();
		dialogMeasurement.show();
	}

	private boolean saveValues() {
		if ( autoInfraction.getCodeInfraction() == null ) {
			bt_selectInfraction.setBackgroundResource( R.drawable.btn_error_background );
			Toast toast = Toast.makeText( getActivity(), getResources().getString( R.string.error_infraction_required ), Toast.LENGTH_LONG );
			toast.setGravity( Gravity.CENTER, 0, 0 );
			toast.show();
			return false;
		}

		if ( et_observations.getText().toString().equals( "" ) ) {
			et_observations.setError( getResources().getString( R.string.error_required ) );
			Toast toast = Toast.makeText( getActivity(), getResources().getString( R.string.error_required ), Toast.LENGTH_LONG );
			toast.setGravity( Gravity.CENTER, 0, 0 );
			toast.show();
			return false;
		}

		completeSaveValues();
		autoInfraction.save();
		return true;
	}

	public void completeSaveValues() {
		autoInfraction.setObservations( et_observations.getText().toString() );
		if ( !et_otherInformation.getText().toString().equals( "" ) )
			autoInfraction.setOtherInformation( et_otherInformation.getText().toString() );
	}

	@Override
	public void onViewCreated( View view, Bundle savedInstanceState ) {
		super.onViewCreated( view, savedInstanceState );
		if ( autoInfraction.getCodeInfraction() != null && autoInfraction.getDeployment() != null ) {
			et_infraction.setText( autoInfraction.getCodeInfraction() + "" + autoInfraction.getDeployment() );
			infraction = InfractionRepository.getInfractionByCodeAndDeployment( autoInfraction.getCodeInfraction(), autoInfraction.getDeployment() );
			if ( infraction != null )
				et_observations.setOnClickListener( clickListener );
		} else if ( autoInfraction.getCodeInfraction() != null )
			et_infraction.setText( autoInfraction.getCodeInfraction() + "" );
		if ( autoInfraction.getPunishment() != null )
			et_setPunishment.setText( autoInfraction.getPunishment() + "" );
		if ( autoInfraction.getAdministrativeMeasure() != null )
			et_setAdministrativeMeasure.setText( autoInfraction.getAdministrativeMeasure() + "" );
		if ( autoInfraction.getObservations() != null )
			et_observations.setText( autoInfraction.getObservations() + "" );
		if ( autoInfraction.getMeasurementType() != null ) {
			bt_addMeasurement.setVisibility( View.VISIBLE );
			List<MeasurementType> measurementList = new ArrayList<MeasurementType>();
			MeasurementType measurementType = MeasurementTypeRepository.getMeasurementTypeByCode( autoInfraction.getMeasurementType() );
			measurementList.add( measurementType );
			adapterMeasurement = new MeasurementAdapter( getActivity(), R.layout.adapter_measurement, measurementList, autoInfraction.getMeasurementPerformed() );
			lv_measurement.setAdapter( adapterMeasurement );
			iv_addMeasurement.setBackgroundResource( R.drawable.ic_btn_edit );
		}

		et_otherInformation.setText( autoInfraction.getOtherInformation() );

		if ( autoInfraction.getDriverName() != null || autoInfraction.getDriverCpfRg() != null ) {
			driverOffender = new DriverOffenderVO( autoInfraction.getDriverName(), autoInfraction.getDriverCpfRg() );
			listDriver.add( driverOffender );
			adapterDriver = new DriverOffenderAdapter( getActivity(), R.layout.adapter_driver, listDriver );
			lv_driver.setAdapter( adapterDriver );
			// tv_btnDriver.setText( getResources().getString(
			// R.string.bt_editDriver ) );
			iv_addDriverOffender.setBackgroundResource( R.drawable.ic_btn_edit );
		}
		if ( autoInfraction.getOffenderName() != null || autoInfraction.getOffenderCpfRg() != null ) {
			driverOffender = new DriverOffenderVO( autoInfraction.getOffenderName(), autoInfraction.getOffenderCpfRg() );
			listOffender.add( driverOffender );
			adapterOffender = new DriverOffenderAdapter( getActivity(), R.layout.adapter_offender, listOffender );
			lv_offender.setAdapter( adapterOffender );
			// tv_btnOffender.setText( getResources().getString(
			// R.string.bt_editDriver ) );
			iv_addDriverOffender.setBackgroundResource( R.drawable.ic_btn_edit );
		}
	}
}
