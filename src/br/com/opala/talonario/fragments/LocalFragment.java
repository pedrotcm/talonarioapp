package br.com.opala.talonario.fragments;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.text.WordUtils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import br.com.opala.talonario.R;
import br.com.opala.talonario.activities.InitAutoFragmentActivity;
import br.com.opala.talonario.adapters.AutoCompletePatioAdapter;
import br.com.opala.talonario.entities.AutoInfraction;
import br.com.opala.talonario.entities.City;
import br.com.opala.talonario.entities.Image;
import br.com.opala.talonario.entities.Patio;
import br.com.opala.talonario.repositories.CityRepository;
import br.com.opala.talonario.repositories.PatioRepository;
import br.com.opala.talonario.repositories.SettingParameterRepository;
import br.com.opala.talonario.services.LocationTrackerService;
import br.com.opala.talonario.utils.ViewUtil;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

public class LocalFragment extends Fragment {

	private ViewPager mPager;
	private AutoInfraction autoInfraction;

	private LinearLayout bt_setDate;
	private LinearLayout bt_setHour;
	private LinearLayout bt_searchPatio;
	private List<Patio> patios;

	private static EditText et_date;
	private static EditText et_hour;
	public static EditText et_street;
	public static EditText et_number;
	public static EditText et_neighborhood;
	private EditText et_complement;

	private Spinner sp_cityState;
	private EditText et_codeCity;
	private static Date currentDate;
	private static SimpleDateFormat hoursFormat;

	// private Organ organSelected;
	private City citySelected;

	private TextView tv_cityState;
	private CaldroidFragment dialogCaldroidFragment;
	private Bundle state;
	private static SimpleDateFormat dateFormat;
	private ArrayAdapter<City> adapterCity;
	private Calendar cal;

	public static LinearLayout bt_useInformations;

	protected static int VALID_DAYS;
	protected static boolean IS_DATE_EDIT;

	public void init( View rootView ) {
		mPager = InitAutoFragmentActivity.mPager;
		autoInfraction = InitAutoFragmentActivity.autoInfraction;

		bt_setDate = (LinearLayout) rootView.findViewById( R.id.bt_setDate );
		bt_setHour = (LinearLayout) rootView.findViewById( R.id.bt_setHour );
		bt_searchPatio = (LinearLayout) rootView.findViewById( R.id.bt_searchPatio );

		et_date = (EditText) rootView.findViewById( R.id.et_date );

		et_hour = (EditText) rootView.findViewById( R.id.et_hour );
		et_street = (EditText) rootView.findViewById( R.id.et_street );
		et_number = (EditText) rootView.findViewById( R.id.et_number );
		et_neighborhood = (EditText) rootView.findViewById( R.id.et_neighborhood );
		et_complement = (EditText) rootView.findViewById( R.id.et_complement );
		InitAutoFragmentActivity.modifiedEditText( et_complement );

		sp_cityState = (Spinner) rootView.findViewById( R.id.sp_cityState );
		et_codeCity = (EditText) rootView.findViewById( R.id.et_codeCity );

		dateCompleteFormat = new SimpleDateFormat( "dd/MM/yyy HH:mm:ss" );
		dateFormat = new SimpleDateFormat( "dd/MM/yyyy" );
		hoursFormat = new SimpleDateFormat( "HH:mm" );

		currentDate = autoInfraction.getCreatedAt();
		et_date.setText( dateFormat.format( currentDate ) );
		et_hour.setText( hoursFormat.format( currentDate ) );

		bt_useInformations = (LinearLayout) rootView.findViewById( R.id.bt_useInformations );
		if ( PhotoFragment.haveImageWithDate )
			bt_useInformations.setVisibility( View.VISIBLE );
		VALID_DAYS = SettingParameterRepository.getValidityAutos();
		IS_DATE_EDIT = SettingParameterRepository.getIsDateEdit();

		cal = Calendar.getInstance();
		// Min date
		cal.add( Calendar.DATE, -VALID_DAYS );
		cal.set( Calendar.HOUR_OF_DAY, 0 );
		cal.set( Calendar.MINUTE, 0 );
		cal.set( Calendar.SECOND, 0 );

		minDate = cal.getTime();

		if ( IS_DATE_EDIT ) {
			bt_setDate.setVisibility( View.VISIBLE );
			bt_setHour.setVisibility( View.VISIBLE );
		}
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		super.onCreateView( inflater, container, savedInstanceState );
		setHasOptionsMenu( true );

		state = savedInstanceState;

		View rootView = inflater.inflate( R.layout.fragment_local, container, false );
		init( rootView );

		List<City> cities = InitAutoFragmentActivity.organ.getCities();
		if ( cities.size() == 1 ) {
			sp_cityState.setEnabled( false );
			sp_cityState.setClickable( false );
		} else {
			cities.add( 0, new City( "Selecione o Município", 0, "Estado", null ) );
		}

		adapterCity = new ArrayAdapter<City>( getActivity(), android.R.layout.simple_spinner_dropdown_item, cities );
		sp_cityState.setAdapter( adapterCity );
		sp_cityState.setOnItemSelectedListener( new OnItemSelectedListener() {

			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {
				citySelected = (City) parent.getSelectedItem();

				// if ( position != 0 ) {
				// autoInfraction.setCity( citySelected.getCity() );
				// autoInfraction.setCodeCity( citySelected.getCodeCity() );
				// autoInfraction.setState( citySelected.getState() );
				// et_codeCity.setText( citySelected.getCodeCity().toString() );
				// }

				if ( citySelected.getCodeCity() != 0 ) {
					// InitAutoFragmentActivity.wasModified = true;

					autoInfraction.setCity( citySelected.getCity() );
					autoInfraction.setCodeCity( citySelected.getCodeCity() );
					autoInfraction.setState( citySelected.getState() );
					et_codeCity.setText( citySelected.getCodeCity().toString() );
				}

				tv_cityState = (TextView) view;

				if ( !et_street.getText().toString().equals( "" ) )
					autoInfraction.setStreet( et_street.getText().toString() );
				if ( !et_number.getText().toString().equals( "" ) )
					autoInfraction.setNumber( Integer.valueOf( et_number.getText().toString() ) );
				if ( !et_street.getText().toString().equals( "" ) )
					autoInfraction.setStreet( et_street.getText().toString() );
				if ( !et_neighborhood.getText().toString().equals( "" ) )
					autoInfraction.setNeighborhood( et_neighborhood.getText().toString() );
				if ( !et_complement.getText().toString().equals( "" ) )
					autoInfraction.setComplement( et_complement.getText().toString() );

				autoInfraction.save();
			}

			@Override
			public void onNothingSelected( AdapterView<?> parent ) {

			}
		} );

		bt_setHour.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				dialogTimePicker();
			}
		} );

		bt_setDate.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				dialogDateSelect();
			}
		} );

		bt_useInformations.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				if ( !autoInfraction.getPhotos().isEmpty() ) {
					Image image = PhotoFragment.imageInformations;
					if ( image != null ) {
						et_date.setText( dateFormat.format( image.getCreatedAt() ) );
						et_hour.setText( hoursFormat.format( image.getCreatedAt() ) );
						if ( image.getAddress() != null ) {
							et_street.setText( image.getAddress().get( "street" ) );
							et_number.setText( image.getAddress().get( "number" ) );
							et_neighborhood.setText( image.getAddress().get( "neighborhood" ) );
							File file = new File( image.getPath() );
							String dateTime = image.getPath().replace( file.getParent() + File.separator, "" ).replace( image.getExtension(), "" );
							if ( dateTime.contains( "#" ) ) {
								String latLng = dateTime.split( "#" )[1];
								Double latitude = Double.valueOf( latLng.split( "_" )[0] ) * -1;
								Double longitude = Double.valueOf( latLng.split( "_" )[1] ) * -1;
								autoInfraction.setLatitude( latitude );
								autoInfraction.setLongitude( longitude );
							}
						}
					}
				}
			}
		} );

		bt_searchPatio.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				PatioDialog dialog = new PatioDialog();
				dialog.show( getFragmentManager(), "dialogPatio" );
			}
		} );

		ViewUtil.checkErrorEditText( et_street );
		ViewUtil.checkErrorEditText( et_number );

		return rootView;
	}

	@Override
	public void onCreateOptionsMenu( Menu menu, MenuInflater inflater ) {
		inflater = getActivity().getMenuInflater();
		inflater.inflate( R.menu.menu_accept, menu );
		super.onCreateOptionsMenu( menu, inflater );
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item ) {
		InitAutoFragmentActivity.hideKeyboard();
		switch ( item.getItemId() ) {
			case R.id.action_accept:
				autoInfraction.save();
				if ( saveValues() ) {
					InitAutoFragmentActivity.dialogSaveAutoInfraction();
				}
				return true;
			case R.id.action_cancel:
				InitAutoFragmentActivity.dialogCancel();
				return true;
			case android.R.id.home:
				mPager.setCurrentItem( mPager.getCurrentItem() - 1 );
				getActivity().setTitle( getResources().getString( R.string.title_infraction ) );
				return true;
			default:
				return super.onOptionsItemSelected( item );
		}
	}

	private void dialogTimePicker() {
		int hour = Integer.valueOf( et_hour.getText().toString().substring( 0, 2 ) );
		int minute = Integer.valueOf( et_hour.getText().toString().substring( 3, 5 ) );
		TimePickerDialog mTimePicker;
		mTimePicker = new TimePickerDialog( getActivity(), new TimePickerDialog.OnTimeSetListener() {
			@Override
			public void onTimeSet( TimePicker timePicker, int selectedHour, int selectedMinute ) {
				String hour = null;
				String minute = null;
				if ( selectedHour < 10 )
					hour = "0" + String.valueOf( selectedHour );
				else
					hour = String.valueOf( selectedHour );
				if ( selectedMinute < 10 )
					minute = "0" + String.valueOf( selectedMinute );
				else
					minute = String.valueOf( selectedMinute );
				et_hour.setText( hour + ":" + minute );
				try {
					autoInfraction.setCreatedAt( dateCompleteFormat.parse( et_date.getText().toString() + " " + et_hour.getText().toString() + ":00" ) );
				} catch ( ParseException e ) {
					e.printStackTrace();
				}
				autoInfraction.save();
			}
		}, hour, minute, true );// Yes 24 hour time
		mTimePicker.setTitle( "Selecione a hora" );
		mTimePicker.setCanceledOnTouchOutside( false );
		mTimePicker.show();
	}

	// private void dialogDatePicker() {
	// int mYear = Integer.valueOf( et_date.getText().toString().substring( 6,
	// 10 ) );
	// int mMonth = Integer.valueOf( et_date.getText().toString().substring( 3,
	// 5 ) );
	// int mDay = Integer.valueOf( et_date.getText().toString().substring( 0, 2
	// ) );
	// mMonth = mMonth - 1;
	//
	// DatePickerDialog mDatePicker;
	// mDatePicker = new DatePickerDialog( getActivity(), new
	// OnDateSetListener() {
	// @Override
	// public void onDateSet( DatePicker datepicker, int selectedyear, int
	// selectedmonth, int selectedday ) {
	//
	// selectedmonth = selectedmonth + 1;
	// String day = null;
	// String month = null;
	// if ( selectedday < 10 ) {
	// day = "0" + selectedday;
	// } else
	// day = String.valueOf( selectedday );
	// if ( selectedmonth < 10 ) {
	// month = "0" + selectedmonth;
	// } else
	// month = String.valueOf( selectedmonth );
	//
	// et_date.setText( day + "/" + month + "/" + selectedyear );
	// }
	// }, mYear, mMonth, mDay );
	// mDatePicker.setTitle( "Selecione a data" );
	// mDatePicker.setCanceledOnTouchOutside( false );
	// mDatePicker.show();
	// }

	// Setup listener
	final CaldroidListener listener = new CaldroidListener() {

		@Override
		public void onSelectDate( Date date, View view ) {
			et_date.setText( dateFormat.format( date ) );
			try {
				autoInfraction.setCreatedAt( dateCompleteFormat.parse( et_date.getText().toString() + " " + et_hour.getText().toString() + ":00" ) );
			} catch ( ParseException e ) {
				e.printStackTrace();
			}
			autoInfraction.save();
			dialogCaldroidFragment.dismiss();
		}

		@Override
		public void onChangeMonth( int month, int year ) {
			String text = "month: " + month + " year: " + year;
		}

		@Override
		public void onLongClickDate( Date date, View view ) {}

	};

	private static SimpleDateFormat dateCompleteFormat;
	private Date minDate;

	private void dialogDateSelect() {
		// Setup caldroid to use as dialog
		dialogCaldroidFragment = new CaldroidFragment();
		dialogCaldroidFragment.setCaldroidListener( listener );

		// If activity is recovered from rotation
		final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
		if ( state != null ) {
			dialogCaldroidFragment.restoreDialogStatesFromKey( getActivity().getSupportFragmentManager(), state, "DIALOG_CALDROID_SAVED_STATE", dialogTag );
			Bundle args = dialogCaldroidFragment.getArguments();
			if ( args == null ) {
				args = new Bundle();
				dialogCaldroidFragment.setArguments( args );
			}
			args.putString( CaldroidFragment.DIALOG_TITLE, "Selecione uma data" );
		} else {
			// Setup arguments
			Bundle bundle = new Bundle();
			// Setup dialogTitle
			bundle.putString( CaldroidFragment.DIALOG_TITLE, "Selecione uma data" );
			bundle.putBoolean( CaldroidFragment.ENABLE_SWIPE, false );
			// bundle.putBoolean( CaldroidFragment.SHOW_NAVIGATION_ARROWS, false
			// );

			dialogCaldroidFragment.setArguments( bundle );
		}

		dialogCaldroidFragment.show( getActivity().getSupportFragmentManager(), dialogTag );

		// Max date
		cal = Calendar.getInstance();
		Date maxDate = cal.getTime();

		dialogCaldroidFragment.setMinDate( minDate );
		dialogCaldroidFragment.setMaxDate( maxDate );
		dialogCaldroidFragment.refreshView();
	}

	public boolean saveValues() {
		if ( et_street.getText().toString().equals( "" ) ) {
			et_street.requestFocus();
			et_street.setError( getResources().getString( R.string.error_required ) );
			return false;
		}

		if ( et_number.getText().toString().equalsIgnoreCase( "" ) ) {
			et_number.requestFocus();
			et_number.setError( getResources().getString( R.string.error_required ) );
			return false;
		} else {
			try {
				Integer.valueOf( et_number.getText().toString() );
			} catch ( NumberFormatException e ) {
				Toast toast = Toast.makeText( getActivity(), getResources().getString( R.string.error_number ), Toast.LENGTH_LONG );
				toast.setGravity( Gravity.CENTER, 0, 0 );
				toast.show();
				return false;
			}

		}

		if ( citySelected.getCodeCity() == 0 ) {
			tv_cityState.setError( "" );
			Toast toast = Toast.makeText( getActivity(), getResources().getString( R.string.error_required ), Toast.LENGTH_LONG );
			toast.setGravity( Gravity.CENTER, 0, 0 );
			toast.show();
			return false;
		}

		try {
			autoInfraction.setCreatedAt( dateCompleteFormat.parse( et_date.getText().toString() + " " + et_hour.getText().toString() + ":00" ) );
		} catch ( ParseException e ) {
			e.printStackTrace();
		}

		System.out.println( autoInfraction.getCreatedAt() );
		System.out.println( minDate );
		if ( autoInfraction.getCreatedAt().compareTo( minDate ) < 0 ) {
			Toast toast = Toast.makeText( getActivity(), getResources().getString( R.string.error_date ), Toast.LENGTH_LONG );
			toast.setGravity( Gravity.CENTER, 0, 0 );
			toast.show();
			return false;
		}

		completeSaveValues();
		autoInfraction.save();
		return true;
	}

	public void completeSaveValues() {

		if ( !et_street.getText().toString().equals( "" ) )
			autoInfraction.setStreet( et_street.getText().toString() );
		if ( !et_number.getText().toString().equals( "" ) )
			autoInfraction.setNumber( Integer.valueOf( et_number.getText().toString() ) );
		if ( !et_neighborhood.getText().toString().equals( "" ) )
			autoInfraction.setNeighborhood( et_neighborhood.getText().toString() );
		if ( !et_complement.getText().toString().equals( "" ) )
			autoInfraction.setComplement( et_complement.getText().toString() );

	}

	@Override
	public void onViewCreated( View view, Bundle savedInstanceState ) {
		super.onViewCreated( view, savedInstanceState );

		if ( ( InitAutoFragmentActivity.idAutoRecovered == null || InitAutoFragmentActivity.idAutoRecovered == 0 ) && autoInfraction.getStreet() == null && autoInfraction.getLatitude() != null && autoInfraction.getLongitude() != null && !LocationTrackerService.obtainedAddress )
			LocationTrackerService.getAddress( autoInfraction.getLatitude().toString(), autoInfraction.getLongitude().toString(), et_street, et_number, et_neighborhood );

		if ( autoInfraction.getCreatedAt() != null ) {
			et_date.setText( dateFormat.format( autoInfraction.getCreatedAt() ) );
			et_hour.setText( hoursFormat.format( autoInfraction.getCreatedAt() ) );
		}

		if ( autoInfraction.getStreet() != null )
			et_street.setText( autoInfraction.getStreet() );
		if ( autoInfraction.getNumber() != null )
			et_number.setText( autoInfraction.getNumber() + "" );
		if ( autoInfraction.getNeighborhood() != null )
			et_neighborhood.setText( autoInfraction.getNeighborhood() );
		if ( autoInfraction.getComplement() != null )
			et_complement.setText( autoInfraction.getComplement() );

		if ( autoInfraction.getCity() != null ) {
			City city = CityRepository.getCityByDescriptionAndCode( autoInfraction.getCity(), autoInfraction.getCodeCity() );
			int cityPosition = adapterCity.getPosition( city );
			sp_cityState.setSelection( cityPosition );
			et_codeCity.setText( autoInfraction.getCodeCity().toString() );
		}

	}

	public static void clearAddressImage() {
		et_date.setText( dateFormat.format( currentDate ) );
		et_hour.setText( hoursFormat.format( currentDate ) );
		et_street.setText( null );
		et_number.setText( null );
		et_neighborhood.setText( null );
		InitAutoFragmentActivity.autoInfraction.setCreatedAt( currentDate );
		InitAutoFragmentActivity.autoInfraction.setStreet( null );
		InitAutoFragmentActivity.autoInfraction.setNumber( null );
		InitAutoFragmentActivity.autoInfraction.setNeighborhood( null );
	}

	public class PatioDialog extends DialogFragment {

		private boolean patioModified = false;
		private ListView lv_patios;
		private AutoCompletePatioAdapter adapterAutoComplete;
		private AutoCompleteTextView auto_SearchPatio;
		private Integer patioPosition;
		private ArrayAdapter<Patio> adapterPatio;

		public PatioDialog() {}

		@Override
		public Dialog onCreateDialog( Bundle savedInstanceState ) {
			LayoutInflater inflater = getActivity().getLayoutInflater();
			View rootView = inflater.inflate( R.layout.dialog_search_patio, null );

			List<String> filters = PatioRepository.getFilterOptions();
			filters.add( 0, "Todos" );
			ArrayAdapter<String> adapterSpinner = new ArrayAdapter<String>( getActivity(), android.R.layout.simple_spinner_dropdown_item, filters );
			Spinner sp_filter = (Spinner) rootView.findViewById( R.id.sp_filter );
			sp_filter.setAdapter( adapterSpinner );

			sp_filter.setOnItemSelectedListener( new OnItemSelectedListener() {

				@Override
				public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {
					String filter = (String) parent.getSelectedItem();
					ArrayAdapter<Patio> adapterPatio;
					if ( !filter.equalsIgnoreCase( "Todos" ) ) {
						patios = PatioRepository.getAllOrderlyByFilter( filter );
						adapterPatio = new ArrayAdapter<Patio>( getActivity(), android.R.layout.simple_list_item_1, patios );
						lv_patios.setAdapter( adapterPatio );
						patioModified = true;
					} else if ( patioModified ) {
						patios = PatioRepository.getAllOrderly();
						adapterPatio = new ArrayAdapter<Patio>( getActivity(), android.R.layout.simple_list_item_1, patios );
						lv_patios.setAdapter( adapterPatio );
						patioModified = false;
					}
					adapterAutoComplete = new AutoCompletePatioAdapter( getActivity(), patios );
					auto_SearchPatio.setAdapter( adapterAutoComplete );
				}

				@Override
				public void onNothingSelected( AdapterView<?> parent ) {}
			} );

			lv_patios = (ListView) rootView.findViewById( android.R.id.list );
			if ( patios == null )
				patios = PatioRepository.getAllOrderly();
			adapterPatio = new ArrayAdapter<Patio>( getActivity(), android.R.layout.simple_list_item_1, patios );
			lv_patios.setAdapter( adapterPatio );

			auto_SearchPatio = (AutoCompleteTextView) rootView.findViewById( R.id.auto_SearchPatio );
			auto_SearchPatio.setThreshold( 1 );

			auto_SearchPatio.addTextChangedListener( new TextWatcher() {

				@Override
				public void onTextChanged( CharSequence s, int start, int before, int count ) {
					if ( s != null && s.length() == 0 && adapterPatio.getCount() == 1 ) {
						adapterPatio = new ArrayAdapter<Patio>( getActivity(), android.R.layout.simple_list_item_1, patios );
						lv_patios.setAdapter( adapterPatio );
						InitAutoFragmentActivity.hideKeyboard( lv_patios );
					}

				}

				@Override
				public void beforeTextChanged( CharSequence s, int start, int count, int after ) {

				}

				@Override
				public void afterTextChanged( Editable s ) {

				}
			} );

			adapterAutoComplete = new AutoCompletePatioAdapter( getActivity(), patios );
			auto_SearchPatio.setAdapter( adapterAutoComplete );

			auto_SearchPatio.setOnItemClickListener( new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {

					Patio patio = (Patio) auto_SearchPatio.getAdapter().getItem( position );
					List<Patio> selectedInfraction = new ArrayList<>();
					selectedInfraction.add( patio );
					adapterPatio = new ArrayAdapter<Patio>( getActivity(), android.R.layout.simple_list_item_1, selectedInfraction );
					lv_patios.setAdapter( adapterPatio );

					auto_SearchPatio.setText( WordUtils.capitalizeFully( patio.getPatioDescription() ) );
					auto_SearchPatio.setSelection( patio.getPatioDescription().length() );
					lv_patios.requestFocus();
					InitAutoFragmentActivity.hideKeyboard( lv_patios );
				}
			} );

			lv_patios.setOnItemClickListener( new OnItemClickListener() {

				@Override
				public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {
					InitAutoFragmentActivity.wasModified = true;

					Patio patio = (Patio) lv_patios.getAdapter().getItem( position );
					autoInfraction.setStreet( WordUtils.capitalizeFully( patio.getPatioType().trim() + " " + patio.getPatioDescription().trim() ) );
					// autoInfraction.setNeighborhood( patio.getNeighborhood()
					// );
					autoInfraction.setNumber( null );
					autoInfraction.setNeighborhood( null );

					et_street.setText( WordUtils.capitalizeFully( patio.getPatioType().trim() + " " + patio.getPatioDescription().trim() ) );
					// if ( patio.getNeighborhood() != null )
					// et_neighborhood.setText( WordUtils.capitalizeFully(
					// patio.getNeighborhood() ) );
					// else
					// et_neighborhood.setText( null );
					et_number.setText( null );
					et_neighborhood.setText( null );

					patioPosition = position;

					autoInfraction.save();
					dismiss();
				}
			} );

			if ( patioPosition != null )
				lv_patios.setSelection( patioPosition );

			AlertDialog.Builder patioDialog = new AlertDialog.Builder( getActivity() );
			patioDialog.setTitle( "Procurar logradouro" );
			patioDialog.setCancelable( true );
			patioDialog.setView( rootView );

			return patioDialog.create();
		}
	}

}
