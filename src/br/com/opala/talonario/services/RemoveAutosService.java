package br.com.opala.talonario.services;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.provider.MediaStore;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.Agent;
import br.com.opala.talonario.entities.AutoInfraction;
import br.com.opala.talonario.entities.Image;
import br.com.opala.talonario.repositories.AgentRepository;
import br.com.opala.talonario.repositories.AutoInfractionRepository;
import br.com.opala.talonario.repositories.SettingParameterRepository;

public class RemoveAutosService extends Service {

	private boolean notRunning = true;

	public static RemoveAutosService context;
	private final String TAG = RemoveAutosService.class.getSimpleName();
	private Integer TIME_TO_REMOVE_AUTOS;
	private Agent agent;

	@Override
	public IBinder onBind( Intent intent ) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		context = this;
	}

	@Override
	public int onStartCommand( Intent intent, int flags, int startId ) {

		if ( notRunning ) {
			TIME_TO_REMOVE_AUTOS = SettingParameterRepository.getTimeToRemoveAutos();
			if ( TIME_TO_REMOVE_AUTOS != null ) {
				Worker worker = new Worker( startId );
				worker.start();
				notRunning = false;
			}
		}

		return super.onStartCommand( intent, flags, startId );
	}

	public class Worker extends Thread {

		int startId;

		public Worker( int startId ) {
			this.startId = startId;
		}

		@Override
		public void run() {
			agent = AgentRepository.getCurrentAgent();
			int validadeAutos = SettingParameterRepository.getValidityAutos();
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis( System.currentTimeMillis() );

			List<AutoInfraction> autoInfractions = AutoInfractionRepository.getAllSent( agent.getUsername() );
			for ( final AutoInfraction auto : autoInfractions ) {
				Calendar calendarAuto = Calendar.getInstance();
				calendarAuto.setTime( auto.getCreatedAt() );
				calendarAuto.add( Calendar.DAY_OF_MONTH, validadeAutos );
				calendarAuto.set( Calendar.HOUR_OF_DAY, 0 );
				calendarAuto.set( Calendar.MINUTE, 0 );
				calendarAuto.set( Calendar.SECOND, 0 );

				if ( calendarAuto.compareTo( calendar ) == -1 ) {
					for ( Image image : auto.getPhotos() ) {
						File file = new File( image.getPath() );
						deleteAndRefresh( file.getAbsolutePath() );
					}

					auto.delete();
				} else {
				}

			}
			agendService();
			stopSelf( startId );
		}
	}

	private void agendService() {
		Intent intent = new Intent( getResources().getString( R.string.REMOVE_RECEIVER ) );
		PendingIntent pi = PendingIntent.getBroadcast( RemoveAutosService.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT );

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis( System.currentTimeMillis() );
		calendar.add( Calendar.MINUTE, TIME_TO_REMOVE_AUTOS );

		AlarmManager alarmManager = (AlarmManager) getSystemService( Context.ALARM_SERVICE );
		alarmManager.set( AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pi );

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	private void deleteAndRefresh( String absolutPath ) {
		// Set up the projection (we only need the ID)
		String[] projection = { MediaStore.Images.Media._ID };

		// Match on the file path
		String selection = MediaStore.Images.Media.DATA + " = ?";
		String[] selectionArgs = new String[] { absolutPath };

		// Query for the ID of the media matching the file path
		Uri queryUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
		ContentResolver contentResolver = context.getContentResolver();
		Cursor c = contentResolver.query( queryUri, projection, selection, selectionArgs, null );
		if ( c.moveToFirst() ) {
			// We found the ID. Deleting the item via the content
			// provider will also remove the file
			long idImage = c.getLong( c.getColumnIndexOrThrow( MediaStore.Images.Media._ID ) );
			Uri deleteUri = ContentUris.withAppendedId( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, idImage );
			contentResolver.delete( deleteUri, null, null );
		} else {
			// File not found in media store DB
		}
		c.close();
	}
}
