package br.com.opala.talonario.services;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.Agent;
import br.com.opala.talonario.entities.AutoInfraction;
import br.com.opala.talonario.entities.gson.AutoInfractionJsonSerializer;
import br.com.opala.talonario.enums.StatusCode;
import br.com.opala.talonario.enums.Urls;
import br.com.opala.talonario.repositories.AgentRepository;
import br.com.opala.talonario.repositories.AutoInfractionRepository;
import br.com.opala.talonario.repositories.SettingParameterRepository;
import br.com.opala.talonario.utils.VolleyErrorHelper;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SynchronizeAutosService extends Service {

	private boolean notRunning = true;

	public static SynchronizeAutosService context;
	private final String TAG = SynchronizeAutosService.class.getSimpleName();
	private Integer TIME_TO_SEND_AUTOS;
	private Agent agent;

	@Override
	public IBinder onBind( Intent intent ) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		context = this;
	}

	@Override
	public int onStartCommand( Intent intent, int flags, int startId ) {

		if ( notRunning ) {
			TIME_TO_SEND_AUTOS = SettingParameterRepository.getTimeToSendAutos();
			if ( TIME_TO_SEND_AUTOS != null ) {
				Worker worker = new Worker( startId );
				worker.start();
				notRunning = false;
			}
		}

		return super.onStartCommand( intent, flags, startId );
	}

	public class Worker extends Thread {

		int startId;

		public Worker( int startId ) {
			this.startId = startId;
		}

		@Override
		public void run() {
			Log.d( "TEST", "Enviando autos..." );
			agent = AgentRepository.getCurrentAgent();
			List<AutoInfraction> autoInfractions = AutoInfractionRepository.getAllNotSent( agent.getUsername() );
			for ( final AutoInfraction auto : autoInfractions ) {
				Log.d( "TEST", "Auto " + auto.getNumberAutoInfraction() );
				Gson gson = new GsonBuilder().registerTypeAdapter( AutoInfraction.class, new AutoInfractionJsonSerializer() ).create();
				String jsonAuto = gson.toJson( auto );
				JSONObject jsonObject = null;
				try {
					jsonObject = new JSONObject( jsonAuto );
				} catch ( JSONException e1 ) {
					e1.printStackTrace();
				}

				JsonObjectRequest jsonObjReq = new JsonObjectRequest( Method.POST, Urls.SEND_AUTOS, jsonObject,

				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse( JSONObject response ) {
						try {
							if ( response.getInt( "code" ) == ( StatusCode.CREATED.value() ) ) {
								Log.d( "TEST", "Auto " + auto.getNumberAutoInfraction() + " enviado!" );
								auto.setSent( true );
								auto.save();
							}
						} catch ( JSONException e ) {
							e.printStackTrace();
						}
					}

				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse( VolleyError error ) {
						String message = VolleyErrorHelper.getMessage( error, SynchronizeAutosService.this );
						VolleyLog.e( "TEST", error );
					}

				} ) {

					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						Map<String, String> params = new HashMap<String, String>();
						String token = AgentRepository.getCurrentAgent().getToken();
						params.put( "Token", token );
						return params;
					}
				};

				jsonObjReq.setRetryPolicy( new DefaultRetryPolicy( 25 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

				// Adding request to request queue
				AppController.getInstance().addToRequestQueue( jsonObjReq, TAG );
			}
			agendService();
			stopSelf( startId );
		}
	}

	private void agendService() {
		Intent intent = new Intent( getResources().getString( R.string.SYNC_RECEIVER ) );
		PendingIntent pi = PendingIntent.getBroadcast( SynchronizeAutosService.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT );

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis( System.currentTimeMillis() );
		calendar.add( Calendar.MINUTE, TIME_TO_SEND_AUTOS );

		AlarmManager alarmManager = (AlarmManager) getSystemService( Context.ALARM_SERVICE );
		alarmManager.set( AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pi );

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
}
