package br.com.opala.talonario.services;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.EditText;
import br.com.opala.talonario.entities.Image;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;

public class LocationTrackerService extends Service implements LocationListener {

	private final Context mContext;

	// flag for GPS status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;

	// flag for GPS status
	boolean canGetLocation = false;

	// flag dialog GPS
	boolean dialogShow = false;

	public static boolean obtainedAddress = false;

	Location location; // location
	Double latitude; // latitude
	Double longitude; // longitude

	// The minimum distance to change Updates in meters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 0 meters

	// The minimum time between updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 0; // 0 minute

	// Declaring a Location Manager
	protected LocationManager locationManager;

	public LocationTrackerService( Context context ) {
		this.mContext = context;
		LocationTrackerService.obtainedAddress = false;
		LocationTrackerService.address = null;
		getLocation();
	}

	public Location getLocation() {
		try {
			locationManager = (LocationManager) mContext.getSystemService( LOCATION_SERVICE );

			// // getting GPS status
			// isGPSEnabled = locationManager.isProviderEnabled(
			// LocationManager.GPS_PROVIDER );

			// getting network status
			isNetworkEnabled = locationManager.isProviderEnabled( LocationManager.NETWORK_PROVIDER );

			if ( !isGPSEnabled && !isNetworkEnabled && !dialogShow ) {
				// no network provider is enabled
				showSettingsAlert();
			} else {
				// First get location from Network Provider
				if ( isNetworkEnabled ) {
					this.canGetLocation = true;
					Log.d( "TEST", "NetworkEnabled..." );
					locationManager.removeUpdates( this );
					locationManager.requestLocationUpdates( LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this );
					if ( locationManager != null ) {
						location = locationManager.getLastKnownLocation( LocationManager.NETWORK_PROVIDER );
						if ( location != null ) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
						}
					}
				}
				// if ( isGPSEnabled ) {
				// if ( location == null ) {
				// this.canGetLocation = true;
				// locationManager.removeUpdates( this );
				// locationManager.requestLocationUpdates(
				// LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
				// MIN_DISTANCE_CHANGE_FOR_UPDATES, this );
				// Log.d( "GPS Enabled", "GPS Enabled" );
				// if ( locationManager != null ) {
				// location = locationManager.getLastKnownLocation(
				// LocationManager.GPS_PROVIDER );
				// if ( location != null ) {
				// latitude = location.getLatitude();
				// longitude = location.getLongitude();
				// }
				// }
				// }
				// }
			}

		} catch ( Exception e ) {
			e.printStackTrace();
		}

		return location;
	}

	/**
	 * Stop using GPS listener Calling this function will stop using GPS in your
	 * app
	 * */
	public void stopUsingLocation() {
		if ( locationManager != null ) {
			locationManager.removeUpdates( LocationTrackerService.this );
		}
	}

	/**
	 * Function to get latitude
	 * */
	public Double getLatitude() {
		if ( location != null ) {
			latitude = location.getLatitude();
		}

		// return latitude
		return latitude;
	}

	/**
	 * Function to get longitude
	 * */
	public Double getLongitude() {
		if ( location != null ) {
			longitude = location.getLongitude();
		}

		// return longitude
		return longitude;
	}

	/**
	 * Function to check GPS/wifi enabled
	 * 
	 * @return boolean
	 * */
	public boolean canGetLocation() {
		return this.canGetLocation;
	}

	/**
	 * Function to show settings alert dialog On pressing Settings button will
	 * lauch Settings Options
	 * */
	public void showSettingsAlert() {
		dialogShow = true;

		AlertDialog.Builder alertDialog = new AlertDialog.Builder( mContext );

		// Setting Dialog Title
		alertDialog.setTitle( "Configurar GPS" );

		// Setting Dialog Message
		alertDialog.setMessage( "GPS não está habilitado. Deseja ir para o menu de configurações?" );

		// On pressing Settings button
		alertDialog.setPositiveButton( "Configurações", new DialogInterface.OnClickListener() {
			@Override
			public void onClick( DialogInterface dialog, int which ) {
				Intent intent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
				mContext.startActivity( intent );
			}
		} );

		// on pressing cancel button
		alertDialog.setNegativeButton( "Cancelar", new DialogInterface.OnClickListener() {
			@Override
			public void onClick( DialogInterface dialog, int which ) {
				dialog.cancel();
			}
		} );

		// Showing Alert Message
		alertDialog.show();
	}

	@Override
	public void onLocationChanged( Location location ) {}

	@Override
	public void onProviderDisabled( String provider ) {}

	@Override
	public void onProviderEnabled( String provider ) {}

	@Override
	public void onStatusChanged( String provider, int status, Bundle extras ) {}

	@Override
	public IBinder onBind( Intent intent ) {
		return null;
	}

	private static Map<String, String> address = null;

	public static Map<String, String> getAddress( String lat, String lng, final Object... editTextAddress ) {

		if ( address != null && address.size() > 0 && !( editTextAddress[0] instanceof Image ) ) {
			( (EditText) editTextAddress[0] ).setText( address.get( "street" ) );
			( (EditText) editTextAddress[1] ).setText( address.get( "number" ) );
			( (EditText) editTextAddress[2] ).setText( address.get( "neighborhood" ) );
			return address;
		}
		address = new HashMap<>();

		new AsyncTask<String, Void, Map<String, String>>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			@Override
			protected Map<String, String> doInBackground( String... params ) {
				Log.i( "LOG", "Getting address..." );
				RequestFuture<JSONObject> future = RequestFuture.newFuture();
				String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=__LAT__,__LNG__&sensor=false";
				url = url.replaceAll( "__LAT__", params[0] );
				url = url.replaceAll( "__LNG__", params[1] );

				JsonObjectRequest jsonObjReq = new JsonObjectRequest( Method.GET, url, null, future, future ) {};

				jsonObjReq.setRetryPolicy( new DefaultRetryPolicy( 25 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

				// Adding request to request queue
				AppController.getInstance().addToRequestQueue( jsonObjReq, null );

				String roadName = null;
				try {
					JSONObject response = future.get();
					JSONArray jArray = response.getJSONArray( "results" );
					if ( jArray != null && jArray.length() > 0 ) {
						JSONObject oneObject = jArray.getJSONObject( 0 );
						// Pulling items from the array
						JSONArray addressComponents = oneObject.getJSONArray( "address_components" );
						Log.i( "LOG", "Address obtained: " + addressComponents.toString() );
						for ( int i = 0; i < addressComponents.length(); i++ ) {
							JSONObject component = addressComponents.getJSONObject( i );
							if ( component.getJSONArray( "types" ).get( 0 ).equals( "street_number" ) )
								address.put( "number", component.get( "long_name" ).toString() );
							else if ( component.getJSONArray( "types" ).get( 0 ).equals( "route" ) )
								address.put( "street", component.get( "long_name" ).toString() );
							else if ( component.getJSONArray( "types" ).get( 0 ).equals( "neighborhood" ) )
								address.put( "neighborhood", component.get( "long_name" ).toString() );
						}
						// roadName = oneObject.getString(
						// "formatted_address"
						// );
					}
				} catch ( InterruptedException e ) {
				} catch ( ExecutionException e ) {
					return null;
				} catch ( JSONException e ) {
					e.printStackTrace();
				}

				return address;
			}

			@Override
			protected void onPostExecute( Map<String, String> result ) {
				super.onPostExecute( result );
				if ( editTextAddress != null && result != null && !result.isEmpty() ) {
					Log.i( "LOG", "Address completed..." );
					obtainedAddress = true;
					if ( editTextAddress[0] instanceof EditText ) {
						( (EditText) editTextAddress[0] ).setText( result.get( "street" ) );
						( (EditText) editTextAddress[1] ).setText( result.get( "number" ) );
						( (EditText) editTextAddress[2] ).setText( result.get( "neighborhood" ) );
					} else if ( editTextAddress[0] instanceof Image ) {
						( (Image) editTextAddress[0] ).setAddress( result );
					}
				}
			}

		}.execute( lat, lng );

		return address;

	}

}