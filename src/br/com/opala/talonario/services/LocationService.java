package br.com.opala.talonario.services;

import java.text.DateFormat;
import java.util.Date;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Html;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

	private final Context mContext;

	private GoogleApiClient mGoogleApiClient;
	private LocationRequest mLocationRequest;

	public LocationService( Context context ) {
		this.mContext = context;
		Log.i( "LOG", "LocationService.onCreate()" );
		callConnection();
	}

	public void startLocation() {
		if ( mGoogleApiClient != null && mGoogleApiClient.isConnected() ) {
			startLocationUpdate();
		}
	}

	public void stopLocation() {
		if ( mGoogleApiClient != null ) {
			stopLocationUpdate();
		}
	}

	private synchronized void callConnection() {
		Log.i( "LOG", "LocationService.callConnection()" );
		mGoogleApiClient = new GoogleApiClient.Builder( mContext ).addOnConnectionFailedListener( this ).addConnectionCallbacks( this ).addApi( LocationServices.API ).build();
		mGoogleApiClient.connect();
	}

	private void initLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval( 5000 );
		mLocationRequest.setFastestInterval( 2000 );
		mLocationRequest.setPriority( LocationRequest.PRIORITY_HIGH_ACCURACY );
	}

	private void startLocationUpdate() {
		initLocationRequest();
		LocationServices.FusedLocationApi.requestLocationUpdates( mGoogleApiClient, mLocationRequest, this );
	}

	private void stopLocationUpdate() {
		LocationServices.FusedLocationApi.removeLocationUpdates( mGoogleApiClient, this );
	}

	// LISTENERS
	@Override
	public void onConnected( Bundle bundle ) {
		Log.i( "LOG", "LocationService.onConnected(" + bundle + ")" );

		Location location = LocationServices.FusedLocationApi.getLastLocation( mGoogleApiClient ); // PARA
		// JÁ
		// TER
		// UMA
		// COORDENADA
		// PARA
		// O
		// UPDATE
		// FEATURE
		// UTILIZAR
		if ( location != null )
			Log.i( "LOG", Html.fromHtml( "Location: " + location.getLatitude() + "<br />" + "Longitude: " + location.getLongitude() + "<br />" + "Bearing: " + location.getBearing() + "<br />" + "Altitude: " + location.getAltitude() + "<br />" + "Speed: " + location.getSpeed() + "<br />" + "Provider: " + location.getProvider() + "<br />" + "Accuracy: " + location.getAccuracy() + "<br />" + "Speed: " + DateFormat.getTimeInstance().format( new Date() ) + "<br />" ).toString() );

		startLocationUpdate();
	}

	@Override
	public void onConnectionSuspended( int i ) {
		Log.i( "LOG", "LocationService.onConnectionSuspended(" + i + ")" );
	}

	@Override
	public void onConnectionFailed( ConnectionResult connectionResult ) {
		Log.i( "LOG", "LocationService.onConnectionFailed(" + connectionResult + ")" );
	}

	@Override
	public void onLocationChanged( Location location ) {
		Log.i( "LOG", Html.fromHtml( "LocationChange: " + location.getLatitude() + "<br />" + "Longitude: " + location.getLongitude() + "<br />" + "Bearing: " + location.getBearing() + "<br />" + "Altitude: " + location.getAltitude() + "<br />" + "Speed: " + location.getSpeed() + "<br />" + "Provider: " + location.getProvider() + "<br />" + "Accuracy: " + location.getAccuracy() + "<br />" + "Speed: " + DateFormat.getTimeInstance().format( new Date() ) + "<br />" ).toString() );
	}

	@Override
	public IBinder onBind( Intent intent ) {
		return null;
	}
}