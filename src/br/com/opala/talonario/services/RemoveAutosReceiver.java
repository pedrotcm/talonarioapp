package br.com.opala.talonario.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class RemoveAutosReceiver extends BroadcastReceiver {

	@Override
	public void onReceive( Context context, Intent intent ) {
		context.startService( new Intent( context, RemoveAutosService.class ) );
	}

}
