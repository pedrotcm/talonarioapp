package br.com.opala.talonario.adapters;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.Infraction;

public class InfractionAutoCompleteAdapter extends ArrayAdapter<Infraction> {

	private final int resource;

	public InfractionAutoCompleteAdapter( Context context, int resource, List<Infraction> infractions ) {
		super( context, resource, infractions );
		this.resource = resource;
	}

	@Override
	public View getView( int position, View convertView, ViewGroup parent ) {

		if ( convertView == null ) {
			LayoutInflater li = LayoutInflater.from( getContext() );
			convertView = li.inflate( resource, null );
		}

		Infraction infraction = getItem( position );

		TextView tv_infraction = (TextView) convertView.findViewById( R.id.tv_textAdapter );
		tv_infraction.setText( Html.fromHtml( "<b><big>" + infraction.getCodeInfraction() + "</big></b>" ) );
		return convertView;
	}
}
