package br.com.opala.talonario.adapters;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import br.com.opala.talonario.R;

public class ObservationsAdapter extends ArrayAdapter<String> {

	private final Activity context;
	private final int resource;
	private final Set<Integer> mCheckedItems;
	private boolean loading;

	public ObservationsAdapter( Activity context, int resource, List<String> observations, List<String> observationsCheckeds ) {
		super( context, resource, observations );
		this.context = context;
		this.resource = resource;
		this.mCheckedItems = new LinkedHashSet<Integer>();
		this.loading = false;

		if ( loading == false && observationsCheckeds != null && !observationsCheckeds.isEmpty() ) {
			for ( String obsChecked : observationsCheckeds ) {
				for ( int i = 0; i < observations.size(); i++ ) {
					if ( observations.get( i ).equalsIgnoreCase( obsChecked ) )
						mCheckedItems.add( i );
				}
			}
			loading = true;
		}
	}

	public List<String> getItemsChecked() {
		List<String> observations = new ArrayList<String>();
		if ( mCheckedItems.iterator().hasNext() ) {
			for ( Integer i : mCheckedItems ) {
				observations.add( getItem( i ) );
			}
			return observations;
		} else
			return new ArrayList<>();
	}

	public String getString( int position ) {
		return getItem( position );
	}

	@Override
	public View getView( int position, View convertView, ViewGroup parent ) {

		String observation = getItem( position );

		final int pos = position;

		LayoutInflater inflater = context.getLayoutInflater();

		Holder holder;
		if ( convertView == null ) {
			convertView = inflater.inflate( resource, null );
			holder = new Holder();
			holder.tv_observation_box = (TextView) convertView.findViewById( R.id.tv_observation_box );
			holder.cb_observation = (CheckBox) convertView.findViewById( R.id.cb_observation );
			holder.tv_observation_box.setTag( holder.cb_observation );
			convertView.setTag( holder );
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.cb_observation.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				CheckBox cb = (CheckBox) v;
				if ( cb.isChecked() ) {
					mCheckedItems.add( pos );
				} else {
					mCheckedItems.remove( pos );
				}

			}
		} );

		OnClickListener tvClickListener = null;
		if ( tvClickListener == null )
			tvClickListener = new OnClickListener() {

				@Override
				public void onClick( View v ) {
					if ( ( (CheckBox) v.getTag() ).isChecked() ) {
						mCheckedItems.remove( pos );
					} else {
						mCheckedItems.add( pos );
					}
					notifyDataSetChanged();
				}
			};

		holder.tv_observation_box.setOnClickListener( tvClickListener );

		if ( !mCheckedItems.contains( pos ) ) {
			holder.cb_observation.setChecked( false );
		} else if ( mCheckedItems.contains( pos ) ) {
			holder.cb_observation.setChecked( true );
		}

		SpannableStringBuilder spannablecontent = new SpannableStringBuilder();
		if ( observation.contains( "Descrever" ) || observation.contains( "descrever" ) ) {
			Drawable myIcon = context.getResources().getDrawable( R.drawable.ic_action_about );
			myIcon.setBounds( 0, 0, holder.tv_observation_box.getLineHeight() + 5, holder.tv_observation_box.getLineHeight() + 5 );
			ImageSpan is = new ImageSpan( myIcon );

			spannablecontent.append( " " ).setSpan( is, spannablecontent.length() - 1, spannablecontent.length(), 0 );
			spannablecontent.append( " " + observation );
			spannablecontent.setSpan( new StyleSpan( Typeface.ITALIC ), 0, spannablecontent.length(), 0 );

			holder.cb_observation.setVisibility( View.GONE );
			holder.tv_observation_box.setOnClickListener( null );
		} else {
			spannablecontent.append( observation );

			spannablecontent.setSpan( new StyleSpan( Typeface.NORMAL ), 0, spannablecontent.length(), 0 );
			holder.cb_observation.setVisibility( View.VISIBLE );
			holder.tv_observation_box.setOnClickListener( tvClickListener );
		}
		holder.tv_observation_box.setText( spannablecontent );

		return convertView;
	}

	private static class Holder {
		public TextView tv_observation_box;
		public CheckBox cb_observation;
	}

}