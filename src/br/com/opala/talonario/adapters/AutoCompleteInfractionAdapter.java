package br.com.opala.talonario.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.Infraction;

public class AutoCompleteInfractionAdapter extends ArrayAdapter<Infraction> implements Filterable {
	private final Context context;
	private final List<Infraction> listFull;
	private List<Infraction> listAux;
	private Filter filter;
	private final LayoutInflater inflater;

	public AutoCompleteInfractionAdapter( Context context, List<Infraction> listFull ) {
		super( context, 0, 0, listFull );
		this.context = context;
		this.listFull = listFull;
		this.listAux = new ArrayList<Infraction>();
		this.inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
	}

	@Override
	public int getCount() {
		return ( listAux.size() );
	}

	@Override
	public Infraction getItem( int position ) {
		return ( listAux.get( position ) );
	}

	@Override
	public View getView( int position, View view, ViewGroup root ) {
		ViewHolder holder;

		if ( view == null ) {
			view = inflater.inflate( R.layout.adapter_list_text, null );
			holder = new ViewHolder();
			view.setTag( holder );

			holder.tvInfraction = (TextView) view.findViewById( R.id.tv_textAdapter );
		} else {
			holder = (ViewHolder) view.getTag();
		}

		String codeInfraction = listAux.get( position ).getCodeInfraction() + "" + listAux.get( position ).getDeployment();
		holder.tvInfraction.setText( Html.fromHtml( "<b><big>" + codeInfraction + "</big></b>" ) );

		return ( view );
	}

	static class ViewHolder {
		TextView tvInfraction;
	}

	// FILTER
	@Override
	public Filter getFilter() {
		if ( filter == null ) {
			filter = new ArrayFilter();
		}
		return ( filter );
	}

	private class ArrayFilter extends Filter {

		@Override
		protected FilterResults performFiltering( CharSequence constraint ) {
			FilterResults results = new FilterResults();
			String constraintString = ( constraint + "" ).toLowerCase();

			if ( constraint == null || constraint.length() == 0 ) {
				List<Infraction> list = new ArrayList<Infraction>( listFull );
				results.count = list.size();
				results.values = list;
			} else {
				int qtdConstraint = constraintString.length();

				// ((MainActivity) context).id = 1;
				// ArrayList<Infraction> newValues = (ArrayList<Infraction>)
				// HttpConnection.getInfractionListWeb("http://www.villopim.com.br/android/Infraction.php",
				// country, constraintString);

				ArrayList<Infraction> newValues = new ArrayList<Infraction>( listFull.size() );
				for ( int i = 0; i < listFull.size(); i++ ) {
					String item = String.valueOf( listFull.get( i ).getCodeInfraction() + "" + listFull.get( i ).getDeployment() );

					if ( item.substring( 0, qtdConstraint ).equalsIgnoreCase( constraintString ) || String.valueOf( listFull.get( i ).getCodeInfraction() + "" + listFull.get( i ).getDeployment() ).substring( 0, qtdConstraint ).equalsIgnoreCase( constraintString ) ) {
						newValues.add( listFull.get( i ) );
					}
				}

				results.count = newValues.size();
				results.values = newValues;
			}

			return ( results );
		}

		@Override
		protected void publishResults( CharSequence constraint, FilterResults results ) {
			if ( results.values != null ) { // && ((MainActivity) context).id >
											// 0){
				listAux = (ArrayList<Infraction>) results.values;
			} else {
				listAux = new ArrayList<Infraction>();
			}

			if ( results.count == 0 ) {
				notifyDataSetInvalidated();
			} else {
				notifyDataSetChanged();
			}
		}

	}
}
