package br.com.opala.talonario.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.Patio;

public class AutoCompletePatioAdapter extends ArrayAdapter<Patio> implements Filterable {
	private final Context context;
	private final List<Patio> listFull;
	private List<Patio> listAux;
	private Filter filter;
	private final LayoutInflater inflater;

	public AutoCompletePatioAdapter( Context context, List<Patio> listFull ) {
		super( context, 0, 0, listFull );
		this.context = context;
		this.listFull = listFull;
		this.listAux = new ArrayList<Patio>();
		this.inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
	}

	@Override
	public int getCount() {
		return ( listAux.size() );
	}

	@Override
	public Patio getItem( int position ) {
		return ( listAux.get( position ) );
	}

	@Override
	public View getView( int position, View view, ViewGroup root ) {
		ViewHolder holder;

		if ( view == null ) {
			view = inflater.inflate( R.layout.adapter_list_text, null );
			holder = new ViewHolder();
			view.setTag( holder );

			holder.tvPatio = (TextView) view.findViewById( R.id.tv_textAdapter );
		} else {
			holder = (ViewHolder) view.getTag();
		}

		String patioDescription = listAux.get( position ).getPatioDescription();
		holder.tvPatio.setText( Html.fromHtml( "<b><big>" + patioDescription + "</big></b>" ) );

		return ( view );
	}

	static class ViewHolder {
		TextView tvPatio;
	}

	// FILTER
	@Override
	public Filter getFilter() {
		if ( filter == null ) {
			filter = new ArrayFilter();
		}
		return ( filter );
	}

	private class ArrayFilter extends Filter {

		@Override
		protected FilterResults performFiltering( CharSequence constraint ) {
			FilterResults results = new FilterResults();
			String constraintString = ( constraint + "" ).toLowerCase();

			if ( constraint == null || constraint.length() == 0 ) {
				List<Patio> list = new ArrayList<Patio>( listFull );
				results.count = list.size();
				results.values = list;
			} else {
				int qtdConstraint = constraintString.length();
				// ((MainActivity) context).id = 1;
				// ArrayList<State> newValues = (ArrayList<State>)
				// HttpConnection.getStateListWeb("http://www.villopim.com.br/android/state.php",
				// country, constraintString);

				ArrayList<Patio> newValues = new ArrayList<Patio>( listFull.size() );
				for ( int i = 0; i < listFull.size(); i++ ) {
					String item = listFull.get( i ).getPatioDescription();
					item = item.toLowerCase();

					try {
						if ( item.substring( 0, qtdConstraint ).equalsIgnoreCase( constraintString ) || listFull.get( i ).getPatioDescription().toLowerCase().contains( constraintString ) ) {
							newValues.add( listFull.get( i ) );
						}
					} catch ( StringIndexOutOfBoundsException e ) {
					}
				}

				results.count = newValues.size();
				results.values = newValues;

			}

			return ( results );
		}

		@Override
		protected void publishResults( CharSequence constraint, FilterResults results ) {
			if ( results.values != null ) { // && ((MainActivity) context).id >
											// 0){
				listAux = (ArrayList<Patio>) results.values;
			} else {
				listAux = new ArrayList<Patio>();
			}

			if ( results.count == 0 ) {
				notifyDataSetInvalidated();
			} else {
				notifyDataSetChanged();
			}
		}

	}
}
