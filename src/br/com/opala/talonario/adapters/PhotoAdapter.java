package br.com.opala.talonario.adapters;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.Image;

public class PhotoAdapter extends ArrayAdapter<Image> {

	private final int resource;

	public PhotoAdapter( Context context, int resource, List<Image> photos ) {
		super( context, resource, photos );
		this.resource = resource;
	}

	@Override
	public View getView( int position, View convertView, ViewGroup parent ) {

		Image photo = getItem( position );

		Holder holder = new Holder();
		if ( convertView == null ) {
			LayoutInflater li = LayoutInflater.from( getContext() );
			convertView = li.inflate( resource, null );

			ImageView iv_photo = (ImageView) convertView.findViewById( R.id.iv_photo );

			Bitmap bitmap = BitmapFactory.decodeByteArray( photo.getImage(), 0, photo.getImage().length );

			holder.iv_photo = iv_photo;
			holder.bitmap = bitmap;

			convertView.setTag( holder );

		} else
			holder = (Holder) convertView.getTag();

		holder.iv_photo.setImageBitmap( holder.bitmap );

		return convertView;
	}

	private static class Holder {
		public ImageView iv_photo;
		public Bitmap bitmap;
	}

}
