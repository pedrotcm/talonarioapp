package br.com.opala.talonario.adapters;

import java.text.SimpleDateFormat;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.AutoInfraction;

public class AutoInfractionAdapter extends ArrayAdapter<AutoInfraction> {

	private final int resource;

	public AutoInfractionAdapter( Context context, int resource, List<AutoInfraction> infractions ) {
		super( context, resource, infractions );
		this.resource = resource;
	}

	@Override
	public View getView( int position, View convertView, ViewGroup parent ) {

		if ( convertView == null ) {
			LayoutInflater li = LayoutInflater.from( getContext() );
			convertView = li.inflate( resource, null );
		}

		AutoInfraction auto = getItem( position );

		SimpleDateFormat dateFormat = new SimpleDateFormat( "dd/MM/yyyy HH:mm" );

		TextView tv_textAdapter = (TextView) convertView.findViewById( R.id.tv_textAdapter );
		tv_textAdapter.setText( Html.fromHtml( "<b><i><big>" + auto.getPlaque() + "</big></i></b>" + "<br>" + auto.getNumberAutoInfraction() + "<br>" + dateFormat.format( auto.getCreatedAt() ) ) );

		return convertView;
	}
}
