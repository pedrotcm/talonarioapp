package br.com.opala.talonario.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.DriverOffenderVO;

public class DriverOffenderAdapter extends ArrayAdapter<DriverOffenderVO> {

	private final int resource;

	public DriverOffenderAdapter( Context context, int resource, List<DriverOffenderVO> driverOffender ) {
		super( context, resource, driverOffender );
		this.resource = resource;
	}

	@Override
	public View getView( int position, View convertView, ViewGroup parent ) {

		if ( convertView == null ) {
			LayoutInflater li = LayoutInflater.from( getContext() );
			convertView = li.inflate( resource, null );
		}

		DriverOffenderVO driverOffender = getItem( position );
		TextView tv_infraction = (TextView) convertView.findViewById( R.id.tv_name );
		TextView tv_cpfRg = (TextView) convertView.findViewById( R.id.tv_cpf_rg );
		tv_infraction.setText( driverOffender.getName() );
		tv_cpfRg.setText( driverOffender.getCpfRg() );

		return convertView;
	}
}
