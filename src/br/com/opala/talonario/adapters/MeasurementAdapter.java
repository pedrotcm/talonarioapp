package br.com.opala.talonario.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.MeasurementType;

public class MeasurementAdapter extends ArrayAdapter<MeasurementType> {

	private final int resource;
	private final String measurementPerformed;

	public MeasurementAdapter( Context context, int resource, List<MeasurementType> measurement, String measurementPerformed ) {
		super( context, resource, measurement );
		this.resource = resource;
		this.measurementPerformed = measurementPerformed;
	}

	@Override
	public View getView( int position, View convertView, ViewGroup parent ) {

		if ( convertView == null ) {
			LayoutInflater li = LayoutInflater.from( getContext() );
			convertView = li.inflate( resource, null );
		}

		MeasurementType measurement = getItem( position );
		TextView tv_measurementType = (TextView) convertView.findViewById( R.id.tv_measurementType );
		tv_measurementType.setText( measurement.getDescription() );
		TextView tv_measurementPerformed = (TextView) convertView.findViewById( R.id.tv_measurementPerformed );
		tv_measurementPerformed.setText( measurementPerformed );

		return convertView;
	}
}
