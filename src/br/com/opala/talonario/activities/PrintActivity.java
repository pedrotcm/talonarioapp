package br.com.opala.talonario.activities;

import java.text.SimpleDateFormat;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.AutoInfraction;
import br.com.opala.talonario.entities.Cancel;
import br.com.opala.talonario.entities.MeasurementType;
import br.com.opala.talonario.entities.Reason;
import br.com.opala.talonario.repositories.BaseRepository;
import br.com.opala.talonario.repositories.MeasurementTypeRepository;
import br.com.opala.talonario.utils.PdfCreate;
import br.com.opala.talonario.utils.ViewUtil;

import com.activeandroid.Model;

public class PrintActivity extends Activity {

	private LinearLayout bt_print;
	private AutoInfraction autoInfraction;
	// private Organ organ;
	// private City city;

	private TextView tv_numberAutoInfraction;
	private TextView tv_actuatorOrgan;
	private TextView tv_codeOrgan;
	private TextView tv_plaque;
	private TextView tv_renavam;
	private TextView tv_brand;
	private TextView tv_specie;
	// private TextView tv_model;
	// private TextView tv_color;
	private TextView tv_driverName;
	private TextView tv_driverCpfRg;
	private TextView tv_driverCnh;
	private TextView tv_stateCnh;
	private TextView tv_offenderName;
	private TextView tv_offenderCpfRg;
	private TextView tv_local;
	private TextView tv_dateHour;
	private TextView tv_codeCity;
	private TextView tv_codeInfraction;
	private TextView tv_descriptionInfraction;
	private TextView tv_legalSupport;
	private TextView tv_observation;
	private TextView tv_nameAgent;

	private LinearLayout ll_measurementPerformed;
	private LinearLayout ll_measurementType;
	private TextView tv_measurementPerformed;
	private TextView tv_measurementType;

	private LinearLayout ll_renavam;
	private LinearLayout ll_driver;
	private LinearLayout ll_driverName;
	private LinearLayout ll_driverCpfRg;
	private LinearLayout ll_driverCnh;
	private LinearLayout ll_stateCnh;
	private LinearLayout ll_offender;
	private LinearLayout ll_offenderName;
	private LinearLayout ll_offenderCpfRg;

	private TextView tv_reason;
	private EditText et_justification;
	private List<Reason> reasons;
	private AlertDialog dialogCancel;
	private Cancel cancel;
	private PdfCreate pdfCreate;

	// private TextView tv_otherInformation;

	public void init() {

		bt_print = (LinearLayout) findViewById( R.id.bt_print );

		tv_numberAutoInfraction = (TextView) findViewById( R.id.tv_numberAutoInfraction );
		tv_actuatorOrgan = (TextView) findViewById( R.id.tv_actuatorOrgan );
		tv_codeOrgan = (TextView) findViewById( R.id.tv_codeOrgan );
		tv_plaque = (TextView) findViewById( R.id.tv_plaque );
		tv_renavam = (TextView) findViewById( R.id.tv_renavam );
		tv_brand = (TextView) findViewById( R.id.tv_brand );
		tv_specie = (TextView) findViewById( R.id.tv_specie );
		// tv_model = (TextView) findViewById( R.id.tv_model );
		// tv_color = (TextView) findViewById( R.id.tv_color );
		tv_driverName = (TextView) findViewById( R.id.tv_driverName );
		tv_driverCpfRg = (TextView) findViewById( R.id.tv_driverCpfRg );
		tv_driverCnh = (TextView) findViewById( R.id.tv_driverCnh );
		tv_stateCnh = (TextView) findViewById( R.id.tv_stateCnh );
		tv_offenderName = (TextView) findViewById( R.id.tv_offenderName );
		tv_offenderCpfRg = (TextView) findViewById( R.id.tv_offenderCpfRg );
		tv_local = (TextView) findViewById( R.id.tv_local );
		tv_dateHour = (TextView) findViewById( R.id.tv_dateHour );
		tv_codeCity = (TextView) findViewById( R.id.tv_codeCity );
		tv_codeInfraction = (TextView) findViewById( R.id.tv_codeInfraction );
		tv_descriptionInfraction = (TextView) findViewById( R.id.tv_descriptionInfraction );
		tv_legalSupport = (TextView) findViewById( R.id.tv_legalSupport );
		tv_observation = (TextView) findViewById( R.id.tv_observation );
		tv_nameAgent = (TextView) findViewById( R.id.tv_nameAgent );
		// tv_otherInformation = (TextView) findViewById(
		// R.id.tv_otherInformation );

		ll_measurementPerformed = (LinearLayout) findViewById( R.id.ll_measurementPerformed );
		ll_measurementType = (LinearLayout) findViewById( R.id.ll_measurementType );
		tv_measurementPerformed = (TextView) findViewById( R.id.tv_measurementPerformed );
		tv_measurementType = (TextView) findViewById( R.id.tv_measurementType );

		ll_renavam = (LinearLayout) findViewById( R.id.ll_renavam );
		ll_driver = (LinearLayout) findViewById( R.id.ll_driver );
		ll_driverName = (LinearLayout) findViewById( R.id.ll_driverName );
		ll_driverCpfRg = (LinearLayout) findViewById( R.id.ll_driverCpfRg );
		ll_driverCnh = (LinearLayout) findViewById( R.id.ll_driverCnh );
		ll_stateCnh = (LinearLayout) findViewById( R.id.ll_stateCnh );
		ll_offender = (LinearLayout) findViewById( R.id.ll_offender );
		ll_offenderName = (LinearLayout) findViewById( R.id.ll_offenderName );
		ll_offenderCpfRg = (LinearLayout) findViewById( R.id.ll_offenderCpfRg );

		reasons = (List<Reason>) BaseRepository.getAll( Reason.class );
		reasons.add( 0, new Reason( "Selecione um motivo" ) );
		cancel = new Cancel();

		Long idAutoInfraction = getIntent().getExtras().getLong( "idAutoInfraction" );
		autoInfraction = Model.load( AutoInfraction.class, idAutoInfraction );

		// CryptUtil.encryptObject( autoInfraction );
		pdfCreate = new PdfCreate( autoInfraction, getApplicationContext() );

		getActionBar().setIcon( getResources().getDrawable( R.drawable.ic_bar_app ) );

	}

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_print );

		init();

		getActionBar().setTitle( getResources().getString( R.string.title_print ) );

		bt_print.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				pdfCreate.createDocument();
			}
		} );

		tv_numberAutoInfraction.setText( autoInfraction.getNumberAutoInfraction() );
		tv_actuatorOrgan.setText( autoInfraction.getOrgan() );
		tv_codeOrgan.setText( autoInfraction.getCodeOrgan().toString() );
		tv_plaque.setText( autoInfraction.getPlaque() );
		if ( autoInfraction.getRenavam() == null ) {
			ll_renavam.setVisibility( View.GONE );
		} else
			tv_renavam.setText( autoInfraction.getRenavam() );
		if ( autoInfraction.getBrand().equalsIgnoreCase( getResources().getString( R.string.option_other ) ) )
			tv_brand.setText( autoInfraction.getOtherBrand() );
		else
			tv_brand.setText( autoInfraction.getBrand() );
		tv_specie.setText( autoInfraction.getSpecie() );
		// tv_model.setText( autoInfraction.getModel() );
		// tv_color.setText( autoInfraction.getColor() );
		if ( autoInfraction.getDriverName() == null && autoInfraction.getDriverCpfRg() == null && autoInfraction.getDriverCnh() == null && autoInfraction.getStateCnh() == null )
			ll_driver.setVisibility( View.GONE );
		else {
			if ( autoInfraction.getDriverName() != null )
				tv_driverName.setText( autoInfraction.getDriverName() );
			else
				ll_driverName.setVisibility( View.GONE );

			if ( autoInfraction.getDriverCpfRg() != null )
				tv_driverCpfRg.setText( autoInfraction.getDriverCpfRg() );
			else
				ll_driverCpfRg.setVisibility( View.GONE );

			if ( autoInfraction.getDriverCnh() != null )
				tv_driverCnh.setText( autoInfraction.getDriverCnh() );
			else
				ll_driverCnh.setVisibility( View.GONE );

			if ( autoInfraction.getStateCnh() != null )
				tv_stateCnh.setText( autoInfraction.getStateCnh() );
			else
				ll_stateCnh.setVisibility( View.GONE );
		}

		if ( autoInfraction.getOffenderName() == null && autoInfraction.getOffenderCpfRg() == null )
			ll_offender.setVisibility( View.GONE );
		else {
			if ( autoInfraction.getOffenderName() != null )
				tv_offenderName.setText( autoInfraction.getOffenderName() );
			else
				ll_offenderName.setVisibility( View.GONE );
			if ( autoInfraction.getOffenderCpfRg() != null )
				tv_offenderCpfRg.setText( autoInfraction.getOffenderCpfRg() );
			else
				ll_offenderCpfRg.setVisibility( View.GONE );
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat( "dd/MM/yyyy HH:mm" );
		tv_dateHour.setText( dateFormat.format( autoInfraction.getCreatedAt() ) );
		tv_local.setText( autoInfraction.getCompleteLocal() );
		tv_codeCity.setText( autoInfraction.getCodeCity().toString() );

		tv_codeInfraction.setText( autoInfraction.getCodeInfraction() + "" + autoInfraction.getDeployment() );
		tv_descriptionInfraction.setText( autoInfraction.getDescription() );
		tv_legalSupport.setText( "CTB, Art. " + autoInfraction.getLegalSupport() );
		tv_observation.setText( autoInfraction.getObservations() );

		if ( autoInfraction.getMeasurementType() != null && autoInfraction.getMeasurementPerformed() != null ) {
			MeasurementType measurementType = MeasurementTypeRepository.getMeasurementTypeByCode( autoInfraction.getMeasurementType() );
			tv_measurementType.setText( measurementType.getDescription() );
			tv_measurementPerformed.setText( autoInfraction.getMeasurementPerformed() );
		} else {
			ll_measurementType.setVisibility( View.GONE );
			ll_measurementPerformed.setVisibility( View.GONE );
		}

		tv_nameAgent.setText( autoInfraction.getAgentUsername() );
		// tv_otherInformation.setText( autoInfraction.getOtherInformation() );

	}

	@Override
	public boolean onCreateOptionsMenu( Menu menu ) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate( R.menu.menu_cancel_and_accept, menu );
		return super.onCreateOptionsMenu( menu );
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item ) {
		switch ( item.getItemId() ) {
			case R.id.action_accept:
				pdfCreate.deletePdf();
				finish();
				return true;
			case R.id.action_cancel:
				dialogCancel();
				pdfCreate.deletePdf();
				return true;
			default:
				return super.onOptionsItemSelected( item );
		}
	}

	@Override
	public boolean onKeyDown( int keyCode, KeyEvent event ) {
		return false;
	}

	public void dialogCancel() {
		LayoutInflater li = (LayoutInflater) this.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		final View view = li.inflate( R.layout.dialog_cancel, null );

		et_justification = (EditText) view.findViewById( R.id.et_justification );
		ViewUtil.customMultilineEnter( this, et_justification );

		final Spinner sp_reason = (Spinner) view.findViewById( R.id.sp_reason );
		sp_reason.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {
				Reason selected = (Reason) sp_reason.getAdapter().getItem( position );
				if ( position != 0 )
					cancel.setReason( selected.getReason() );

				tv_reason = (TextView) view;
			}

			@Override
			public void onNothingSelected( AdapterView<?> parent ) {

			}
		} );

		ArrayAdapter<Reason> adapterReason = new ArrayAdapter<Reason>( this, android.R.layout.simple_spinner_dropdown_item, reasons );
		sp_reason.setAdapter( adapterReason );

		AlertDialog.Builder builder = new AlertDialog.Builder( this );
		builder.setTitle( "Solicitar cancelamento" );
		builder.setView( view );

		builder.setPositiveButton( "Não", new DialogInterface.OnClickListener() {

			@Override
			public void onClick( DialogInterface dialog, int which ) {
				ViewUtil.hideKeyboard( PrintActivity.this, view );
				dialogCancel.dismiss();
			}
		} );

		builder.setNegativeButton( "Sim", new DialogInterface.OnClickListener() {
			@Override
			public void onClick( DialogInterface dialog, int which ) {}
		} );

		dialogCancel = builder.create();
		dialogCancel.show();

		dialogCancel.getButton( AlertDialog.BUTTON_NEGATIVE ).setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				if ( saveCancelValues() ) {
					cancel.setJustification( et_justification.getText().toString() );
					cancel.save();

					autoInfraction.setCancel( cancel );
					autoInfraction.save();
					dialogCancel.dismiss();
					( (Activity) PrintActivity.this ).finish();
				}
			}
		} );

	}

	public boolean saveCancelValues() {
		if ( cancel.getReason() == null ) {
			tv_reason.setError( "" );
			Toast toast = Toast.makeText( this, this.getResources().getString( R.string.error_required ), Toast.LENGTH_LONG );
			toast.setGravity( Gravity.CENTER, 0, 0 );
			toast.show();
			return false;
		}

		if ( et_justification.getText().toString().equals( "" ) ) {
			et_justification.requestFocus();
			et_justification.setError( this.getResources().getString( R.string.error_required ) );
			return false;
		}

		return true;
	}

}
