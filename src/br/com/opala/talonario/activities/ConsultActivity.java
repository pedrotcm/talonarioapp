package br.com.opala.talonario.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import br.com.opala.talonario.R;

public class ConsultActivity extends Activity {

	LinearLayout bt_consultAuto;

	public void init() {
		bt_consultAuto = (LinearLayout) findViewById( R.id.bt_consultAuto );

		getActionBar().setDisplayHomeAsUpEnabled( true );
		getActionBar().setLogo( R.drawable.ic_action_previous );
		getActionBar().setTitle( getResources().getString( R.string.title_consult ) );
	}

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_consult );

		init();

		bt_consultAuto.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				startActivity( new Intent( ConsultActivity.this, ConsultAutoActivity.class ) );
			}
		} );
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item ) {
		switch ( item.getItemId() ) {
			case android.R.id.home:
				finish();
				return true;
			default:
				return super.onOptionsItemSelected( item );
		}
	}

	@Override
	public boolean onKeyDown( int keyCode, KeyEvent event ) {
		return false;
	}
}
