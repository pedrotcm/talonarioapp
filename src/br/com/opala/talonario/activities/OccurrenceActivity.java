package br.com.opala.talonario.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.ContactOccurrence;
import br.com.opala.talonario.entities.TelephoneOccurrence;
import br.com.opala.talonario.repositories.BaseRepository;

public class OccurrenceActivity extends Activity {

	private Spinner sp_organOccurrence;
	private ContactOccurrence organOccurence;
	private ArrayAdapter<ContactOccurrence> adapterOccurrence;
	private List<ContactOccurrence> organOccurrences;
	private LinearLayout bt_call;
	private LinearLayout bt_sms;
	private EditText et_description;

	public void init() {

		sp_organOccurrence = (Spinner) findViewById( R.id.sp_organOccurrence );
		et_description = (EditText) findViewById( R.id.et_description );
		bt_call = (LinearLayout) findViewById( R.id.bt_call );
		bt_sms = (LinearLayout) findViewById( R.id.bt_sms );

		getActionBar().setDisplayHomeAsUpEnabled( true );
		getActionBar().setLogo( R.drawable.ic_action_previous );
		getActionBar().setTitle( getResources().getString( R.string.title_occorrence ) );

		organOccurrences = (List<ContactOccurrence>) BaseRepository.getAll( ContactOccurrence.class );
		if ( organOccurrences == null )
			organOccurrences = new ArrayList<ContactOccurrence>();
		organOccurrences.add( 0, new ContactOccurrence( "Selecione um contato" ) );

	}

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_occurrence );

		init();

		adapterOccurrence = new ArrayAdapter<ContactOccurrence>( this, android.R.layout.simple_spinner_dropdown_item, organOccurrences );
		sp_organOccurrence.setAdapter( adapterOccurrence );
		sp_organOccurrence.setOnItemSelectedListener( new OnItemSelectedListener() {

			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {
				organOccurence = (ContactOccurrence) parent.getSelectedItem();
			}

			@Override
			public void onNothingSelected( AdapterView<?> parent ) {

			}
		} );

		bt_call.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				if ( organOccurence.getId() != null ) {
					Intent i = new Intent( Intent.ACTION_DIAL );
					String p = null;
					for ( TelephoneOccurrence phone : organOccurence.getTelephones() ) {
						if ( phone.getStandard() ) {
							p = "tel:" + phone.getTelephone().toString();
							System.out.println( "TELEFONE PADRAO: " + p );
							break;
						}
					}
					i.setData( Uri.parse( p ) );
					startActivity( i );
				} else {
					toastSelect( "Selecione um contato" );
				}
			}
		} );

		bt_sms.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				if ( organOccurence.getId() != null ) {
					String telephones = "";
					for ( TelephoneOccurrence phones : organOccurence.getTelephones() ) {
						telephones += phones.getTelephone() + ";";
					}
					// String phone = "tel:" +
					// organOccurence.getTelephones().get( 0
					// ).getTelephone().toString();
					Uri uri = Uri.parse( "smsto:" + telephones );
					Intent it = new Intent( Intent.ACTION_SENDTO, uri );
					it.putExtra( "sms_body", et_description.getText().toString() );
					startActivity( it );
				} else {
					toastSelect( "Selecione um contato" );
				}
			}
		} );
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item ) {
		switch ( item.getItemId() ) {
			case android.R.id.home:
				finish();
				return true;
			default:
				return super.onOptionsItemSelected( item );
		}
	}

	@Override
	public boolean onKeyDown( int keyCode, KeyEvent event ) {
		return false;
	}

	private void toastSelect( String msg ) {
		Toast toast = Toast.makeText( this, msg, Toast.LENGTH_LONG );
		toast.setGravity( Gravity.CENTER, 0, 0 );
		toast.show();
	}
}
