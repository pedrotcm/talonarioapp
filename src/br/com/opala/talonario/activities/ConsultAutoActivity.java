package br.com.opala.talonario.activities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import br.com.opala.talonario.R;
import br.com.opala.talonario.adapters.AutoInfractionAdapter;
import br.com.opala.talonario.entities.Agent;
import br.com.opala.talonario.entities.AutoInfraction;
import br.com.opala.talonario.repositories.AgentRepository;
import br.com.opala.talonario.repositories.AutoInfractionRepository;
import br.com.opala.talonario.repositories.SettingParameterRepository;
import br.com.opala.talonario.utils.MaskedWatcher;
import br.com.opala.talonario.utils.ViewUtil;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

public class ConsultAutoActivity extends FragmentActivity {

	private LinearLayout bt_search;
	private LinearLayout bt_clear;
	private ListView lv_consult;
	private EditText et_plaque;
	private EditText et_plaqueNumber;
	private EditText et_dateConsult;
	private List<AutoInfraction> autoInfractions;
	private AutoInfractionAdapter adapterAutoInfraction;
	private Agent agent;

	private CaldroidFragment dialogCaldroidFragment;
	private String dialogTag;
	final SimpleDateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy" );
	private Bundle bundleState;
	// private Parcelable state;

	private int VALID_DAYS;
	private int indexList;
	private int topList;

	public void init() {
		bt_search = (LinearLayout) findViewById( R.id.bt_search );
		bt_clear = (LinearLayout) findViewById( R.id.bt_clear );
		lv_consult = (ListView) findViewById( R.id.lv_consult );
		et_plaque = (EditText) findViewById( R.id.et_plaque );
		et_plaqueNumber = (EditText) findViewById( R.id.et_plaqueNumber );
		et_dateConsult = (EditText) findViewById( R.id.et_dateConsult );

		autoInfractions = new ArrayList<>();

		getActionBar().setDisplayHomeAsUpEnabled( true );
		getActionBar().setLogo( R.drawable.ic_action_previous );
		getActionBar().setTitle( getResources().getString( R.string.title_consult_auto ) );

		VALID_DAYS = SettingParameterRepository.getValidityAutos();
		agent = AgentRepository.getCurrentAgent();

		et_plaque.setFilters( new InputFilter[] { new InputFilter() {
			@Override
			public CharSequence filter( CharSequence src, int start, int end, Spanned dst, int dstart, int dend ) {
				// if ( src.equals( "" ) ) { // for backspace
				// return src;
				// }
				if ( src.toString().matches( "[A-Z]+" ) ) {
					return src;
				}
				return "";
			}
		} } );

		new MaskedWatcher( et_plaque, "###" );

		et_plaque.addTextChangedListener( new TextWatcher() {

			@Override
			public void onTextChanged( CharSequence s, int start, int before, int count ) {
				if ( s.length() == 3 && et_plaqueNumber.getText().toString().length() < 4 )
					et_plaqueNumber.requestFocus();
				if ( s.length() == 3 && et_plaqueNumber.getText().toString().length() == 4 )
					ViewUtil.hideKeyboard( getApplicationContext(), et_plaque );
			}

			@Override
			public void beforeTextChanged( CharSequence s, int start, int count, int after ) {}

			@Override
			public void afterTextChanged( Editable s ) {}
		} );

		et_plaqueNumber.addTextChangedListener( new TextWatcher() {

			@Override
			public void onTextChanged( CharSequence s, int start, int before, int count ) {
				if ( s.length() == 4 )
					ViewUtil.hideKeyboard( getApplicationContext(), et_plaqueNumber );
			}

			@Override
			public void beforeTextChanged( CharSequence s, int start, int count, int after ) {}

			@Override
			public void afterTextChanged( Editable s ) {}
		} );
	}

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_consult_auto );
		init();

		bundleState = savedInstanceState;

		adapterAutoInfraction = new AutoInfractionAdapter( this, R.layout.adapter_list_text, autoInfractions );
		lv_consult.setAdapter( adapterAutoInfraction );
		// state = lv_consult.onSaveInstanceState();
		listSaveInstance();

		lv_consult.setOnItemClickListener( new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {
				AutoInfraction auto = (AutoInfraction) lv_consult.getAdapter().getItem( position );
				Bundle extras = new Bundle();
				extras.putLong( "idAutoInfraction", auto.getId() );
				Intent intent = new Intent( getApplicationContext(), PrintActivity.class );
				intent.putExtras( extras );
				startActivity( intent );

			}
		} );

		et_dateConsult.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				if ( dialogCaldroidFragment == null )
					dialogDateSelect();
				else
					dialogCaldroidFragment.show( getSupportFragmentManager(), dialogTag );
			}
		} );

		bt_search.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				String plaque = et_plaque.getText().toString() + "-" + et_plaqueNumber.getText().toString();
				try {
					autoInfractions = AutoInfractionRepository.findAutoInfractionByAgentAndPlaqueAndDate( agent.getUsername(), plaque, formatter.parse( et_dateConsult.getText().toString() ) );
				} catch ( ParseException e ) {
					autoInfractions = AutoInfractionRepository.findAutoInfractionByAgentAndPlaqueAndDate( agent.getUsername(), plaque, null );
				}
				adapterAutoInfraction = new AutoInfractionAdapter( getApplicationContext(), R.layout.adapter_list_text, autoInfractions );
				lv_consult.setAdapter( adapterAutoInfraction );
				ViewUtil.hideKeyboard( getApplicationContext(), et_plaque );
			}
		} );

		bt_clear.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				et_plaque.setText( null );
				et_plaqueNumber.setText( null );
				et_dateConsult.setText( null );
				et_plaque.requestFocus();
			}
		} );

	}

	private void listSaveInstance() {
		indexList = lv_consult.getFirstVisiblePosition();
		View v = lv_consult.getChildAt( 0 );
		topList = ( v == null ) ? 0 : ( v.getTop() - lv_consult.getPaddingTop() );
	}

	@Override
	protected void onResume() {
		super.onResume();
		autoInfractions = AutoInfractionRepository.findAutoInfractionByAgentAndPlaqueAndDate( agent.getUsername(), et_plaque.getText().toString(), null );
		adapterAutoInfraction = new AutoInfractionAdapter( getApplicationContext(), R.layout.adapter_list_text, autoInfractions );
		lv_consult.setAdapter( adapterAutoInfraction );

		lv_consult.setSelectionFromTop( indexList, topList );

		// lv_consult.onRestoreInstanceState( state );
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item ) {
		switch ( item.getItemId() ) {
			case android.R.id.home:
				finish();
				return true;
			default:
				return super.onOptionsItemSelected( item );
		}
	}

	@Override
	public boolean onKeyDown( int keyCode, KeyEvent event ) {
		return false;
	}

	// Setup listener
	final CaldroidListener listener = new CaldroidListener() {

		@Override
		public void onSelectDate( Date date, View view ) {
			et_dateConsult.setText( formatter.format( date ) );
			dialogCaldroidFragment.dismiss();
		}

		@Override
		public void onChangeMonth( int month, int year ) {
			String text = "month: " + month + " year: " + year;
		}

		@Override
		public void onLongClickDate( Date date, View view ) {}

	};

	private void dialogDateSelect() {
		// Setup caldroid to use as dialog
		dialogCaldroidFragment = new CaldroidFragment();
		dialogCaldroidFragment.setCaldroidListener( listener );

		// If activity is recovered from rotation
		dialogTag = "CALDROID_DIALOG_FRAGMENT";
		if ( bundleState != null ) {
			dialogCaldroidFragment.restoreDialogStatesFromKey( getSupportFragmentManager(), bundleState, "DIALOG_CALDROID_SAVED_STATE", dialogTag );
			Bundle args = dialogCaldroidFragment.getArguments();
			if ( args == null ) {
				args = new Bundle();
				dialogCaldroidFragment.setArguments( args );
			}
			args.putString( CaldroidFragment.DIALOG_TITLE, "Selecione uma data" );
		} else {
			// Setup arguments
			Bundle bundle = new Bundle();
			// Setup dialogTitle
			bundle.putString( CaldroidFragment.DIALOG_TITLE, "Selecione uma data" );
			bundle.putBoolean( CaldroidFragment.ENABLE_SWIPE, false );
			// bundle.putBoolean( CaldroidFragment.SHOW_NAVIGATION_ARROWS, false
			// );

			dialogCaldroidFragment.setArguments( bundle );
		}

		dialogCaldroidFragment.show( getSupportFragmentManager(), dialogTag );

		Calendar cal = Calendar.getInstance();
		// Min date
		cal.add( Calendar.DATE, -VALID_DAYS );
		Date minDate = cal.getTime();
		// Max date
		cal = Calendar.getInstance();
		Date maxDate = cal.getTime();

		dialogCaldroidFragment.setMinDate( minDate );
		dialogCaldroidFragment.setMaxDate( maxDate );
		dialogCaldroidFragment.refreshView();
	}

	/**
	 * Save current states of the Caldroid here
	 */
	@Override
	protected void onSaveInstanceState( Bundle outState ) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState( outState );

		if ( dialogCaldroidFragment != null ) {
			dialogCaldroidFragment.saveStatesToKey( outState, "DIALOG_CALDROID_SAVED_STATE" );
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		listSaveInstance();
		// state = lv_consult.onSaveInstanceState();
	}

}
