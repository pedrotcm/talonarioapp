package br.com.opala.talonario.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.Agent;
import br.com.opala.talonario.entities.AutoInfraction;
import br.com.opala.talonario.entities.Brand;
import br.com.opala.talonario.entities.Infraction;
import br.com.opala.talonario.entities.Organ;
import br.com.opala.talonario.entities.PackageNumbers;
import br.com.opala.talonario.entities.Patio;
import br.com.opala.talonario.entities.Reason;
import br.com.opala.talonario.entities.SettingParameter;
import br.com.opala.talonario.entities.Specie;
import br.com.opala.talonario.entities.gson.AutoInfractionJsonSerializer;
import br.com.opala.talonario.entities.gson.DataJsonDeserializer;
import br.com.opala.talonario.enums.StatusCode;
import br.com.opala.talonario.enums.Urls;
import br.com.opala.talonario.repositories.AutoInfractionRepository;
import br.com.opala.talonario.repositories.BaseRepository;
import br.com.opala.talonario.repositories.OrganRepository;
import br.com.opala.talonario.repositories.PackageNumberRepository;
import br.com.opala.talonario.repositories.SettingParameterRepository;
import br.com.opala.talonario.services.AppController;
import br.com.opala.talonario.services.LocationTrackerService;
import br.com.opala.talonario.utils.DialogUtil;
import br.com.opala.talonario.utils.VolleyErrorHelper;

import com.activeandroid.Model;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MainActivity extends Activity {

	private Spinner sp_organ;
	private TextView tv_organAgent;
	private TextView tv_autoCount;
	private LinearLayout ll_autoCount;
	private PackageNumbers packageNumbers;
	private Agent agent;
	private Organ organSelected;
	private int qntAutos;
	private int qntAutosSaved;
	private boolean syncOk;
	private Integer qntAutoNumbers;
	private Integer minAutosWarn;

	private final String TAG = MainActivity.class.getSimpleName();
	private SettingParameter setting;

	private static final int PICK_FROM_CAMERA = 1;
	private Uri mImageCaptureUri;
	private Date dateImagePicked;
	private String imeiDevice;

	public void init() {
		sp_organ = (Spinner) findViewById( R.id.sp_organ );
		tv_organAgent = (TextView) findViewById( R.id.tv_organAgent );
		tv_autoCount = (TextView) findViewById( R.id.tv_autoCount );
		ll_autoCount = (LinearLayout) findViewById( R.id.ll_autoCount );

		tv_autoCount.setText( "0" );
		ll_autoCount.setBackground( getResources().getDrawable( R.drawable.autocount_main_background_warn ) );
		// ll_teste = (LinearLayout) findViewById( R.id.ll_camera );
		// SemiCircleDrawable semiCircleNormal = new SemiCircleDrawable(
		// getResources().getColor( R.color.blue_normal ), Direction.TOP );
		// SemiCircleDrawable semiCirclePressed = new SemiCircleDrawable(
		// getResources().getColor( R.color.blue_pressed ), Direction.TOP );
		// StateListDrawable states = new StateListDrawable();
		// states.addState( new int[] { android.R.attr.state_pressed },
		// semiCirclePressed );
		// // states.addState( new int[] { android.R.attr.state_focused },
		// // getResources().getDrawable( R.drawable.focused ) );
		// states.addState( new int[] {}, semiCircleNormal );
		// ll_teste.setBackgroundDrawable( states );

		Long idAgent = getIntent().getExtras().getLong( "idAgent" );
		agent = Model.load( Agent.class, idAgent );

		checkData();
		setting = (SettingParameter) BaseRepository.getLast( SettingParameter.class );

		TelephonyManager telephonyManager = null;
		telephonyManager = (TelephonyManager) getSystemService( TELEPHONY_SERVICE );
		imeiDevice = telephonyManager.getDeviceId();

		getActionBar().setIcon( getResources().getDrawable( R.drawable.ic_bar_app ) );

	}

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_main );

		init();

		tv_organAgent.setText( agent.getName() );

		ArrayAdapter<Organ> adapterOrgan = new ArrayAdapter<Organ>( this, android.R.layout.simple_spinner_dropdown_item, agent.getOrgans() );
		if ( agent.getOrgans().size() == 1 ) {
			sp_organ.setEnabled( false );
			sp_organ.setClickable( false );
		}
		sp_organ.setAdapter( adapterOrgan );
		sp_organ.setOnItemSelectedListener( new OnItemSelectedListener() {

			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {
				if ( organSelected != null && (Organ) parent.getSelectedItem() != organSelected ) {
					packageNumbers = PackageNumberRepository.getPackageByAgentAndOrgan( agent.getUsername(), organSelected.getCodeOrgan() );
					countQntAutos();
				}
				organSelected = (Organ) parent.getSelectedItem();

			}

			@Override
			public void onNothingSelected( AdapterView<?> parent ) {

			}
		} );

		LinearLayout bt_initAuto = (LinearLayout) findViewById( R.id.bt_initAuto );
		bt_initAuto.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				if ( checkData() ) {
					if ( packageNumbers == null || ( packageNumbers.getEndNumber().equals( packageNumbers.getCurrentNumber() ) ) ) {
						Toast toast = Toast.makeText( getApplicationContext(), "Não há auto de infração disponível para utilização. Selecione no menu a opcão 'Obter talonário de auto'.", Toast.LENGTH_LONG );
						toast.setGravity( Gravity.CENTER, 0, 0 );
						toast.show();
					} else {
						Bundle extras = new Bundle();
						extras.putLong( "idOrganSelected", organSelected.getId() );
						Intent intent = new Intent( MainActivity.this, InitAutoFragmentActivity.class );
						intent.putExtras( extras );
						startActivity( intent );
					}
				}
			}
		} );

		LinearLayout bt_notifyOccurrences = (LinearLayout) findViewById( R.id.bt_notifyOccurrences );
		bt_notifyOccurrences.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				Intent intent = new Intent( MainActivity.this, OccurrenceActivity.class );
				startActivity( intent );
			}
		} );

		LinearLayout bt_consults = (LinearLayout) findViewById( R.id.bt_consult );
		bt_consults.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				// Intent intent = new Intent( MainActivity.this,
				// ConsultActivity.class );
				// startActivity( intent );
				startActivity( new Intent( MainActivity.this, ConsultAutoActivity.class ) );
			}
		} );

		checkAutoInfractionPendent();
	}

	@Override
	public boolean onCreateOptionsMenu( Menu menu ) {
		getMenuInflater().inflate( R.menu.menu_main, menu );
		return true;
	}

	@Override
	public boolean onOptionsItemSelected( MenuItem item ) {
		if ( item.getItemId() == R.id.action_camera ) {
			takePhoto();
			return true;
		} else
			switch ( item.getItemId() ) {
				case R.id.action_getPackageNumber:
					if ( !haveAvailableNumbers() ) {
						if ( !haveAutosToSend() ) {
							loadPackageNumberTask();
						} else {
							String message = "É necessário enviar os autos antes de obter um novo talonário de auto. Deseja enviar agora?";
							dialogSync( message );
						}
					}
					return true;
				case R.id.action_syncAutos:
					synchronizeAutos();
					return true;
				case R.id.action_updateData:
					dataLoadTask();
					return true;
				case R.id.action_leave:
					if ( !haveAutosToSend() ) {
						logoutTask();
					} else {
						String message = null;
						if ( qntAutos == 1 )
							message = "Ainda há 1 auto de infração para ser enviado. Deseja enviar agora?";
						else
							message = "Ainda há " + qntAutos + " autos de infração para serem enviados. Deseja enviar agora?";
						dialogSync( message );
					}
					return true;
				default:
					return super.onOptionsItemSelected( item );
			}

	}

	@Override
	protected void onResume() {
		if ( organSelected == null )
			packageNumbers = PackageNumberRepository.getPackageByAgentAndOrgan( agent.getUsername(), agent.getOrgans().get( 0 ).getCodeOrgan() );
		else
			packageNumbers = PackageNumberRepository.getPackageByAgentAndOrgan( agent.getUsername(), organSelected.getCodeOrgan() );
		if ( packageNumbers != null )
			countQntAutos();
		if ( setting != null ) {
			isSendServiceRunning();
			isRemoveServiceRunning();
		}
		super.onResume();
	}

	@Override
	public boolean onKeyDown( int keyCode, KeyEvent event ) {
		return false;
	}

	private boolean synchronizeAutos() {
		List<AutoInfraction> autoInfractions = AutoInfractionRepository.getAllNotSent( agent.getUsername() );
		System.out.println( "QNT AUTOS: " + autoInfractions.size() );
		if ( !autoInfractions.isEmpty() )
			return syncAutosTask( autoInfractions );
		else
			Toast.makeText( MainActivity.this, "Autos enviados com sucesso", Toast.LENGTH_LONG ).show();
		return true;
	}

	// if ( ConnectivityUtil.isConnectedWifi( this ) ) {
	// BaseRepository.deleteAll( AutoInfraction.class );
	// return true;
	// } else {
	// Toast toast = Toast.makeText( this, "Conexão com Wi-Fi não encontrada.",
	// Toast.LENGTH_LONG );
	// toast.setGravity( Gravity.CENTER, 0, 0 );
	// toast.show();
	// return false;
	// }

	private boolean haveAvailableNumbersTask() {
		if ( packageNumbers == null || ( packageNumbers.getEndNumber().equals( packageNumbers.getCurrentNumber() ) ) ) {
			if ( packageNumbers != null )
				packageNumbers.delete();
			return false;
		} else {
			return true;
		}
	}

	private boolean haveAvailableNumbers() {
		if ( packageNumbers == null || ( packageNumbers.getEndNumber().equals( packageNumbers.getCurrentNumber() ) ) ) {
			if ( packageNumbers != null )
				packageNumbers.delete();
			return false;
		} else {
			String message = null;
			countQntAutos();
			if ( qntAutoNumbers == 1 )
				message = "Ainda há " + qntAutoNumbers + " auto de infração disponível para utilização.";
			else
				message = "Ainda há " + qntAutoNumbers + " autos de infração disponíveis para utilização.";
			Toast toast = Toast.makeText( this, message, Toast.LENGTH_LONG );
			toast.setGravity( Gravity.CENTER, 0, 0 );
			toast.show();
			return true;
		}
	}

	@SuppressLint( "NewApi" )
	private void countQntAutos() {
		minAutosWarn = SettingParameterRepository.getMinAutosWarn();
		if ( packageNumbers.getCurrentNumber() == 0 ) {
			qntAutoNumbers = packageNumbers.getEndNumber() - packageNumbers.getInitNumber() + 1;
			tv_autoCount.setText( String.format( "%02d", qntAutoNumbers ) );
		} else {
			qntAutoNumbers = packageNumbers.getEndNumber() - packageNumbers.getCurrentNumber();
			tv_autoCount.setText( String.format( "%02d", qntAutoNumbers ) );
		}

		if ( qntAutoNumbers == null || qntAutoNumbers == 0 )
			tv_autoCount.setText( "0" );

		if ( minAutosWarn != null && qntAutoNumbers <= minAutosWarn )
			ll_autoCount.setBackground( getResources().getDrawable( R.drawable.autocount_main_background_warn ) );
		// tv_autoCount.setTextColor( getResources().getColor(
		// android.R.color.holo_red_light ) );
		else
			ll_autoCount.setBackground( getResources().getDrawable( R.drawable.autocount_main_background ) );
		// tv_autoCount.setTextColor( getResources().getColor(
		// android.R.color.white ) );
	}

	private void dialogSync( String message ) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( this );

		alertDialogBuilder.setTitle( "Enviar Autos" );
		alertDialogBuilder.setMessage( message ).setCancelable( false );

		alertDialogBuilder.setPositiveButton( "Sim", new DialogInterface.OnClickListener() {
			@Override
			public void onClick( DialogInterface dialog, int id ) {
				synchronizeAutos();
			}
		} ).setNegativeButton( "Não", new DialogInterface.OnClickListener() {
			@Override
			public void onClick( DialogInterface dialog, int id ) {
				dialog.cancel();
			}
		} );

		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}

	private void cancelSynchronizeAutosService() {
		Intent intent = new Intent( getResources().getString( R.string.SYNC_RECEIVER ) );
		PendingIntent pi = PendingIntent.getBroadcast( this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT );

		AlarmManager alarmManager = (AlarmManager) getSystemService( Context.ALARM_SERVICE );
		alarmManager.cancel( pi );
		pi.cancel();
	}

	private void cancelRemoveAutosService() {
		Intent intent = new Intent( getResources().getString( R.string.REMOVE_RECEIVER ) );
		PendingIntent pi = PendingIntent.getBroadcast( this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT );

		AlarmManager alarmManager = (AlarmManager) getSystemService( Context.ALARM_SERVICE );
		alarmManager.cancel( pi );
		pi.cancel();
	}

	private void checkAutoInfractionPendent() {
		final AutoInfraction autoRecovered = (AutoInfraction) AutoInfractionRepository.getLast( AutoInfraction.class );
		if ( autoRecovered != null && !autoRecovered.getCompleted() ) {
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( this );
			alertDialogBuilder.setTitle( "Auto recuperado" );

			alertDialogBuilder.setMessage( "Há um auto de infração pendente para ser concluído." ).setCancelable( false );

			alertDialogBuilder.setPositiveButton( "CONTINUAR...", new DialogInterface.OnClickListener() {
				@Override
				public void onClick( DialogInterface dialog, int id ) {
					Bundle extras = new Bundle();
					Organ organ = OrganRepository.getOrganByDescription( autoRecovered.getOrgan() );
					extras.putLong( "idOrganSelected", organ.getId() );
					extras.putLong( "idAutoRecovered", autoRecovered.getId() );
					Intent intent = new Intent( MainActivity.this, InitAutoFragmentActivity.class );
					intent.putExtras( extras );
					startActivity( intent );
				}
			} );

			AlertDialog alertDialog = alertDialogBuilder.create();

			alertDialog.show();
		}
	}

	private void dataLoadTask() {
		new AsyncTask<Void, Void, String>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				DialogUtil.getInstance( MainActivity.this, "Carregando dados..." );
				DialogUtil.showProgressDialog();
			}

			@Override
			protected String doInBackground( Void... params ) {
				RequestFuture<JSONObject> future = RequestFuture.newFuture();

				JsonObjectRequest jsonObjReq = new JsonObjectRequest( Method.GET, Urls.DATA_LOAD, null, future, future ) {
					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						Map<String, String> params = new HashMap<String, String>();
						String token = agent.getToken();
						params.put( "Token", token );
						return params;
					}
				};

				jsonObjReq.setRetryPolicy( new DefaultRetryPolicy( 25 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

				// Adding request to request queue
				AppController.getInstance().addToRequestQueue( jsonObjReq, TAG );

				try {
					JSONObject response = future.get();
					if ( response.getInt( "code" ) == ( StatusCode.OK.value() ) ) {
						JSONArray json = response.getJSONArray( "object" );
						Gson gson = new GsonBuilder().registerTypeAdapter( Boolean.class, new DataJsonDeserializer() ).create();
						Boolean loadOk = gson.fromJson( json.toString(), Boolean.class );
						if ( !loadOk )
							return "Erro no banco de dados";

						// responseSuccess( response );
					}
				} catch ( InterruptedException e ) {
				} catch ( ExecutionException e ) {
					String message = VolleyErrorHelper.getMessage( e.getCause(), MainActivity.this );
					return message;
				} catch ( JSONException e ) {
					e.printStackTrace();
				}

				// TalonarioDeAutos
				Log.d( "TEST", "Inicia numbersTask..." );
				if ( !haveAvailableNumbersTask() ) {
					Log.d( "TEST", "Obtendo talonario..." );
					RequestFuture<JSONObject> futurePackageNumber = RequestFuture.newFuture();
					String URL = Urls.PACKAGE_LOAD + "?username=" + agent.getUsername() + "&organ=" + organSelected.getOrgan() + "&codeOrgan=" + organSelected.getCodeOrgan();
					JsonObjectRequest jsonObjReqTalonario = new JsonObjectRequest( Method.GET, URL, null, futurePackageNumber, futurePackageNumber ) {
						@Override
						public Map<String, String> getHeaders() throws AuthFailureError {
							Map<String, String> params = new HashMap<String, String>();
							String token = agent.getToken();
							params.put( "Token", token );
							return params;
						}
					};

					jsonObjReqTalonario.setRetryPolicy( new DefaultRetryPolicy( 25 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

					// Adding request to request queue
					AppController.getInstance().addToRequestQueue( jsonObjReqTalonario, TAG );

					try {
						JSONObject response = futurePackageNumber.get();
						if ( response.getInt( "code" ) == ( StatusCode.OK.value() ) ) {
							JSONObject json = response.getJSONObject( "object" );
							packageNumbers = new PackageNumbers( new Date( json.getInt( "requestDate" ) ), json.getInt( "initNumber" ), json.getInt( "endNumber" ), json.getInt( "currentNumber" ) );
							packageNumbers.setAgentUsername( agent.getUsername() );
							packageNumbers.setCodeOrgan( organSelected.getCodeOrgan() );
							packageNumbers.save();
						}
					} catch ( InterruptedException e ) {
					} catch ( ExecutionException e ) {
						String message = VolleyErrorHelper.getMessage( e.getCause(), MainActivity.this );
						return message;
					} catch ( JSONException e ) {
						e.printStackTrace();
					}
				}

				return null;
			}

			@Override
			protected void onPostExecute( String result ) {
				super.onPostExecute( result );
				DialogUtil.hideProgressDialog();
				if ( result != null ) {
					Toast.makeText( MainActivity.this, result, Toast.LENGTH_LONG ).show();
				} else {
					Toast.makeText( MainActivity.this, "Dados carregados com sucesso", Toast.LENGTH_LONG ).show();
					countQntAutos();
					setting = (SettingParameter) BaseRepository.getLast( SettingParameter.class );
				}
			}

		}.execute();

	}

	private boolean syncAutosTask( final List<AutoInfraction> autoInfractions ) {
		qntAutos = autoInfractions.size();
		qntAutosSaved = 0;
		syncOk = false;
		DialogUtil.getInstance( MainActivity.this, "Enviando autos..." );
		final ProgressDialog pDialog = DialogUtil.getpDialog();

		new AsyncTask<Void, Void, String>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				pDialog.setProgress( 0 );
				pDialog.setMax( autoInfractions.size() );
				pDialog.setProgressStyle( ProgressDialog.STYLE_HORIZONTAL );
				DialogUtil.showProgressDialog();
			}

			@Override
			protected String doInBackground( Void... params ) {

				for ( AutoInfraction auto : autoInfractions ) {
					RequestFuture<JSONObject> future = RequestFuture.newFuture();

					Gson gson = new GsonBuilder().registerTypeAdapter( AutoInfraction.class, new AutoInfractionJsonSerializer() ).create();
					String jsonAuto = gson.toJson( auto );

					JSONObject jsonObject = null;
					try {
						jsonObject = new JSONObject( jsonAuto );
					} catch ( JSONException e1 ) {
						e1.printStackTrace();
					}

					JsonObjectRequest jsonObjReq = new JsonObjectRequest( Method.POST, Urls.SEND_AUTOS, jsonObject, future, future ) {
						@Override
						public Map<String, String> getHeaders() throws AuthFailureError {
							Map<String, String> params = new HashMap<String, String>();
							String token = agent.getToken();
							params.put( "Token", token );
							return params;
						}
					};

					jsonObjReq.setRetryPolicy( new DefaultRetryPolicy( 25 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

					// Adding request to request queue
					AppController.getInstance().addToRequestQueue( jsonObjReq, TAG );

					try {
						JSONObject response = future.get();
						if ( response.getInt( "code" ) == ( StatusCode.CREATED.value() ) ) {
							Log.d( TAG, "Saved: " + response.toString() );
							auto.setSent( true );
							auto.save();
							pDialog.incrementProgressBy( 1 );
							qntAutosSaved++;
						}
					} catch ( InterruptedException e ) {
					} catch ( ExecutionException e ) {
						String message = VolleyErrorHelper.getMessage( e.getCause(), MainActivity.this );
						return message;
					} catch ( JSONException e ) {
						e.printStackTrace();
					}
				}

				return null;
			}

			@Override
			protected void onPostExecute( String result ) {
				super.onPostExecute( result );
				DialogUtil.hideProgressDialog();
				if ( result != null ) {
					Toast.makeText( MainActivity.this, result, Toast.LENGTH_LONG ).show();
				} else {
					if ( qntAutos == qntAutosSaved ) {
						Toast.makeText( MainActivity.this, "Autos enviados com sucesso", Toast.LENGTH_LONG ).show();
						syncOk = true;
					} else {
						int qntAutosLeft = qntAutos - qntAutosSaved;
						if ( qntAutosLeft == 1 )
							Toast.makeText( MainActivity.this, qntAutosLeft + " auto não foi enviado. Verifique a internet e tente novamente.", Toast.LENGTH_LONG ).show();
						else
							Toast.makeText( MainActivity.this, qntAutosLeft + " autos não foram enviados. Verifique a internet e tente novamente.", Toast.LENGTH_LONG ).show();
					}
				}
			}

		}.execute();

		return syncOk;
	}

	private void loadPackageNumberTask() {
		new AsyncTask<Void, Void, String>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				DialogUtil.getInstance( MainActivity.this, "Obtendo talonário..." );
				DialogUtil.showProgressDialog();
			}

			@Override
			protected String doInBackground( Void... params ) {
				RequestFuture<JSONObject> future = RequestFuture.newFuture();
				String URL = Urls.PACKAGE_LOAD + "?username=" + agent.getUsername() + "&organ=" + organSelected.getOrgan() + "&codeOrgan=" + organSelected.getCodeOrgan();
				JsonObjectRequest jsonObjReq = new JsonObjectRequest( Method.GET, URL, null, future, future ) {
					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						Map<String, String> params = new HashMap<String, String>();
						String token = agent.getToken();
						params.put( "Token", token );
						return params;
					}
				};

				jsonObjReq.setRetryPolicy( new DefaultRetryPolicy( 25 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

				// Adding request to request queue
				AppController.getInstance().addToRequestQueue( jsonObjReq, TAG );

				try {
					JSONObject response = future.get();
					if ( response.getInt( "code" ) == ( StatusCode.OK.value() ) ) {
						JSONObject json = response.getJSONObject( "object" );
						packageNumbers = new PackageNumbers( new Date( json.getInt( "requestDate" ) ), json.getInt( "initNumber" ), json.getInt( "endNumber" ), json.getInt( "currentNumber" ) );
						packageNumbers.setAgentUsername( agent.getUsername() );
						packageNumbers.setCodeOrgan( organSelected.getCodeOrgan() );
						packageNumbers.save();
					}
				} catch ( InterruptedException e ) {
				} catch ( ExecutionException e ) {
					String message = VolleyErrorHelper.getMessage( e.getCause(), MainActivity.this );
					return message;
				} catch ( JSONException e ) {
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute( String result ) {
				super.onPostExecute( result );
				DialogUtil.hideProgressDialog();
				if ( result != null ) {
					Toast.makeText( MainActivity.this, result, Toast.LENGTH_LONG ).show();
				} else {
					Toast.makeText( MainActivity.this, "Talonário obtido com sucesso", Toast.LENGTH_LONG ).show();
					countQntAutos();
				}
			}

		}.execute();
	}

	private void logoutTask() {
		Map<String, String> jsonParams = new HashMap<String, String>();
		jsonParams.put( "username", agent.getUsername() );
		jsonParams.put( "imei", imeiDevice );

		DialogUtil.getInstance( MainActivity.this, "Saindo..." );
		DialogUtil.showProgressDialog();
		JsonObjectRequest jsonObjReq = new JsonObjectRequest( Method.POST, Urls.USER_LOGOUT, new JSONObject( jsonParams ),

		new Response.Listener<JSONObject>() {
			@Override
			public void onResponse( JSONObject response ) {
				try {
					DialogUtil.hideProgressDialog();
					if ( response.getInt( "code" ) == ( StatusCode.OK.value() ) ) {
						agent.delete();
						cancelSynchronizeAutosService();
						cancelRemoveAutosService();
						BaseRepository.deleteAll( PackageNumbers.class );
						startActivity( new Intent( MainActivity.this, LoginActivity.class ) );
						finish();
					}
				} catch ( JSONException e ) {
					e.printStackTrace();
				}
			}

		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse( VolleyError error ) {
				DialogUtil.hideProgressDialog();
				String message = VolleyErrorHelper.getMessage( error, MainActivity.this );
				Toast.makeText( MainActivity.this, message, Toast.LENGTH_LONG ).show();
			}

		} ) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				String token = agent.getToken();
				params.put( "Token", token );
				return params;
			}
		};

		jsonObjReq.setRetryPolicy( new DefaultRetryPolicy( 25 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue( jsonObjReq, TAG );
	}

	private boolean checkData() {

		if ( BaseRepository.getAll( Patio.class ).isEmpty() || BaseRepository.getAll( Brand.class ).isEmpty() || BaseRepository.getAll( Specie.class ).isEmpty() || BaseRepository.getAll( Reason.class ).isEmpty() || BaseRepository.getAll( SettingParameter.class ).isEmpty() || BaseRepository.getAll( Infraction.class ).isEmpty() ) {

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( this );
			alertDialogBuilder.setTitle( "Base de dados vazia" );

			alertDialogBuilder.setMessage( "É necessário carregar os dados antes de iniciar um auto." ).setCancelable( false );

			alertDialogBuilder.setPositiveButton( "CARREGAR DADOS", new DialogInterface.OnClickListener() {
				@Override
				public void onClick( DialogInterface dialog, int id ) {
					dataLoadTask();
				}
			} );

			AlertDialog alertDialog = alertDialogBuilder.create();

			alertDialog.show();
			return false;
		}
		return true;
	}

	private boolean isSendServiceRunning() {
		Intent intentService = new Intent( getResources().getString( R.string.SYNC_RECEIVER ) );

		PendingIntent pi = PendingIntent.getBroadcast( this, 0, intentService, PendingIntent.FLAG_NO_CREATE );

		if ( pi != null ) {
			Log.d( "TEST", "Service is already active" );
		} else {
			Log.d( "TEST", "Service Not Running..." );
			Intent intent = new Intent( getResources().getString( R.string.SYNC_AUTOS ) );
			startService( intent );
		}
		return false;
	}

	private void isRemoveServiceRunning() {
		if ( !BaseRepository.getAll( SettingParameter.class ).isEmpty() ) {
			Intent intentService = new Intent( getResources().getString( R.string.REMOVE_RECEIVER ) );

			PendingIntent pi = PendingIntent.getBroadcast( this, 0, intentService, PendingIntent.FLAG_NO_CREATE );

			if ( pi != null ) {
				Log.d( "TEST", "RemoveService is already active" );
			} else {
				Log.d( "TEST", "RemoveService Not Running..." );
				Intent intent = new Intent( getResources().getString( R.string.REMOVE_AUTOS ) );
				startService( intent );

			}
		}
	}

	private boolean haveAutosToSend() {
		List<AutoInfraction> autoInfractions = AutoInfractionRepository.getAllNotSent( agent.getUsername() );
		qntAutos = autoInfractions.size();
		if ( autoInfractions.isEmpty() )
			return false;
		else
			return true;
	}

	private void takePhoto() {

		Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
		File dir = new File( Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES ), "Talonário" );
		if ( !dir.exists() ) {
			dir.mkdirs();
		}

		LocationTrackerService location = new LocationTrackerService( this );
		Double latitude = null;
		Double longitude = null;
		if ( location.canGetLocation() ) {
			latitude = location.getLatitude();
			longitude = location.getLongitude();
		}
		location.stopUsingLocation();

		dateImagePicked = new Date();
		String creationDate = new SimpleDateFormat( "yyyyMMdd_HHmmss" ).format( dateImagePicked );
		String path = null;
		if ( latitude != null && longitude != null ) {
			path = dir.getPath() + "/" + creationDate + "#" + Math.abs( latitude ) + "_" + Math.abs( longitude ) + ".jpg";
		} else
			path = dir.getPath() + "/" + creationDate + ".jpg";

		File file = new File( path );
		mImageCaptureUri = Uri.fromFile( file );
		try {
			intent.putExtra( MediaStore.EXTRA_OUTPUT, mImageCaptureUri );
			intent.putExtra( "return-data", true );

			startActivityForResult( intent, PICK_FROM_CAMERA );
		} catch ( Exception e ) {
			e.printStackTrace();
		}

	}

	@Override
	public void onActivityResult( int requestCode, int resultCode, Intent data ) {
		if ( resultCode != Activity.RESULT_OK )
			return;

		Bitmap bitmap = null;
		String path = "";
		Uri fileUri = null;

		path = mImageCaptureUri.getPath();
		bitmap = BitmapFactory.decodeFile( path );

		fileUri = Uri.fromFile( new File( path ) );
		bitmap = adjustPhoto( fileUri, path, false );

		// AddMedia
		updateGallery( path );

		takePhoto();
	}

	private void updateGallery( String path ) {
		getApplicationContext().sendBroadcast( new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile( new File( path ) ) ) );
	}

	private Bitmap adjustPhoto( Uri fileUri, String path, boolean bitmapGallery ) {

		getApplicationContext().getContentResolver().notifyChange( fileUri, null );
		ContentResolver cr = getApplicationContext().getContentResolver();

		Bitmap bitmap = null;
		int w = 0;
		int h = 0;

		float angle = 0;
		try {

			// bitmap = android.provider.MediaStore.Images.Media.getBitmap( cr,
			// fileUri );
			bitmap = readBitmap( fileUri );
			// captura as dimens�es da imagem

			// pega o caminho onda a imagem est� salva
			ExifInterface exif = new ExifInterface( path );
			// pega a orienta��o real da imagem

			int orientation = exif.getAttributeInt( ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL );
			// gira a imagem de acordo com a orienta��o

			switch ( orientation ) {

				case 3: // ORIENTATION_ROTATE_180
					angle = 180;
					break;
				case 6: // ORIENTATION_ROTATE_90
					angle = 90;
					break;
				case 8: // ORIENTATION_ROTATE_270
					angle = 270;
					break;
				default: // ORIENTATION_ROTATE_0
					angle = 0;
					break;
			}

		} catch ( FileNotFoundException e1 ) {
			e1.printStackTrace();
		} catch ( IOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		FileOutputStream out;
		Bitmap rotatedBitmap = null;
		Bitmap mutableBitmap = null;
		try {
			out = new FileOutputStream( path );
			// define um indice = 1 pois se der erro vai manter a imagem como
			// est�.
			// Integer idx = 1;
			// reupera as dimens�es da imagem
			w = bitmap.getWidth();
			h = bitmap.getHeight();
			// verifica qual a maior dimens�o e divide pela lateral final para
			// definir qual o indice de redu��o

			if ( w > h ) {
				if ( w > 800 ) {
					w = 800;
				}

				if ( h > 600 ) {
					h = 600;
				}

			} else {
				if ( h > 800 ) {
					h = 800;
				}

				if ( w > 600 ) {
					w = 600;
				}
			}

			// scale it to fit the screen, x and y swapped because my image is
			// wider than it is tall
			Bitmap scaledBitmap = Bitmap.createScaledBitmap( bitmap, w, h, true );

			// create a matrix object
			Matrix matrix = new Matrix();
			matrix.postRotate( angle ); // rotate by angle

			// create a new bitmap from the original using the matrix to
			// transform the result
			rotatedBitmap = Bitmap.createBitmap( scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true );

			if ( !bitmapGallery ) {
				SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy HH:mm" );
				String dateTime = sdf.format( dateImagePicked );
				mutableBitmap = rotatedBitmap.copy( Bitmap.Config.ARGB_8888, true );
				Canvas cs = new Canvas( mutableBitmap );
				Paint tPaint = new Paint();
				tPaint.setTextSize( 20 );
				tPaint.setColor( Color.YELLOW );
				tPaint.setStyle( Style.FILL );
				float height = tPaint.measureText( "yY" );
				cs.drawText( dateTime, 20f, height + 15f, tPaint );
				cs.drawBitmap( mutableBitmap, 0, 0, tPaint );

				// salva a imagem reduzida no disco
				mutableBitmap.compress( Bitmap.CompressFormat.PNG, 100, out );
				return mutableBitmap;
			}
			rotatedBitmap.compress( Bitmap.CompressFormat.PNG, 100, out );

		} catch ( FileNotFoundException e ) {
			e.printStackTrace();
		}

		clearBitmap( bitmap );
		// uma nova instancia do bitmap rotacionado
		return rotatedBitmap;
	}

	private Bitmap readBitmap( Uri selectedImage ) {
		Bitmap bm = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 4;
		AssetFileDescriptor fileDescriptor = null;
		try {
			fileDescriptor = this.getContentResolver().openAssetFileDescriptor( selectedImage, "r" );
		} catch ( FileNotFoundException e ) {
			e.printStackTrace();
		} finally {
			try {
				bm = BitmapFactory.decodeFileDescriptor( fileDescriptor.getFileDescriptor(), null, options );
				fileDescriptor.close();
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
		return bm;
	}

	public static void clearBitmap( Bitmap bm ) {
		bm.recycle();
		System.gc();
	}

}
