package br.com.opala.talonario.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.Agent;
import br.com.opala.talonario.entities.Organ;
import br.com.opala.talonario.entities.PackageNumbers;
import br.com.opala.talonario.entities.gson.AgentJsonDeserializer;
import br.com.opala.talonario.enums.StatusCode;
import br.com.opala.talonario.enums.Urls;
import br.com.opala.talonario.repositories.AgentRepository;
import br.com.opala.talonario.services.AppController;
import br.com.opala.talonario.utils.BCrypt;
import br.com.opala.talonario.utils.DialogUtil;
import br.com.opala.talonario.utils.ViewUtil;
import br.com.opala.talonario.utils.VolleyErrorHelper;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class LoginActivity extends Activity {

	private EditText et_username;
	private EditText et_password;
	private RelativeLayout bt_login;
	private TextView tv_error_login;
	private Agent currentAgent;

	private final String TAG = LoginActivity.class.getSimpleName();
	private String imeiDevice;

	public void init() {
		et_username = (EditText) findViewById( R.id.et_username );
		et_password = (EditText) findViewById( R.id.et_password );
		bt_login = (RelativeLayout) findViewById( R.id.bt_login );
		tv_error_login = (TextView) findViewById( R.id.tv_error_login );

		ViewUtil.checkErrorEditText( et_username );
		ViewUtil.checkErrorEditText( et_password );
	}

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		this.requestWindowFeature( Window.FEATURE_NO_TITLE );
		getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN );
		setContentView( R.layout.activity_login );
		init();

		TelephonyManager telephonyManager = null;
		telephonyManager = (TelephonyManager) getSystemService( TELEPHONY_SERVICE );
		imeiDevice = telephonyManager.getDeviceId();

		// et_password.setOnEditorActionListener( new OnEditorActionListener() {
		// @Override
		// public boolean onEditorAction( TextView v, int actionId, KeyEvent
		// event ) {
		// if ( ( event != null && ( event.getKeyCode() ==
		// KeyEvent.KEYCODE_ENTER ) ) || ( actionId ==
		// EditorInfo.IME_ACTION_DONE ) ) {
		// if ( validateLogin() ) {
		// login();
		// }
		// }
		// return false;
		// }
		// } );

		bt_login.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				ViewUtil.hideKeyboard( LoginActivity.this, v );
				if ( validateLogin() ) {
					login();
				}
			}
		} );

		// backup();

	}

	private boolean validateLogin() {
		if ( et_username.getText().toString().equals( "" ) ) {
			et_username.requestFocus();
			et_username.setError( getResources().getString( R.string.error_required ) );
			return false;
		} else if ( et_username.getText().toString().length() < 5 ) {
			et_username.requestFocus();
			et_username.setError( getResources().getString( R.string.error_username_length ) );
			return false;
		}

		if ( et_password.getText().toString().equals( "" ) ) {
			et_password.requestFocus();
			et_password.setError( getResources().getString( R.string.error_required ) );
			return false;
		} else if ( et_password.getText().toString().length() < 6 ) {
			et_password.requestFocus();
			et_password.setError( getResources().getString( R.string.error_password_length ) );
			return false;
		}

		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	private void backup() {
		try {
			File sdcard = Environment.getExternalStorageDirectory();
			File outputFile = new File( sdcard, "Talonario.db" );

			if ( !outputFile.exists() )
				outputFile.createNewFile();

			File data = Environment.getDataDirectory();
			File inputFile = new File( data, "data/br.com.opala.talonario/databases/Talonario.db" );
			InputStream input = new FileInputStream( inputFile );
			OutputStream output = new FileOutputStream( outputFile );
			byte[] buffer = new byte[1024];
			int length;
			while ( ( length = input.read( buffer ) ) > 0 ) {
				output.write( buffer, 0, length );
			}
			output.flush();
			output.close();
			input.close();
		} catch ( IOException e ) {
			e.printStackTrace();
			throw new Error( "Copying Failed" );
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private void loginTask() {
		Map<String, String> jsonParams = new HashMap<String, String>();
		jsonParams.put( "username", et_username.getText().toString() );
		jsonParams.put( "password", et_password.getText().toString() );
		jsonParams.put( "imei", imeiDevice );

		// DialogUtil.showProgressDialog();
		JsonObjectRequest jsonObjReq = new JsonObjectRequest( Method.POST, Urls.USER_LOGIN, new JSONObject( jsonParams ),

		new Response.Listener<JSONObject>() {
			@Override
			public void onResponse( JSONObject response ) {
				try {
					if ( response.getInt( "code" ) == ( StatusCode.OK.value() ) ) {
						responseSuccess( response );
					} else {
						DialogUtil.hideProgressDialog();
						et_password.setText( null );
						tv_error_login.setText( response.getString( "message" ) );
					}
				} catch ( JSONException e ) {
					e.printStackTrace();
				}
			}

		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse( VolleyError error ) {
				DialogUtil.hideProgressDialog();
				String message = VolleyErrorHelper.getMessage( error, LoginActivity.this );
				Toast.makeText( LoginActivity.this, message, Toast.LENGTH_LONG ).show();
			}

		} ) {

		};

		jsonObjReq.setRetryPolicy( new DefaultRetryPolicy( 25 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue( jsonObjReq, TAG );
	}

	protected void responseSuccess( JSONObject response ) throws JSONException {
		JSONObject json = response.getJSONObject( "object" );
		Gson gson = new GsonBuilder().registerTypeAdapter( Agent.class, new AgentJsonDeserializer() ).create();
		Agent agent = gson.fromJson( json.toString(), Agent.class );

		loadPackageNumberTask( agent );
	}

	private void login() {

		DialogUtil.getInstance( LoginActivity.this, "Acessando..." );

		new AsyncTask<Void, Void, String>() {

			String message = null;

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				DialogUtil.showProgressDialog();
			}

			@Override
			protected String doInBackground( Void... params ) {
				currentAgent = AgentRepository.getCurrentAgent();
				if ( currentAgent != null ) {
					Agent loginAgent = AgentRepository.getAgentByUsername( et_username.getText().toString() );
					if ( loginAgent == null || !currentAgent.getUsername().equalsIgnoreCase( loginAgent.getUsername() ) ) {
						message = "Usuário ou senha não conferem com o usuário logado";
					} else {
						if ( BCrypt.checkpw( et_password.getText().toString(), currentAgent.getPassword() ) ) {
							Bundle extras = new Bundle();
							extras.putLong( "idAgent", loginAgent.getId() );
							Intent intent = new Intent( getApplicationContext(), MainActivity.class );
							intent.putExtras( extras );
							startActivity( intent );
							finish();
							DialogUtil.hideProgressDialog();
							message = "OK";
						} else {
							message = "Usuário ou senha não conferem com o usuário logado";
						}
					}
				}
				return message;
			}

			@Override
			protected void onPostExecute( String result ) {
				super.onPostExecute( result );
				if ( result == null ) {
					loginTask();
				} else if ( !result.equals( "OK" ) ) {
					DialogUtil.hideProgressDialog();
					et_password.setText( null );
					tv_error_login.setText( result );
				}
			}

		}.execute();

	}

	private void loadPackageNumberTask( final Agent agent ) {
		new AsyncTask<Void, Void, String>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			@Override
			protected String doInBackground( Void... params ) {

				for ( Organ organ : agent.getOrgans() ) {
					RequestFuture<JSONObject> future = RequestFuture.newFuture();
					String URL = Urls.PACKAGE_LOAD + "?username=" + agent.getUsername() + "&organ=" + organ.getOrgan() + "&codeOrgan=" + organ.getCodeOrgan();
					JsonObjectRequest jsonObjReq = new JsonObjectRequest( Method.GET, URL, null, future, future ) {
						@Override
						public Map<String, String> getHeaders() throws AuthFailureError {
							Map<String, String> params = new HashMap<String, String>();
							String token = agent.getToken();
							params.put( "Token", token );
							return params;
						}
					};

					jsonObjReq.setRetryPolicy( new DefaultRetryPolicy( 25 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) );

					// Adding request to request queue
					AppController.getInstance().addToRequestQueue( jsonObjReq, TAG );

					try {
						JSONObject response = future.get();
						if ( response.getInt( "code" ) == ( StatusCode.OK.value() ) ) {
							JSONObject json = response.getJSONObject( "object" );
							PackageNumbers packageNumbers = new PackageNumbers( new Date( json.getInt( "requestDate" ) ), json.getInt( "initNumber" ), json.getInt( "endNumber" ), json.getInt( "currentNumber" ) );
							packageNumbers.setAgentUsername( agent.getUsername() );
							packageNumbers.setCodeOrgan( organ.getCodeOrgan() );
							packageNumbers.save();
						}
					} catch ( InterruptedException e ) {
					} catch ( ExecutionException e ) {
						String message = VolleyErrorHelper.getMessage( e.getCause(), LoginActivity.this );
						return message;
					} catch ( JSONException e ) {
						e.printStackTrace();
					}
				}

				return null;
			}

			@Override
			protected void onPostExecute( String result ) {
				super.onPostExecute( result );
				DialogUtil.hideProgressDialog();
				Bundle extras = new Bundle();
				extras.putLong( "idAgent", agent.getId() );
				Intent intent = new Intent( getApplicationContext(), MainActivity.class );
				intent.putExtras( extras );
				startActivity( intent );
				finish();
			}

		}.execute();
	}
}
