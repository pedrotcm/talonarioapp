package br.com.opala.talonario.activities;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.Agent;
import br.com.opala.talonario.entities.AutoInfraction;
import br.com.opala.talonario.entities.Cancel;
import br.com.opala.talonario.entities.Organ;
import br.com.opala.talonario.entities.PackageNumbers;
import br.com.opala.talonario.entities.Reason;
import br.com.opala.talonario.fragments.InfractionFragment;
import br.com.opala.talonario.fragments.LocalFragment;
import br.com.opala.talonario.fragments.LocationFragment;
import br.com.opala.talonario.fragments.PhotoFragment;
import br.com.opala.talonario.fragments.VehicleFragment;
import br.com.opala.talonario.repositories.AgentRepository;
import br.com.opala.talonario.repositories.BaseRepository;
import br.com.opala.talonario.repositories.OrganRepository;
import br.com.opala.talonario.repositories.PackageNumberRepository;
import br.com.opala.talonario.services.LocationTrackerService;
import br.com.opala.talonario.utils.ConnectivityUtil;

import com.activeandroid.Model;

public class InitAutoFragmentActivity extends FragmentActivity {

	public static FragmentManager fm;

	private int NUM_PAGES;
	private PagerAdapter mPagerAdapter;

	public static Long idAutoRecovered;

	public static LocationTrackerService location;

	public static boolean haveImageWithDate;
	public static Organ organ;
	private static List<Reason> reasons;

	public static ViewPager mPager;
	private static InitAutoFragmentActivity context;
	public static AutoInfraction autoInfraction;
	private static AlertDialog dialogCancel;
	private static AlertDialog dialogSave;
	private static Cancel cancel;
	private static Agent agent;
	private static PackageNumbers packageNumbers;

	public static boolean wasModified;
	private static int currentNumber;
	private static String numberAutoInfraction;

	private static TextView tv_reason;
	private static EditText et_justification;

	public void init() {
		Long idOrganSelected = getIntent().getExtras().getLong( "idOrganSelected" );
		idAutoRecovered = getIntent().getExtras().getLong( "idAutoRecovered" );
		wasModified = false;
		haveImageWithDate = false;
		if ( idAutoRecovered == null || idAutoRecovered == 0 ) {
			autoInfraction = new AutoInfraction();

			agent = AgentRepository.getCurrentAgent();
			autoInfraction.setAgentUsername( agent.getUsername() );
			autoInfraction.setAgentName( agent.getName() );

			autoInfraction.setCreatedAt( new Date() );

			organ = Model.load( Organ.class, idOrganSelected );
			// autoInfraction.setIdOrgan( idOrganSelected );
			autoInfraction.setOrgan( organ.getOrgan() );
			autoInfraction.setCodeOrgan( organ.getCodeOrgan() );

			autoInfraction.save();
		} else {
			autoInfraction = Model.load( AutoInfraction.class, idAutoRecovered );
			agent = AgentRepository.getAgentByUsername( autoInfraction.getAgentUsername() );
			organ = OrganRepository.getOrganByDescription( autoInfraction.getOrgan() );
			wasModified = true;
		}

		cancel = new Cancel();

		context = this;

		reasons = (List<Reason>) BaseRepository.getAll( Reason.class );
		reasons.add( 0, new Reason( "Selecione um motivo" ) );
		packageNumbers = PackageNumberRepository.getPackageByAgentAndOrgan( agent.getUsername(), organ.getCodeOrgan() );

		getActionBar().setIcon( getResources().getDrawable( R.drawable.ic_bar_app ) );

	}

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.fragment_activity_screen_slide );
		init();

		fm = getSupportFragmentManager();

		mPagerAdapter = new ScreenSlidePagerAdapter( fm );
		mPager = (ViewPager) findViewById( R.id.pager );

		if ( ConnectivityUtil.isConnected( context ) ) { // Connected
			NUM_PAGES = 4;
			mPager.setOffscreenPageLimit( NUM_PAGES );
			mPager.setAdapter( mPagerAdapter );
			location = new LocationTrackerService( this );
			if ( location.canGetLocation() ) {
				autoInfraction.setLatitude( location.getLatitude() );
				autoInfraction.setLongitude( location.getLongitude() );
			}
			location.stopUsingLocation();

		} else { // Disconnected
			NUM_PAGES = 5;
			mPager.setOffscreenPageLimit( NUM_PAGES );
			mPager.setAdapter( mPagerAdapter );
		}

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	public boolean onKeyDown( int keyCode, KeyEvent event ) {
		if ( wasModified == false ) {
			autoInfraction.delete();
			return super.onKeyDown( keyCode, event );
		}
		return false;
	}

	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
		public ScreenSlidePagerAdapter( FragmentManager fm ) {
			super( fm );
		}

		@Override
		public Fragment getItem( int position ) {
			switch ( position ) {
				case 0:
					return new PhotoFragment();
				case 1:
					return new VehicleFragment();
				case 2:
					return new InfractionFragment();
				case 3:
					return new LocalFragment();
				case 4:
					return new LocationFragment();
				default:
					return null;
			}
		}

		@Override
		public int getCount() {
			return NUM_PAGES;
		}

	}

	public static void dialogCancel() {
		LayoutInflater li = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		final View view = li.inflate( R.layout.dialog_cancel, null );

		et_justification = (EditText) view.findViewById( R.id.et_justification );
		customMultilineEnter( et_justification );

		final Spinner sp_reason = (Spinner) view.findViewById( R.id.sp_reason );
		sp_reason.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {
				Reason selected = (Reason) sp_reason.getAdapter().getItem( position );
				if ( position != 0 )
					cancel.setReason( selected.getReason() );

				tv_reason = (TextView) view;
			}

			@Override
			public void onNothingSelected( AdapterView<?> parent ) {

			}
		} );

		ArrayAdapter<Reason> adapterReason = new ArrayAdapter<Reason>( context, android.R.layout.simple_spinner_dropdown_item, reasons );
		sp_reason.setAdapter( adapterReason );

		AlertDialog.Builder builder = new AlertDialog.Builder( context );
		builder.setTitle( "Solicitar cancelamento" );
		builder.setView( view );

		builder.setPositiveButton( "Não", new DialogInterface.OnClickListener() {

			@Override
			public void onClick( DialogInterface dialog, int which ) {
				hideKeyboard( view );
				dialogCancel.dismiss();
			}
		} );

		builder.setNegativeButton( "Sim", new DialogInterface.OnClickListener() {
			@Override
			public void onClick( DialogInterface dialog, int which ) {}
		} );

		dialogCancel = builder.create();
		dialogCancel.show();

		dialogCancel.getButton( AlertDialog.BUTTON_NEGATIVE ).setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick( View v ) {
				if ( saveCancelValues() ) {
					generateNumberInfraction();
					packageNumbers.setCurrentNumber( currentNumber );
					packageNumbers.save();

					autoInfraction.setNumberAutoInfraction( numberAutoInfraction );
					autoInfraction.setCompleted( true );

					cancel.setJustification( et_justification.getText().toString() );
					cancel.save();

					autoInfraction.setCancel( cancel );
					autoInfraction.save();
					dialogCancel.dismiss();
					( (Activity) context ).finish();
				}
			}
		} );

	}

	public static void customMultilineEnter( EditText edit_text ) {
		edit_text.setOnKeyListener( new View.OnKeyListener() {
			@Override
			public boolean onKey( View v, int keyCode, KeyEvent event ) {
				if ( keyCode == KeyEvent.KEYCODE_ENTER ) {
					hideKeyboard( v );
					return true;
				} else
					return false;
			}
		} );
	}

	public static void dialogSaveAutoInfraction() {
		LayoutInflater li = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		View view = li.inflate( R.layout.dialog_save_auto_infraction, null );
		AlertDialog.Builder builder = new AlertDialog.Builder( context );
		builder.setTitle( "Salvar auto de infração" );
		builder.setCancelable( false );
		builder.setView( view );

		generateNumberInfraction();
		TextView tv_numberInfraction = (TextView) view.findViewById( R.id.tv_numberAutoInfraction );
		tv_numberInfraction.setText( "Auto Nº.: " + numberAutoInfraction );

		builder.setPositiveButton( "Sim", new DialogInterface.OnClickListener() {

			@Override
			public void onClick( DialogInterface dialog, int which ) {
				location.stopUsingLocation();

				packageNumbers.setCurrentNumber( currentNumber );
				packageNumbers.save();
				autoInfraction.setNumberAutoInfraction( numberAutoInfraction );
				autoInfraction.setCompleted( true );
				autoInfraction.save();

				Bundle extras = new Bundle();
				extras.putLong( "idAutoInfraction", autoInfraction.getId() );
				Intent intent = new Intent( context, PrintActivity.class );
				intent.putExtras( extras );
				( (Activity) context ).startActivity( intent );
				( (Activity) context ).finish();
			}
		} );

		builder.setNegativeButton( "Não", new DialogInterface.OnClickListener() {
			@Override
			public void onClick( DialogInterface dialog, int which ) {
				// InitAutoFragmentActivity.dialogCancel();
				dialogSave.dismiss();
			}
		} );

		dialogSave = builder.create();
		dialogSave.show();
	}

	public static void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) context.getSystemService( Context.INPUT_METHOD_SERVICE );
		imm.hideSoftInputFromWindow( mPager.getWindowToken(), 0 );
	}

	public static void hideKeyboard( View view ) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService( Context.INPUT_METHOD_SERVICE );
		imm.hideSoftInputFromWindow( view.getWindowToken(), 0 );
	}

	@Override
	protected void onResume() {
		if ( mPager.getCurrentItem() != 0 )
			getActionBar().setDisplayHomeAsUpEnabled( true );
		super.onResume();
	}

	//
	// public static void setActioBarTitle( String title ) {
	// View v = context.getActionBar().getCustomView();
	// TextView titleTxtView = (TextView) v.findViewById( R.id.title );
	// titleTxtView.setText( title );
	// }

	public static void modifiedEditText( EditText view ) {
		view.addTextChangedListener( new TextWatcher() {
			@Override
			public void onTextChanged( CharSequence s, int start, int before, int count ) {
				if ( s != null && s.length() > 0 ) {
					wasModified = true;
				}
			}

			@Override
			public void beforeTextChanged( CharSequence s, int start, int count, int after ) {}

			@Override
			public void afterTextChanged( Editable s ) {}
		} );
	}

	public static void generateNumberInfraction() {
		String organLetter = organ.getOrgan().substring( 0, 2 );

		if ( packageNumbers.getCurrentNumber() == 0 )
			currentNumber = packageNumbers.getInitNumber();
		else
			currentNumber = packageNumbers.getCurrentNumber() + 1;

		numberAutoInfraction = String.format( "%s%09d", organLetter, currentNumber );
	}

	public static void saveValues() {
		for ( int i = 1; i < 4; i++ ) {
			Fragment page = (Fragment) mPager.getAdapter().instantiateItem( mPager, i );
			if ( page instanceof VehicleFragment ) {
				( (VehicleFragment) page ).completeSaveValues();
			} else if ( page instanceof InfractionFragment ) {
				( (InfractionFragment) page ).completeSaveValues();
			} else if ( page instanceof LocalFragment ) {
				( (LocalFragment) page ).completeSaveValues();
			}
		}
	}

	public static boolean saveCancelValues() {
		if ( cancel.getReason() == null ) {
			tv_reason.setError( "" );
			Toast toast = Toast.makeText( context, context.getResources().getString( R.string.error_required ), Toast.LENGTH_LONG );
			toast.setGravity( Gravity.CENTER, 0, 0 );
			toast.show();
			return false;
		}

		if ( et_justification.getText().toString().equals( "" ) ) {
			et_justification.requestFocus();
			et_justification.setError( context.getResources().getString( R.string.error_required ) );
			return false;
		}

		return true;
	}
}
