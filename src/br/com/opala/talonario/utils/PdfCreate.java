package br.com.opala.talonario.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.net.Uri;
import android.view.Gravity;
import android.widget.Toast;
import br.com.opala.talonario.R;
import br.com.opala.talonario.entities.AutoInfraction;
import br.com.opala.talonario.entities.MeasurementType;
import br.com.opala.talonario.repositories.MeasurementTypeRepository;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

public class PdfCreate {

	private static Font size12bolditalic = new Font( Font.FontFamily.HELVETICA, 12, Font.BOLDITALIC );
	private static Font size10bold = new Font( Font.FontFamily.HELVETICA, 10, Font.BOLD );
	private static Font size10normal = new Font( Font.FontFamily.HELVETICA, 10, Font.BOLD );
	private static Font size8bold = new Font( Font.FontFamily.HELVETICA, 8, Font.BOLD );
	private static Font size8normal = new Font( Font.FontFamily.HELVETICA, 8, Font.NORMAL );
	private static LineSeparator UNDERLINE = new LineSeparator( 0.5f, 100, null, Element.ALIGN_CENTER, -3 );

	private final AutoInfraction autoInfraction;
	private final Context context;
	private File filePDF;
	private float sizeWidth;
	private Paragraph paraAutoTitle;
	private Paragraph paraVehicleTitle;
	private Paragraph paraOffenderTitle;
	private Paragraph paraLocalTitle;
	private Paragraph paraInfractionTitle;
	private Paragraph paraAgentTitle;
	private Paragraph paraDriverTitle;

	public PdfCreate( AutoInfraction autoInfraction, Context context ) {
		this.autoInfraction = autoInfraction;
		this.context = context;
	}

	public void createDocument() {
		try {

			ContextWrapper cw = new ContextWrapper( context );
			// path to /data/data/yourapp/app_data/imageDir
			File dir = cw.getDir( "pdfs", Context.MODE_PRIVATE );
			if ( !dir.exists() )
				dir.mkdirs();
			// Create imageDir
			filePDF = new File( dir, autoInfraction.getNumberAutoInfraction() + ".pdf" );

			// String path =
			// Environment.getExternalStorageDirectory().getAbsolutePath() +
			// "/talonario/pdfs";
			// File dir = new File( path );
			// if ( !dir.exists() )
			// dir.mkdirs();
			//
			// File file = new File( dir,
			// autoInfraction.getNumberAutoInfraction() + ".pdf" );
			FileOutputStream fOut = new FileOutputStream( filePDF );
			sizeWidth = 510f;
			createContentAuto();

			Rectangle pageSize = new Rectangle( 225f, sizeWidth );
			Document document = new Document( pageSize, 18f, 18f, 18f, 18f );
			PdfWriter.getInstance( document, fOut );
			document.open();
			addTitlePage( document );
			completeContentAuto( document );

			document.close();

			Uri uri = Uri.parse( "content://br.com.opala.talonario" + filePDF.getPath() );

			Intent target = new Intent( Intent.ACTION_VIEW );
			target.setDataAndType( uri, "application/pdf" );
			target.setFlags( Intent.FLAG_ACTIVITY_NO_HISTORY );

			Intent intent = Intent.createChooser( target, "Abrir pdf" );
			intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
			try {
				context.startActivity( intent );
			} catch ( ActivityNotFoundException e ) {
				Toast toast = Toast.makeText( context, "Instale algum leitor de pdf", Toast.LENGTH_LONG );
				toast.setGravity( Gravity.CENTER, 0, 0 );
				toast.show();
			}
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}

	private void addTitlePage( Document document ) throws DocumentException {
		Paragraph title = new Paragraph( "Auto de Infração e Notificação de Autuação de Trânsito", size12bolditalic );
		title.setAlignment( Element.ALIGN_CENTER );
		addEmptyLine( title, 1 );
		document.add( title );
	}

	private void createContentAuto() throws DocumentException {
		paraAutoTitle = new Paragraph( "Identificação da Autuação", size10bold );
		Paragraph paraAuto = new Paragraph();
		paraAuto.setIndentationLeft( 6f );
		createLine( paraAuto, "Número do auto: ", autoInfraction.getNumberAutoInfraction() );
		createLine( paraAuto, "Orgão autuador: ", autoInfraction.getOrgan() );
		createLine( paraAuto, "Código do orgão: ", autoInfraction.getCodeOrgan().toString() );
		paraAutoTitle.add( paraAuto );

		paraVehicleTitle = new Paragraph( "Identificação do Veículo", size10bold );
		Paragraph paraVehicle = new Paragraph();
		paraVehicle.setIndentationLeft( 6f );
		createLine( paraVehicle, "Placa: ", autoInfraction.getPlaque() );
		createLine( paraVehicle, "Renavam: ", autoInfraction.getRenavam() );
		if ( autoInfraction.getBrand().equalsIgnoreCase( context.getResources().getString( R.string.option_other ) ) ) {
			createLine( paraVehicle, "Marca: ", autoInfraction.getOtherBrand() );
		} else {
			createLine( paraVehicle, "Marca: ", autoInfraction.getBrand() );
		}
		createLine( paraVehicle, "Espécie: ", autoInfraction.getSpecie() );
		paraVehicleTitle.add( paraVehicle );

		if ( autoInfraction.getDriverName() != null || autoInfraction.getDriverCpfRg() != null ) {
			paraDriverTitle = new Paragraph( "Identificação do Condutor", size10bold );
			Paragraph paraDriver = new Paragraph();
			paraDriver.setIndentationLeft( 6f );
			addSizeWidth( 20f );
			if ( autoInfraction.getDriverName() != null ) {
				createLine( paraDriver, "Nome: ", autoInfraction.getDriverName() );
				addSizeWidth( 20f );
			}

			if ( autoInfraction.getDriverCpfRg() != null ) {
				createLine( paraDriver, "CPF/RG/Outros: ", autoInfraction.getDriverCpfRg() );
				addSizeWidth( 20f );
			}

			if ( autoInfraction.getDriverCnh() != null ) {
				createLine( paraDriver, "CNH/Permissão: ", autoInfraction.getDriverCnh() );
				addSizeWidth( 20f );
			}

			if ( autoInfraction.getStateCnh() != null ) {
				createLine( paraDriver, "UF CNH: ", autoInfraction.getStateCnh() );
				addSizeWidth( 20f );
			}
			paraDriver.add( UNDERLINE );
			paraDriver.add( Chunk.NEWLINE );
			paraDriverTitle.add( paraDriver );
		}

		if ( autoInfraction.getOffenderName() != null || autoInfraction.getOffenderCpfRg() != null ) {
			paraOffenderTitle = new Paragraph( "Identificação do Infrator", size10bold );
			Paragraph paraOffender = new Paragraph();
			paraOffender.setIndentationLeft( 6f );
			addSizeWidth( 20f );
			if ( autoInfraction.getOffenderName() != null ) {
				createLine( paraOffender, "Nome: ", autoInfraction.getOffenderName() );
				addSizeWidth( 20f );
			}

			if ( autoInfraction.getOffenderCpfRg() != null ) {
				createLine( paraOffender, "CPF/RG/Outros: ", autoInfraction.getOffenderCpfRg() );
				addSizeWidth( 20f );
			}

			paraOffenderTitle.add( paraOffender );
		}

		paraLocalTitle = new Paragraph( "Identificação do Local", size10bold );
		Paragraph paraLocal = new Paragraph();
		paraLocal.setIndentationLeft( 6f );
		SimpleDateFormat dateFormat = new SimpleDateFormat( "dd/MM/yyyy HH:mm" );
		createLine( paraLocal, "Local: ", autoInfraction.getCompleteLocal() );
		createLine( paraLocal, "Data e hora: ", dateFormat.format( autoInfraction.getCreatedAt() ) );
		createLine( paraLocal, "Código do Município: ", autoInfraction.getCodeCity().toString() );
		paraLocalTitle.add( paraLocal );

		paraInfractionTitle = new Paragraph( "Identificação da Infração", size10bold );
		Paragraph paraInfraction = new Paragraph();
		paraInfraction.setIndentationLeft( 6f );
		createLine( paraInfraction, "Código: ", autoInfraction.getCodeInfraction().toString() + autoInfraction.getDeployment().toString() );
		createLine( paraInfraction, "Descrição: ", autoInfraction.getDescription() );
		createLine( paraInfraction, "Amparo Legal: ", "CTB, Art. " + autoInfraction.getLegalSupport() );
		if ( autoInfraction.getMeasurementType() != null && autoInfraction.getMeasurementPerformed() != null ) {
			MeasurementType measurementType = MeasurementTypeRepository.getMeasurementTypeByCode( autoInfraction.getMeasurementType() );
			createLine( paraInfraction, "Tipo Medição: ", measurementType.getDescription() );
			addSizeWidth( 20f );
			createLine( paraInfraction, "Medição Realizada: ", autoInfraction.getMeasurementPerformed() );
			addSizeWidth( 20f );
		}
		createLine( paraInfraction, "Observações: ", autoInfraction.getObservations() );
		paraInfractionTitle.add( paraInfraction );

		paraAgentTitle = new Paragraph( "Identificação do Agente", size10bold );
		Paragraph paraAgent = new Paragraph();
		paraAgent.setIndentationLeft( 6f );
		createLine( paraAgent, "Matrícula: ", autoInfraction.getAgentUsername() );
		paraAgent.add( UNDERLINE );
		paraAgentTitle.add( paraAgent );

	}

	private void completeContentAuto( Document document ) throws DocumentException {
		document.add( paraAutoTitle );
		document.add( paraVehicleTitle );
		if ( paraDriverTitle != null )
			document.add( paraDriverTitle );
		if ( paraOffenderTitle != null )
			document.add( paraOffenderTitle );
		document.add( paraLocalTitle );
		document.add( paraInfractionTitle );
		document.add( paraAgentTitle );
	}

	private void addEmptyLine( Paragraph paragraph, int number ) {
		for ( int i = 0; i < number; i++ ) {
			paragraph.add( new Paragraph( " " ) );
		}
	}

	public void deletePdf() {
		if ( filePDF != null )
			filePDF.delete();
	}

	public void createLine( Paragraph para, String name, String value ) {
		if ( value != null ) {
			para.add( new Chunk( name, size8bold ) );
			para.add( new Chunk( value, size8normal ) );
			para.add( Chunk.NEWLINE );
		}
	}

	public void signature( Paragraph para ) {
		Chunk UNDERLINE = new Chunk( new LineSeparator( 0.5f, 95, null, Element.ALIGN_CENTER, -3 ) );
		para.add( new Chunk( "Assinatura: ", size8bold ) );
		para.add( UNDERLINE );
		para.add( Chunk.NEWLINE );
	}

	public void addSizeWidth( float addSize ) {
		sizeWidth = sizeWidth + addSize;
	}

}
