package br.com.opala.talonario.utils;

import android.app.ProgressDialog;
import android.content.Context;

public class DialogUtil {

	private static ProgressDialog pDialog;

	public static void getInstance( Context context, String message ) {
		pDialog = new ProgressDialog( context );
		pDialog.setMessage( message );
		pDialog.setCancelable( false );
	}

	public static void showProgressDialog() {
		if ( !pDialog.isShowing() )
			pDialog.show();
	}

	public static void hideProgressDialog() {
		if ( pDialog.isShowing() )
			pDialog.dismiss();
	}

	public static ProgressDialog getpDialog() {
		return pDialog;
	}

}
