package br.com.opala.talonario.utils;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

public class ViewUtil {

	public static void setListViewHeightBasedOnChildren( ListView listView ) {
		ListAdapter listAdapter = listView.getAdapter();
		if ( listAdapter == null ) {
			// pre-condition
			return;
		}

		int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
		for ( int i = 0; i < listAdapter.getCount(); i++ ) {
			View listItem = listAdapter.getView( i, null, listView );
			if ( listItem instanceof ViewGroup ) {
				listItem.setLayoutParams( new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT ) );
			}
			listItem.measure( 0, 0 );
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + ( listView.getDividerHeight() * ( listAdapter.getCount() - 1 ) );
		listView.setLayoutParams( params );
	}

	public static void checkErrorEditText( final EditText view ) {
		view.addTextChangedListener( new TextWatcher() {
			@Override
			public void onTextChanged( CharSequence s, int start, int before, int count ) {
				if ( s != null && s.length() > 0 && view.getError() != null ) {
					view.setError( null );
				}
			}

			@Override
			public void beforeTextChanged( CharSequence s, int start, int count, int after ) {}

			@Override
			public void afterTextChanged( Editable s ) {}
		} );
	}

	public static void hideKeyboard( Context context, View view ) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService( Context.INPUT_METHOD_SERVICE );
		imm.hideSoftInputFromWindow( view.getWindowToken(), 0 );
	}

	public static void customMultilineEnter( final Context context, EditText edit_text ) {
		edit_text.setOnKeyListener( new View.OnKeyListener() {
			@Override
			public boolean onKey( View v, int keyCode, KeyEvent event ) {
				if ( keyCode == KeyEvent.KEYCODE_ENTER ) {
					hideKeyboard( context, v );
					return true;
				} else
					return false;
			}
		} );
	}
}