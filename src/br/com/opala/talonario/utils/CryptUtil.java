package br.com.opala.talonario.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.Date;

import android.util.Log;

public class CryptUtil {

	public static void encryptObject( Object klass ) {
		Log.i( "Talonario", "CLASSE: " + klass.getClass().getSimpleName() );
		Field[] fields = klass.getClass().getDeclaredFields();
		for ( Field field : fields ) {
			field.setAccessible( true );
			Class<?> targetType = field.getType();
			try {
				if ( isMountable( targetType ) ) {
					Object innerClass = field.get( klass );
					if ( innerClass != null )
						encryptObject( innerClass );
				} else {
					String methodName = "set" + field.getName().substring( 0, 1 ).toUpperCase() + field.getName().substring( 1 );
					Method method = klass.getClass().getMethod( methodName, targetType );
					Object valueObject = field.get( klass );
					method.invoke( klass, valueObject );
					Log.i( "Talonario", "Método: " + method.getName() + " Valor: " + valueObject );
				}
			} catch ( IllegalAccessException e ) {
				e.printStackTrace();
			} catch ( IllegalArgumentException e ) {
				e.printStackTrace();
			} catch ( InvocationTargetException e ) {
				e.printStackTrace();
			} catch ( NoSuchMethodException e ) {
				e.printStackTrace();
			}

		}
	}

	private static boolean isMountable( Class<?> k ) {
		return !Modifier.isAbstract( k.getModifiers() ) && !k.isPrimitive() && k != Byte.class && k != Short.class && k != Integer.class && k != Long.class && k != Float.class && k != Double.class && k != BigDecimal.class && k != Boolean.class && k != Character.class && k != String.class && k != Date.class && k != byte[].class;
	}
}
